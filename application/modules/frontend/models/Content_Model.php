<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function view_barang()
	{
		return $this->db->query("
			SELECT a.`id`, a.`nama`, a.`rating`, a.`harga`,
			b.image
			FROM ms_barang a
			INNER JOIN (
				SELECT *
				FROM tb_image_barang 
				WHERE main = 1
			) AS b ON a.`id` = b.id_barang
			WHERE a.`status` = 1 
			LIMIT 10 ")->result();
	}

	function barang_new()
	{
		return $this->db->query("
			SELECT a.`id`, a.`nama`, a.`rating`, a.`harga`,
			b.image
			FROM ms_barang a
			INNER JOIN (
				SELECT *
				FROM tb_image_barang 
				WHERE main = 1
			) AS b ON a.`id` = b.id_barang
			WHERE a.`status` = 1 
			LIMIT 3 ")->result();
	}

	function barang_hot()
	{
		return $this->db->query("
			SELECT a.`id`, a.`nama`, a.`rating`, a.`harga`,
			b.image
			FROM ms_barang a
			INNER JOIN (
				SELECT *
				FROM tb_image_barang 
				WHERE main = 1
			) AS b ON a.`id` = b.id_barang
			WHERE a.`status` = 1 
			LIMIT 3 ")->result();
	}

	function barang_sale()
	{
		return $this->db->query("
			SELECT a.`id`, a.`nama`, a.`rating`, a.`harga`,
			b.image
			FROM ms_barang a
			INNER JOIN (
				SELECT *
				FROM tb_image_barang 
				WHERE main = 1
			) AS b ON a.`id` = b.id_barang
			WHERE a.`status` = 1 
			LIMIT 3 ")->result();
	}

	function view_banner()
	{
		return $this->db->where(['status' => 1])->get('ms_banner')->result();
	}
}

/* End of file Content_Model.php */
/* Location: ./application/models/Content_Model.php */ ?>