<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Keranjang extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Keranjang_Model');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('GET', false);
        if ($auth == true) {
            $data = $this->Keranjang_Model->view_keranjang();

            $response = [];
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $data);
        }
    }

    function add_to_cart()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_barang = isset($params['id_barang']) ? $params['id_barang'] : '';
            $jumlah = isset($params['jumlah']) ? $params['jumlah'] : 0;
            $jenis = isset($params['jenis']) ? $params['jenis'] : '';

            $response = [];
            if ($id_barang == '' || $jumlah == 0 || $jenis == '') {
                $status = 404;
                $message = '';

                if ($id_barang == '') {
                    $message .= 'Masukkan Barang';
                }

                if ($jumlah == 0) {
                    $message .= 'Masukkan Jumlah';
                }

                if ($jenis == '') {
                    $message .= 'Masukkan Jenis';
                }
            } else {
                $user_id = userid();
                $id_user = iduser();

                # jika uid kosong berarti user belum login
                if ($user_id != '') {
                    # check barang di keranjang apakah sudah ada
                    $check_barang = $this->Main_Model->view_by_id('tb_keranjang', ['id_barang' => $id_barang, 'id_user' => $id_user, 'flag' => 1], '', '', [], [], 'row');

                    # jika barang belum ada maka insert
                    if (empty($check_barang)) {
                        $ms_barang = $this->Main_Model->view_by_id('ms_barang', ['id' => $id_barang], '', '', [], [], 'row');
                        $id_penjual = isset($ms_barang->id_penjual) ? $ms_barang->id_penjual : '';
                        $nama = isset($ms_barang->nama) ? $ms_barang->nama : '';
                        $harga = isset($ms_barang->harga) ? $ms_barang->harga : 0;
                        $total = $harga * $jumlah;
                        $id_user = iduser();

                        $data = array(
                            'id_user' => $id_user,
                            'id_barang' => $id_barang,
                            'nama' => $nama,
                            'harga' => $harga,
                            'jumlah' => $jumlah,
                            'total' => $total,
                            'jenis' => $jenis,
                            'id_penjual' => $id_penjual,
                            'user_insert' => $user_id
                        );

                        $simpan = $this->Main_Model->process_data('tb_keranjang', $data);
                        if ($simpan > 0) {
                            $response[] = $simpan;
                            $status = 200;
                            $message = 'Berhasil menyimpan data';
                        } else {
                            $status = 500;
                            $message = 'Terjadi kesalahan saat menyimpan data';
                        }

                        # jika barang sudah ada dikeranjang maka update jumlah barang
                    } else {
                        $where = array(
                            'id_user' => $id_user,
                            'id_barang' => $id_barang,
                            'flag' => 1
                        );
                        $qty = isset($check_barang->jumlah) ? $check_barang->jumlah : 0;

                        $data_update = array(
                            'jumlah' => $qty + $jumlah
                        );

                        $update = $this->Main_Model->process_data('tb_keranjang', $data_update, $where);
                        if ($update > 0) {
                            $status = 200;
                            $message = 'Berhasil menyimpan data';
                        } else {
                            $status = 500;
                            $message = 'Terjadi kesalahan saat menyimpan data';
                        }
                    }
                    # return ketika user belum login
                } else {
                    $status = 401;
                    $message = 'Silahkan login untuk menambahkan barang.';
                }
            }

            print_json($status, $message);
            log_api($params, $status, $message, $response);
        }
    }

    function hapus_keranjang()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            # array id keranjang
            $id_keranjang = isset($params['id_keranjang']) ? $params['id_keranjang'] : '';

            $response = [];
            if ($id_keranjang) {
                $jumlah = count($id_keranjang);

                for ($i = 0; $i < $jumlah; $i++) {
                    $id = isset($id_keranjang[$i]) ? $id_keranjang[$i] : '';

                    if ($id != '') {
                        $hapus = $this->Main_Model->delete_data('tb_keranjang', ['id' => $id]);
                        if ($hapus > 0) {
                            $response[] = $hapus;
                        }
                    }
                }

                $status = 200;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 404;
                $message = 'Pilih Barang yang akan dihapus.';
            }

            print_json($status, $message);
            log_api($params, $status, $message, $response);
        }
    }

    function update_flag()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            # array id keranjang
            $id_keranjang = isset($params['id_keranjang']) ? $params['id_keranjang'] : '';

            $response = [];
            if ($id_keranjang) {
                $jumlah = count($id_keranjang);

                for ($i = 0; $i < $jumlah; $i++) {
                    $id = isset($id_keranjang[$i]) ? $id_keranjang[$i] : '';

                    if ($id != '') {
                        $hapus = $this->Main_Model->process_data('tb_keranjang', ['flag' => 2], ['id' => $id]);
                        if ($hapus > 0) {
                            $response[] = $hapus;
                        }
                    }
                }

                $status = 200;
                $message = 'Data berhasil diupdate';
            } else {
                $status = 404;
                $message = 'Pilih Barang yang akan diupdate.';
            }

            print_json($status, $message);
            log_api($params, $status, $message, $response);
        }
    }
}

/* End of file Keranjang.php */
/* Location: ./application/controllers/Keranjang.php */
