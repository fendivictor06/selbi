<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Chat extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Chat_Model');
    }

    function list_chat()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $data  = $this->Chat_Model->list_chat($start, $count);

            $response = [];
            if (!empty($data)) {
                $response = $data;

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data';
            }

            print_json($status, $message, $response);
        }
    }

    function detail_chat()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $from  = isset($params['from']) ? $params['from'] : 0;
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $data  = $this->Chat_Model->detail_chat($from, $start, $count);
            $this->Chat_Model->update_chat_open($from);

            $response = [];
            if (!empty($data)) {
                $response = $data;

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data';
            }

            print_json($status, $message, $response);
        }
    }

    function insert()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $to   = isset($params['to']) ? $params['to'] : 0;
            $msg  = isset($params['message']) ? $params['message'] : 0;

            $response = '';
            if($msg == '') {
                $status = 404;
                $message = 'Message tidak boleh kosong !';

            } else {
                $user_id = userid();
                $data = array(
                            'from' => $user_id,
                            'to' => $to,
                            'message' => $msg,
                            'tanggal' => date('Y-m-d')
                        );

                $uniq_chat= $this->Main_Model->jobs_id();
                $is_eksis = $this->Chat_Model->check_eksis_chat($user_id, $to);
                if(!empty($is_eksis)) {
                    $uniq_chat = isset($is_eksis->uniq_chat) ? $is_eksis->uniq_chat:'';
                }

                // push uniq_chat
                $data['uniq_chat'] = $uniq_chat;

                $result = $this->Chat_Model->save_chat($data);

                if ($result != '') {
                    $this->Chat_Model->send_notif_chat($data);

                    $status = 200;
                    $message = 'Berhasil mengirim pesan';
                } else {
                    $status = 404;
                    $message = 'Gagal mengirim pesan';
                }                
            }


            print_json($status, $message, $response);
        }
    }

    function delete()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id_chat']) ? $params['id_chat'] : 0;

            $result = $this->Chat_Model->delete_chat($id);

            $response = '';
            if ($result != '') {

                $status = 200;
                $message = 'Berhasil menghapus pesan';
            } else {
                $status = 404;
                $message = 'Gagal menghapus pesan';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */