<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notif extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('api/Notif_Model');
	}

	function index()
	{
		$auth = $this->auth->check_auth_client('POST');
        if ($auth == true) {
        	$params = get_params();
        	$tipe = isset($params['tipe']) ? $params['tipe'] : '';
        	$start = isset($params['start']) ? $params['start'] : '';
        	$count = isset($params['count']) ? $params['count'] : '';

        	$data = $this->Notif_Model->view_notif($tipe, $start, $count);
        	$response = [];
            if ($data) {
            	$response = $data;
                $status = 200;
                $message = 'Success';
            } else {
                $status = 404;
                $message = 'Tidak ada data ditemukan';
            }

            print_json($status, $message, $response);
        }
	}

	function update_notif()
	{
		$auth = $this->auth->check_auth_client('POST');
        if ($auth == true) {
        	$params = get_params();
        	$id = isset($params['id']) ? $params['id'] : '';

        	$data = ['read' => 1];
        	$update = $this->Main_Model->process_data('tb_notif', $data, ['id' => $id]);
        	$response = [];
            if ($update > 0) {
                $status = 200;
                $message = 'Success';
            } else {
                $status = 500;
                $message = 'Gagal menyimpan data.';
            }

            print_json($status, $message, $response);
        }
	}
}

/* End of file Notif.php */
/* Location: ./application/controllers/Notif.php */ ?>
