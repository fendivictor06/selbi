<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Penjual extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Penjual_Model');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $pekerjaan = isset($params['pekerjaan']) ? $params['pekerjaan'] : '';

            $jumlah = $this->Penjual_Model->total_penjual();
            $is_update = $this->Penjual_Model->is_update_random();

            if ($jumlah > 0 && $is_update > 1) {
                $this->Main_Model->process_data('tb_user', ['urutan' => 0], ['penjual' => 1]);

                $data_to_update = $this->Main_Model->view_by_id('tb_user', ['status' => 1, 'penjual' => 1], '', '', [], [], 'result');
                if ($data_to_update) {
                    foreach ($data_to_update as $upd) {
                        # update random urutan
                        $random = $this->Penjual_Model->random_urutan($jumlah);
                        $update = $this->Main_Model->process_data('tb_user', ['urutan' => $random, 'update_urutan' => now()], ['id' => $upd->id]);
                    }
                }
            }

            $data = $this->Penjual_Model->view_penjual($id, $start, $count, $keyword, $pekerjaan);

            $response = [];
            if ($data) {
                $response['total_records'] = $jumlah;
                foreach ($data as $row => $val) {
                    $id = $val->id;
                    $nama = $val->nama;
                    $image = $val->image;
                    $deskripsi = $val->deskripsi;
                    $join_at = $val->join_at;
                    $kota = $val->kota;
                    $pekerjaan = $val->pekerjaan;
     
                    $response['pelapak'][$row] = array(
                        'id' => $id,
                        'nama' => $nama,
                        'image' => $image,
                        'deskripsi' => $deskripsi,
                        'join_at' => $join_at,
                        'kota' => $kota,
                        'pekerjaan' => $pekerjaan
                    );
                }
                
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

    function index20181121()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $data = $this->Penjual_Model->view_penjual($id, $start, $count);

            $response = [];
            if ($data) {
                $jumlah = $this->Penjual_Model->total_penjual();
                $response['total_records'] = $jumlah;
                foreach ($data as $row => $val) {
                    # path penjual
                    $path_penjual = path_pelapak();
                    $id = $val->id;
                    $nama = $val->nama;
                    $image = $val->image;
                    $deskripsi = $val->deskripsi;
                    $join_at = $val->join_at;
                    $kota = $val->kota;

                    $image = ($image != '') ? $path_penjual.$image : '';
     
                    $response['pelapak'][$row] = array(
                        'id' => $id,
                        'nama' => $nama,
                        'image' => $image,
                        'deskripsi' => $deskripsi,
                        'join_at' => $join_at,
                        'kota' => $kota
                    );
                }
                
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

    function following()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $pekerjaan = isset($params['pekerjaan']) ? $params['pekerjaan'] : '';

            $data = $this->Penjual_Model->view_following($id, $start, $count, $keyword, $pekerjaan);

            $response = [];
            if ($data) {
                foreach ($data as $row => $val) {
                    $id = $val->id;
                    $nama = $val->nama;
                    $image = $val->image;
                    $deskripsi = $val->deskripsi;
                    $join_at = $val->join_at;
                    $kota = $val->kota;
                    $pekerjaan = $val->pekerjaan;
     
                    $response['pelapak'][$row] = array(
                        'id' => $id,
                        'nama' => $nama,
                        'image' => $image,
                        'deskripsi' => $deskripsi,
                        'join_at' => $join_at,
                        'kota' => $kota,
                        'pekerjaan' => $pekerjaan
                    );
                }
                
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

    function follower()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $pekerjaan = isset($params['pekerjaan']) ? $params['pekerjaan'] : '';

            $data = $this->Penjual_Model->view_follower($id, $start, $count, $keyword, $pekerjaan);

            $response = [];
            if ($data) {
                foreach ($data as $row => $val) {
                    $id = $val->id;
                    $nama = $val->nama;
                    $image = $val->image;
                    $deskripsi = $val->deskripsi;
                    $join_at = $val->join_at;
                    $kota = $val->kota;
                    $pekerjaan = $val->pekerjaan;
     
                    $response['pelapak'][$row] = array(
                        'id' => $id,
                        'nama' => $nama,
                        'image' => $image,
                        'deskripsi' => $deskripsi,
                        'join_at' => $join_at,
                        'kota' => $kota,
                        'pekerjaan' => $pekerjaan
                    );
                }
                
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Penjual.php */
/* Location: ./application/controllers/Penjual.php */
