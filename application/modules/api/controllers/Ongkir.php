<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ongkir extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('Rajaongkir');
	}

	function provinsi()
	{
		$auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';

            $data = $this->rajaongkir->request('province', ['id' => $id]);
            $status = isset($data['status']) ? $data['status'] : 0;
            $message = isset($data['message']) ? $data['message'] : '';
            $response = isset($data['response']) ? $data['response'] : ''; 

            $response = json_decode($response, true);
            $response = isset($response['rajaongkir']['results']) ? $response['rajaongkir']['results'] : [];

            if ($response) {
            	$status = 200;
            	$message = 'Berhasil';
            } else {
            	$status = 404;
            	$message = 'Tidak ada data ditemukan.';
            }

            print_json($status, $message, $response);
        }
	}

	function kota()
	{
		$auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $province = isset($params['id_provinsi']) ? $params['id_provinsi'] : '';

            $data = $this->rajaongkir->request('city', ['id' => $id, 'province' => $province]);
            $status = isset($data['status']) ? $data['status'] : 0;
            $message = isset($data['message']) ? $data['message'] : '';
            $response = isset($data['response']) ? $data['response'] : '';

            $response = json_decode($response, true);
            $response = isset($response['rajaongkir']['results']) ? $response['rajaongkir']['results'] : [];

            if ($response) {
            	$status = 200;
            	$message = 'Berhasil';
            } else {
            	$status = 404;
            	$message = 'Tidak ada data ditemukan.';
            }

            print_json($status, $message, $response);
        }
	}

    function subdistrict()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $city = isset($params['city']) ? $params['city'] : '';

            $data = $this->rajaongkir->request('subdistrict', ['id' => $id, 'city' => $city]);
            $status = isset($data['status']) ? $data['status'] : 0;
            $message = isset($data['message']) ? $data['message'] : '';
            $response = isset($data['response']) ? $data['response'] : '';

            $response = json_decode($response, true);
            $response = isset($response['rajaongkir']['results']) ? $response['rajaongkir']['results'] : [];

            if ($response) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

	function hitung()
	{
		$auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $origin = isset($params['asal']) ? $params['asal'] : '';
			$destination = isset($params['tujuan']) ? $params['tujuan'] : '';
			$weight = isset($params['berat']) ? $params['berat'] : 0;
			$courier = isset($params['kurir']) ? $params['kurir'] : '';
            $origin_type = isset($params['originType']) ? $params['originType'] : '';
            $destination_type = isset($params['destinationType']) ? $params['destinationType'] : '';

			$condition = array(
				'origin' => $origin,
				'destination' => $destination,
				'weight' => $weight,
				'courier' => $courier,
                'originType' => $origin_type,
                'destinationType' => $destination_type
			);

            $data = $this->rajaongkir->request('cost', $condition);
            $status = isset($data['status']) ? $data['status'] : 0;
            $message = isset($data['message']) ? $data['message'] : '';
            $response = isset($data['response']) ? $data['response'] : '';

            $hasil = [];
            if ($response) {
                for ($i = 0; $i < count($response); $i++) {
                    if ($response[$i]) {
                        for ($a = 0; $a < count($response[$i]); $a++) {
                            $hasil[] = $response[$i][$a];
                        }
                    }
                }

            	$status = 200;
            	$message = 'Berhasil';
            } else {
            	$status = 404;
            	$message = 'Tidak ada data ditemukan.';
            }

            print_json($status, $message, $hasil);
        }
	}

    function waybill()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params(); 
            $courier = isset($params['courier']) ? $params['courier'] : '';
            $waybill = isset($params['waybill']) ? $params['waybill'] : '';

            $condition = array(
                'courier' => $courier,
                'waybill' => $waybill 
            );

            $data = $this->rajaongkir->request('waybill', $condition);
            $status = isset($data['status']) ? $data['status'] : 0;
            $message = isset($data['message']) ? $data['message'] : '';
            $response = isset($data['response']) ? $data['response'] : '';

            $response = json_decode($response, true);
            $response = isset($response['rajaongkir']['result']) ? $response['rajaongkir']['result'] : [];
 
            if ($response) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

	function kurir()
	{
		$auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';

            $response = [];
            $condition = ($id != '') ? ['id' => $id] : [];
            $result = ($id != '') ? 'row' : 'result';

            $condition['status'] = 1;
			$data = $this->Main_Model->view_by_id('ms_kurir', $condition, 'id, kode, label, status', '', [], [], $result);

            if ($data) {
            	$status = 200;
            	$message = 'Berhasil';

            	$response = $data;
            } else {
            	$status = 404;
            	$message = 'Tidak ada data ditemukan.';
            }

            print_json($status, $message, $response);
        }
	}
}

/* End of file Ongkir.php */
/* Location: ./application/controllers/Ongkir.php */ ?>