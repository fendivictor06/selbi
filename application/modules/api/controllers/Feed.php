<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Feed extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Feed_Model');
        $this->load->model('api/Notif_Model');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $jenis = isset($params['jenis']) ? $params['jenis'] : '';
            $id = isset($params['id']) ? $params['id'] : '';
            $id_penjual = isset($params['id_penjual']) ? $params['id_penjual'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $data = $this->Feed_Model->view_feed($jenis, $id, $id_penjual, $start, $count);

            $response = [];
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $data);
        }
    }

    function gallery()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $id_penjual = isset($params['id_penjual']) ? $params['id_penjual'] : '';
 
            $data = $this->Feed_Model->view_gallery($id, $id_penjual);

            $response = [];
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $data);
        }
    }

    function kegiatan()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $id_penjual = isset($params['id_penjual']) ? $params['id_penjual'] : '';
 
            $data = $this->Feed_Model->view_kegiatan($id, $id_penjual);

            $response = [];
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $data);
        }
    }

    function add_image()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            # define path
            define('UPLOAD_PATH', 'assets/uploads/feed/gallery/');
            $user_id = userid();
            $params = [];
            $arr_data = [];

            if (isset($_FILES['pic']['name'])) {
                $params['files'] = $_FILES['pic'];

                try {
                    move_uploaded_file($_FILES['pic']['tmp_name'], UPLOAD_PATH.$_FILES['pic']['name']);

                    $exists = file_exists(UPLOAD_PATH.$_FILES['pic']['name']);
                    if ($exists == true) {
                        $data = array(
                            'id_feed' => '',
                            'image' => $_FILES['pic']['name'],
                            'user_insert' => $user_id
                        );

                        $simpan = $this->Main_Model->process_data('tb_feed_image', $data);
                        if ($simpan > 0) {
                            $arr_data = ['id' => $simpan];

                            $status = 200;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 500;
                            $message = 'Terjadi kesalahan saat menyimpan data';
                        }
                    } else {
                        throw new Exception('Terjadi kesalahan saat mengupload file.');
                    }
                } catch (Exception $e) {
                    $status = 400;
                    $message = 'Terjadi kesalahan saat mengupload file.';
                }
            } else {
                $status = 404;
                $message = 'File tidak ditemukan.';
            }

            print_json($status, $message, $arr_data);
            log_api($params, $status, $message, $arr_data);
        }
    }

    function add_post()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            # jenis 1 : kegiatan, 2 : galeri, 3 : teks, 4 : produk
            $jenis = isset($params['jenis']) ? $params['jenis'] : '';
            $judul = isset($params['judul']) ? $params['judul'] : '';
            $gambar = isset($params['id_gambar']) ? $params['id_gambar'] : '';
            $tgl = isset($params['tgl']) ? $params['tgl'] : '';
            $tempat = isset($params['tempat']) ? $params['tempat'] : '';
            $deskripsi = isset($params['deskripsi']) ? $params['deskripsi'] : '';

            $user_id = userid();
            $id_user = iduser();

            if ($jenis == '') {
                $status = 404;
                $message = '';

                if ($jenis == '') {
                    $message .= 'Masukkan Jenis Feed';
                }
            } else {
                $data = array(
                    'id_penjual' => $id_user,
                    'jenis' => $jenis,
                    'judul' => $judul,
                    'tgl' => $tgl,
                    'tempat' => $tempat,
                    'deskripsi' => $deskripsi,
                    'user_insert' => $user_id
                );

                $simpan = $this->Main_Model->process_data('tb_feed', $data);
                if ($simpan > 0) {
                    $status = 200;
                    $message = 'Data berhasil disimpan';

                    if ($gambar) {
                        for ($i = 0; $i < count($gambar); $i++) {
                            $id_gambar = isset($gambar[$i]) ? $gambar[$i] : '';
                           
                            # update
                            $update = $this->Main_Model->process_data('tb_feed_image', ['id_feed' => $simpan], ['id' => $id_gambar]);
                        }
                    }

                    if ($jenis == '1' || $jenis == '2' || $jenis == '3') {
                        $teks = '';
                        if ($jenis == '1') {
                            $teks = 'Anda telah menambahkan Kegiatan';
                        } else if ($jenis == '2') {
                            $teks = 'Anda telah menambahkan '.count($gambar).' gambar';
                        } else {
                            $teks = 'Anda telah menambahkan status';
                        }
                        
                        # insert untuk riwayat
                        $this->Notif_Model->create_notif($id_user, '', $teks, 'feed');
                    }
                } else {
                    $status = 500;
                    $message = 'Terjadi kesalahan saat menyimpan data';
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }
}

/* End of file Feed.php */
/* Location: ./application/controllers/Feed.php */
