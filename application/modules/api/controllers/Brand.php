<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Brand extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $limit = '';
            if ($count > 0) {
                $limit[0] = $start;
                $limit[1] = $count;
            }

            # get category data
            $category = $this->Main_Model->view_by_id('ms_brand', ['status' => 1], '', "'brand', 'ASC'", [], $limit, 'result');

            # path image
            $path = path_brand();

            $response = [];
            if ($category) {
                foreach ($category as $row) {
                    $image = ($row->image != '') ? $path.$row->image : '';

                    $response[] = array(
                        'id' => $row->id,
                        'brand' => $row->brand,
                        'image' => $image
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Brand.php */
/* Location: ./application/controllers/Brand.php */
