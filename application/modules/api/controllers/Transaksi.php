<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Transaksi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Transaksi_Model');
        $this->load->library('Midtrans');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
 
            $data = $this->Transaksi_Model->header_transaksi($keyword, $start, $count);

            $response = [];
            if ($data) {
                foreach ($data as $row => $val) {
                    $response[$row] = array(
                        'nobukti' => $val->nobukti,
                        'tgl' => $val->tgl,
                        'total' => $val->total,
                        'noresi' => $val->noresi,
                        'status' => $val->status,
                        'status_transaksi' => $val->status_transaksi
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

    function details()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';

            $data = $this->Transaksi_Model->detail_transaksi($id);

            $response = [];
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $data);
        }
    }

    function prepayment()
    {
        $params = get_params();

        $response = $this->midtrans->payment($params);

        echo $response;
    }

    function payment($type = '')
    {
        $params = get_params();

        $transaction_time = isset($params['transaction_time']) ? $params['transaction_time']  : '';
        $transaction_status = isset($params['transaction_status']) ? $params['transaction_status'] : '';
        $transaction_id = isset($params['transaction_id']) ? $params['transaction_id'] : '';
        $status_message = isset($params['status_message']) ? $params['status_message'] : '';
        $status_code = isset($params['status_code']) ? $params['status_code'] : '';
        $signature_key = isset($params['signature_key']) ? $params['signature_key'] : '';
        $payment_type = isset($params['payment_type']) ? $params['payment_type'] : '';
        $order_id = isset($params['order_id']) ? $params['order_id'] : '';
        $masked_card = isset($params['masked_card']) ? $params['masked_card'] : '';
        $gross_amount = isset($params['gross_amount']) ? $params['gross_amount'] : '';
        $fraud_status = isset($params['fraud_status']) ? $params['fraud_status'] : '';
        $eci = isset($params['eci']) ? $params['eci'] : '';
        $channel_response_message = isset($params['channel_response_message']) ? $params['channel_response_message'] : '';
        $channel_response_code = isset($params['channel_response_code']) ? $params['channel_response_code'] : '';
        $bank = isset($params['bank']) ? $params['bank'] : '';
        $approval_code = isset($params['approval_code']) ? $params['approval_code'] : '';
        $va_number = isset($params['va_numbers'][0]['va_number']) ? $params['va_numbers'][0]['va_number'] : '';
        if ($bank == '') {
            $bank = isset($params['va_numbers'][0]['bank']) ? $params['va_numbers'][0]['bank'] : '';
        }

        $notify = array(
            'transaction_time' => $transaction_time,
            'transaction_status' => $transaction_status,
            'transaction_id' => $transaction_id,
            'status_message' => $status_message,
            'status_code' => $status_code,
            'signature_key' => $signature_key,
            'payment_type' => $payment_type,
            'order_id' => $order_id,
            'masked_card' => $masked_card,
            'gross_amount' => $gross_amount,
            'fraud_status' => $fraud_status,
            'eci' => $eci,
            'channel_response_message' => $channel_response_message,
            'channel_response_code' => $channel_response_code,
            'bank' => $bank,
            'approval_code' => $approval_code,
            'va_number' => $va_number
        );

        // insert into log
        $this->customdb->process_data('log_notify', $notify);

        if ($type == 'notification') {
            if ($transaction_status == 'settlement' || $transaction_status == 'capture') {
                $this->Transaksi_Model->simpan_transaksi($order_id);
            } else if ($transaction_status == 'expire') {
                $this->customdb->process_data('temp_transaksi', ['status' => 9], ['order_id' => $order_id]);
            } else {
                # do nothing
            }
        } else if ($type == 'finish') {

        } else if ($type == 'unfinish') {

        } else if ($type == 'error') {

        } else {

        }

        log_api($params, 200, 'Berhasil', ['type' => $type]);
    }


    function prelelang()
    {
        $params = get_params();

        $response = $this->midtrans->lelang($params);

        echo $response;
    }

    function lelang($type = '')
    {
        $params = get_params();

        $transaction_time = isset($params['transaction_time']) ? $params['transaction_time']  : '';
        $transaction_status = isset($params['transaction_status']) ? $params['transaction_status'] : '';
        $transaction_id = isset($params['transaction_id']) ? $params['transaction_id'] : '';
        $status_message = isset($params['status_message']) ? $params['status_message'] : '';
        $status_code = isset($params['status_code']) ? $params['status_code'] : '';
        $signature_key = isset($params['signature_key']) ? $params['signature_key'] : '';
        $payment_type = isset($params['payment_type']) ? $params['payment_type'] : '';
        $order_id = isset($params['order_id']) ? $params['order_id'] : '';
        $masked_card = isset($params['masked_card']) ? $params['masked_card'] : '';
        $gross_amount = isset($params['gross_amount']) ? $params['gross_amount'] : '';
        $fraud_status = isset($params['fraud_status']) ? $params['fraud_status'] : '';
        $eci = isset($params['eci']) ? $params['eci'] : '';
        $channel_response_message = isset($params['channel_response_message']) ? $params['channel_response_message'] : '';
        $channel_response_code = isset($params['channel_response_code']) ? $params['channel_response_code'] : '';
        $bank = isset($params['bank']) ? $params['bank'] : '';
        $approval_code = isset($params['approval_code']) ? $params['approval_code'] : '';
        $va_number = isset($params['va_numbers'][0]['va_number']) ? $params['va_numbers'][0]['va_number'] : '';
        if ($bank == '') {
            $bank = isset($params['va_numbers'][0]['bank']) ? $params['va_numbers'][0]['bank'] : '';
        }

        $notify = array(
            'transaction_time' => $transaction_time,
            'transaction_status' => $transaction_status,
            'transaction_id' => $transaction_id,
            'status_message' => $status_message,
            'status_code' => $status_code,
            'signature_key' => $signature_key,
            'payment_type' => $payment_type,
            'order_id' => $order_id,
            'masked_card' => $masked_card,
            'gross_amount' => $gross_amount,
            'fraud_status' => $fraud_status,
            'eci' => $eci,
            'channel_response_message' => $channel_response_message,
            'channel_response_code' => $channel_response_code,
            'bank' => $bank,
            'approval_code' => $approval_code,
            'va_number' => $va_number
        );

        // insert into log
        $this->customdb->process_data('log_notify', $notify);

        if ($type == 'notification') {
            if ($transaction_status == 'settlement' || $transaction_status == 'capture') {
                $this->Transaksi_Model->simpan_bid($order_id);
            } else if ($transaction_status == 'expire') {
                $this->customdb->process_data('temp_transaksi', ['status' => 9], ['order_id' => $order_id]);
            } else {
                # do nothing
            }
        } else if ($type == 'finish') {

        } else if ($type == 'unfinish') {

        } else if ($type == 'error') {

        } else {

        }

        log_api($params, 200, 'Berhasil', ['type' => $type]);
    }
}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */
