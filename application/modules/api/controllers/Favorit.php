<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Favorit extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Favorit_Model');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $data = $this->Favorit_Model->view_favorit($keyword, $start, $count);

            $response = [];
            if ($data) {
                $path = path_produk();

                foreach ($data as $row => $val) {
                    $response[$row] = array(
                        'id' => $val->id,
                        'id_barang' => $val->id_barang,
                        'barang' => $val->barang,
                        'image' => $path.$val->image,
                        'harga' => $val->harga,
                        'rating' => $val->rating,
                        'penjual' => $val->penjual,
                        'foto' => $val->foto,
                        'rating_penjual' => '4.5'
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

    function add_to_favorit()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_barang = isset($params['id_barang']) ? $params['id_barang'] : '';
            $response = [];

            if ($id_barang == '') {
                $status = 404;
                $message = '';

                if ($id_barang == '') {
                    $message .= 'Masukkan Barang';
                }
            } else {
                $id_user = iduser();
                $user_id = userid();

                if ($id_user != '') {
                    $data = array(
                        'id_user'  => $id_user,
                        'id_barang' => $id_barang,
                        'user_insert' => $user_id
                    );

                    $hapus = $this->Main_Model->delete_data('tb_favorit', ['id_barang' => $id_barang, 'id_user' => $id_user]);

                    $simpan = $this->Main_Model->process_data('tb_favorit', $data);
                    if ($simpan > 0) {
                        $response[] = $simpan;

                        $status = 200;
                        $message = 'Berhasil menyimpan data';
                    } else {
                        $status = 404;
                        $message = 'Terjadi kesalahan saat menyimpan data';
                    }
                } else {
                    $status = 401;
                    $message = 'Silahkan login untuk menambahkan barang.';
                }
            }

            print_json($status, $message);
            log_api($params, $status, $message, $response);
        }
    }

    function hapus_favorit()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            # array id keranjang
            $id_barang = isset($params['id_barang']) ? $params['id_barang'] : '';
            $id_user = iduser();

            $response = [];
            if ($id_barang) {
                $jumlah = count($id_barang);

                for ($i = 0; $i < $jumlah; $i++) {
                    $id = isset($id_barang[$i]) ? $id_barang[$i] : '';

                    if ($id != '') {
                        $hapus = $this->Main_Model->delete_data('tb_favorit', ['id_barang' => $id, 'id_user' => $id_user]);
                        if ($hapus > 0) {
                            $response[] = $hapus;
                        }
                    }
                }

                $status = 200;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 404;
                $message = 'Pilih Barang yang akan dihapus.';
            }

            print_json($status, $message);
            log_api($params, $status, $message, $response);
        }
    }
}

/* End of file Favorit.php */
/* Location: ./application/controllers/Favorit.php */
