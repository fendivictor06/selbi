<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Authentication extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $uid = isset($params['uid']) ? $params['uid'] : '';
            $foto = isset($params['foto']) ? $params['foto'] : '';
            $fcm_id = isset($params['fcm_id']) ? $params['fcm_id'] : '';

            $response = [];
            if ($uid != '') {
                $check_uid = $this->Main_Model->view_by_id('tb_user', ['uid' => $uid], '', '', [], [], 'row');

                if ($check_uid) {
                    $profile_name = isset($check_uid->profile_name) ? $check_uid->profile_name : '';
                    $penjual = isset($check_uid->penjual) ? $check_uid->penjual : 0;

                    if ($fcm_id != '') {
                        // if ($foto != '') {
                        //     $update['foto'] = $foto;
                        // }
                        if ($fcm_id != '') {
                            $update['fcm_id'] = $fcm_id;
                        }

                        $this->Main_Model->process_data('tb_user', $update, array('uid' => $uid));
                    }

                    $response = array(
                        'uid' => $uid,
                        'penjual' => $penjual
                    );

                    $status = 200;
                    $message = 'Welcome, '.$profile_name;
                } else {
                    $status = 404;
                    $message = 'User tidak ditemukan.';
                }
            } else {
                $status = 404;
                $message = 'Uid tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }

    function register()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $uid = isset($params['uid']) ? $params['uid'] : '';
            $email = isset($params['email']) ? $params['email'] : '';
            $profile_name = isset($params['profile_name']) ? $params['profile_name'] : '';
            $foto = isset($params['foto']) ? $params['foto'] : '';
            $type = isset($params['type']) ? $params['type'] : '';
            $fcm_id  = isset($params['fcm_id']) ? $params['fcm_id'] : '';

            $response = [];
            if ($uid != '') {
                $check_uid = $this->Main_Model->view_by_id('tb_user', ['uid' => $uid], '', '', [], [], 'row');
                $name = isset($check_uid->profile_name) ? $check_uid->profile_name : '';
                $penjual = isset($check_uid->penjual) ? $check_uid->penjual : 0;

                if (empty($check_uid)) {
                    $data = array(
                        'uid' => $uid,
                        'email' => $email,
                        'profile_name' => $profile_name,
                        'foto' => $foto,
                        'type' => $type,
                        'fcm_id' => $fcm_id,
                        'insert_at' => now(),
                        'user_insert' => $uid
                    );

                    $response = array(
                        'uid' => $uid,
                        'penjual' => $penjual
                    );

                    $simpan = $this->Main_Model->process_data('tb_user', $data);
                    if ($simpan > 0) {
                        $status = 200;
                        $message = 'Welcome, '.$profile_name;
                    } else {
                        $status = 500;
                        $message = 'Register Gagal, ulangi beberapa saat lagi.';
                    }
                } else {
                    $update = [];
                    if ($fcm_id != '') {
                        // if ($foto != '') {
                        //     $update['foto'] = $foto;
                        // }
                        if ($fcm_id != '') {
                            $update['fcm_id'] = $fcm_id;
                        }

                        $this->Main_Model->process_data('tb_user', $update, array('uid' => $uid));
                    }


                    $response = array(
                        'uid' => $uid,
                        'penjual' => $penjual
                    );

                    $status = 200;
                    $message = 'Welcome, '.$name;
                }
            } else {
                $status = 404;
                $message = 'Uid tidak ditemukan';
            }

            print_json($status, $message, $response);
            log_api($params, $status, $message, $response);
        }
    }

    function generate($userid = 'EKOtlZ63TEXHeBhfNuVhv4DRkqB3')
    {
        header('Content-Type:application/json');

        $timestamp = $this->auth->generate_timestamp();
        $signature = $this->auth->generate_signature($userid, $timestamp);

        $result = array(
            'User-Id' => $userid,
            'Timestamp' => $timestamp,
            'Signature' => $signature,
            'Time' => date('Y-m-d H:i:s')
        );

        echo json_encode($result);
    }

    function update_fcm_id()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $fcm_id = isset($params['fcm_id']) ? $params['fcm_id']:'';
            
            $uid  = userid();
            $response = [];
            if($fcm_id != '') {
                $data = array(
                            'fcm_id' => $fcm_id
                        );

                $this->Main_Model->process_data('tb_user', $data, array('uid' => $uid));

                $status  = 200;
                $message = 'Update fcm id sukses';
            } else {
                $status  = 404;
                $message = 'fcm id kosong';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */
