<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('api/User_Model');
	}

	function view_alamat()
	{
		$auth = $this->auth->check_auth_client('POST');
		if ($auth == true) {
			$params = get_params();
			$response = [];
			$id = isset($params['id']) ? $params['id'] : '';
			$keyword = isset($params['keyword']) ? $params['keyword'] : '';
			$start = isset($params['start']) ? $params['start'] : 0;
			$count = isset($params['count']) ? $params['count'] : 0;

			$data = $this->User_Model->view_alamat($keyword, $start, $count, $id);
			if ($data) {
				$status = 200;
				$message = 'Berhasil';

				$response = $data;
			} else {
				$status = 404;
				$message = 'Tidak ada data ditemukan';
			}

			print_json($status, $message, $response);
		}
	}

	function tambah_alamat()
	{
		$auth = $this->auth->check_auth_client('POST');
		if ($auth == true) {
			$params = get_params();
			$id_user = iduser();
			$id = isset($params['id']) ? $params['id'] : '';
			$label = isset($params['label']) ? $params['label'] : '';
			$penerima = isset($params['penerima']) ? $params['penerima'] : '';
			$alamat = isset($params['alamat']) ? $params['alamat'] : '';
			$id_provinsi = isset($params['id_provinsi']) ? $params['id_provinsi'] : '';
			$id_kota = isset($params['id_kota']) ? $params['id_kota'] : '';
			$kodepos = isset($params['kodepos']) ? $params['kodepos'] : '';
			$telepon = isset($params['telepon']) ? $params['telepon'] : '';
			$latitude = isset($params['latitude']) ? $params['latitude'] : '';
			$longitude = isset($params['longitude']) ? $params['longitude'] : '';


			if ($label == '' || $penerima == '' || $alamat == '' || $id_provinsi == '' || $id_kota == '' || $kodepos == '' || $telepon == '') {
				$status = 404;
				$message = '';

				$message .= ($label == '') ? 'Masukkan Label Alamat' : '';
				$message .= ($penerima == '') ? 'Masukkan Nama Penerima' : '';
				$message .= ($alamat == '') ? 'Masukkan Alamat' : '';
				$message .= ($id_provinsi == '') ? 'Masukkan Provinsi' : '';
				$message .= ($id_kota == '') ? 'Masukkan Kota' : '';
				$message .= ($kodepos == '') ? 'Masukkan Kode Pos' : '';
				$message .= ($telepon == '') ? 'Masukkan Telepon' : '';
			} else {
				# cek exists
				$tb_alamat = $this->customdb->view_by_id('tb_alamat_user', ['id_user' => $id_user]);
				$utama = ($tb_alamat) ? 0 : 1;

				$time = ($id == '') ? 'insert_at' : 'update_at';
				$user = ($id == '') ? 'user_insert' : 'user_update';

				$condition = ($id == '') ? [] : ['id' => $id];

				$now = now();
				$user_id = userid();

				$data = array(
					'id_user' => $id_user,
					'utama' => $utama,
					'label' => $label,
					'penerima' => $penerima,
					'alamat' => $alamat,
					'ref_provinsi' => $id_provinsi,
					'ref_kota' => $id_kota,
					'kodepos' => $kodepos,
					'telepon' => $telepon,
					'latitude' => $latitude,
					'longitude' => $longitude
				);

				$simpan = $this->Main_Model->process_data('tb_alamat_user', $data, $condition);
				if ($simpan > 0) {
					$status = 200;
					$message = 'Data berhasil disimpan';
				} else {
					$status = 500;
					$message = 'Gagal menyimpan data';
				}
			}

			print_json($status, $message, []);
		}
	}

	function hapus_alamat()
	{
		$auth = $this->auth->check_auth_client('POST');
		if ($auth == true) {
			$params = get_params();
			$id = isset($params['id']) ? $params['id'] : '';

			$delete = $this->customdb->delete_data('tb_alamat_user', ['id' => $id]);
			if ($delete > 0) {
				$status = 200;
				$message = 'Data berhasil dihapus';
			} else {
				$status = 500;
				$message = 'Gagal menghapus data';
			}

			print_json($status, $message, []);
		}
	}
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */ ?>