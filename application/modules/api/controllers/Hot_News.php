<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hot_News extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('api/Hot_News_Model');
	}

	function view()
	{
		$auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $id = isset($params['id']) ? $params['id'] : '';

            $data = $this->Hot_News_Model->view_news($keyword, $start, $count, $id);

            $response = [];
            if ($data) {
                $response = $data;

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data';
            }

            print_json($status, $message, $response);
        }
	}

}

/* End of file Hot_News.php */
/* Location: ./application/controllers/Hot_News.php */ ?>