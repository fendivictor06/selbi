<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function status()
	{
		$auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
        	$params = get_params();
        	$kode = isset($params['kode']) ? $params['kode'] : '';
        	$modul = isset($params['modul']) ? $params['modul'] : '';
        	$response = [];
        	$condition = [];
        	$row = 'result';

        	if ($kode != '') {
        		$condition['kode'] = $kode;
        		$row = 'row';
        	}

        	if ($modul != '') {
        		$condition['modul'] = $modul;
        	}

        	$data = $this->Main_Model->view_by_id('ms_status', $condition, '', '', [], [], $row);
        	if ($data) {
        		$response = $data;
        		$status = 200;
        		$message = 'Berhasil';
        	} else {
        		$status = 404;
        		$message = 'Tidak ada data ditemukan';
        	}

        	print_json($status, $message, $response);
        }
	}
}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */ ?>