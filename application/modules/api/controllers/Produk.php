<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Produk extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Produk_Model');
        $this->load->model('api/Notif_Model');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $jenis = isset($params['jenis']) ? $params['jenis'] : '';

            # brand
            $brand = isset($params['brand']) ? $params['brand'] : '';

            # kategori
            $kategori = isset($params['kategori']) ? $params['kategori'] : '';

            # penjual
            $penjual = isset($params['penjual']) ? $params['penjual'] : '';

            # donasi 
            $donasi = isset($params['donasi']) ? $params['donasi'] : '';

            # lelang 
            $lelang = isset($params['lelang']) ? $params['lelang'] : '';

            # data
            $data = $this->Produk_Model->view_produk($start, $count, $keyword, $brand, $kategori, $penjual, $jenis, $donasi, $lelang);

            # store array
            $response = [];
            if ($data) {
                # path
                $path = path_produk();

                foreach ($data as $row => $val) {
                    $response[$row] = array(
                        'id_barang' => $val->id_barang,
                        'kode' => $val->kode,
                        'nama' => $val->nama,
                        'category' => isset($val->category) ? $val->category : '',
                        'penjual' => $val->penjual,
                        'foto_penjual' => $val->foto,
                        'rating' => '4.2',
                        'brand' => isset($val->brand) ? $val->brand : '',
                        'jenis' => isset($val->jenis) ? $val->jenis : '',
                        'harga' => $val->harga,
                        'diskon' => $val->diskon,
                        'image' => $path.$val->image,
                        'is_favorit' => $val->is_favorit,
                        'pemakaian' => $val->pemakaian,
                        'satuan_pemakaian' => $val->satuan_pemakaian,
                        'donasi' => $val->donasi
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }

    function index20181121()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';

            # brand
            $brand = isset($params['brand']) ? $params['brand'] : '';

            # kategori
            $kategori = isset($params['kategori']) ? $params['kategori'] : '';

            # penjual
            $penjual = isset($params['penjual']) ? $params['penjual'] : '';

            # data
            $data = $this->Produk_Model->view_produk($start, $count, $keyword, $brand, $kategori, $penjual);

            # store array
            $response = [];
            if ($data) {
                # path
                $path = path_produk();

                foreach ($data as $row => $val) {
                    $response[$row] = array(
                        'id_barang' => $val->id_barang,
                        'kode' => $val->kode,
                        'nama' => $val->nama,
                        'category' => $val->category,
                        'penjual' => $val->penjual,
                        'brand' => $val->brand,
                        // 'deskripsi' => $val->deskripsi,
                        'harga' => $val->harga,
                        'diskon' => $val->diskon,
                        // 'berat' => $val->berat,
                        // 'berat_satuan' => $val->berat_satuan,
                        'image' => $path.$val->image,
                        'is_favorit' => $val->is_favorit
                    );

                    # push image gallery
                    // $image = $this->Main_Model->view_by_id('tb_image_barang', ['id_barang' => $val->id_barang, 'main' => 0], '', '', '', '', 'result');

                    // if ($image) {
                    //     foreach ($image as $img) {
                    //         $response[$row]['gallery'][]['image'] = $path.$img->image;
                    //     }
                    // } else {
                    //     $response[$row]['gallery'] = [];
                    // }
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }

    function details()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : 0;
            $id_user = iduser();
            $user_id = userid();

            $data = $this->Produk_Model->produk_detail($id);

            $response = [];
            if ($data) {
                # path
                $path = path_produk();

                # path penjual
                $path_penjual = path_pelapak();

                $id_barang = isset($data->id_barang) ? $data->id_barang : '';
                $kode = isset($data->kode) ? $data->kode : '';
                $nama = isset($data->nama) ? $data->nama : '';
                $category = isset($data->category) ? $data->category : '';
                $id_penjual = isset($data->id_penjual) ? $data->id_penjual : '';
                $brand = isset($data->brand) ? $data->brand : '';
                $deskripsi = isset($data->deskripsi) ? $data->deskripsi : '';
                $harga = isset($data->harga) ? $data->harga : 0;
                $diskon = isset($data->diskon) ? $data->diskon : 0;
                $berat = isset($data->berat) ? $data->berat : 0;
                $berat_satuan = isset($data->berat_satuan) ? $data->berat_satuan : '';
                $image = isset($data->image) ? $data->image : '';
                $rating = isset($data->rating) ? $data->rating : 0;
                $kondisi = isset($data->kondisi) ? $data->kondisi : '';
                $is_favorit = isset($data->is_favorit) ? $data->is_favorit : '0';
                $tahun_pembelian = isset($data->tahun_pembelian) ? $data->tahun_pembelian : '';
                $kondisi_barang = isset($data->kondisi_barang) ? $data->kondisi_barang : '';
                $deskripsi_pemakaian = isset($data->deskripsi_pemakaian) ? $data->deskripsi_pemakaian : '';

                # ms_penjual
                $ms_penjual = $this->Produk_Model->data_penjual($id_penjual);
                $image_penjual = isset($ms_penjual->foto) ? $ms_penjual->foto : '';
                $nama_penjual = isset($ms_penjual->profile_name) ? $ms_penjual->profile_name : '';
                $kota = isset($ms_penjual->kota) ? $ms_penjual->kota : '';
                $uid = isset($ms_penjual->uid) ? $ms_penjual->uid : '';

                # is followed
                $follow = $this->Main_Model->view_by_id('tb_follower', ['follower' => $id_user, 'id_user' => $id_penjual]);
                $is_followed = (!empty($follow)) ? 1 : 0;
 
                # array of penjual
                $penjual = array(
                    'id' => $id_penjual,
                    'nama' => $nama_penjual,
                    'image' => $image_penjual,
                    'kota' => $kota,
                    'rating' => '4.2',
                    'respon' => '80',
                    'feedback' => '1000',
                    'last_login' => '2018-10-03 23:59',
                    'followed' => $is_followed,
                    'uid' => $uid
                );

                $image_produk = ($image != '') ? $path.$image : '';

                $response = array(
                    'id_barang' => $id_barang,
                    'kode' => $kode,
                    'nama' => $nama,
                    'category' => $category,
                    'penjual' => $penjual,
                    'brand' => $brand,
                    'deskripsi' => $deskripsi,
                    'harga' => $harga,
                    'diskon' => $diskon,
                    'berat' => $berat,
                    'berat_satuan' => $berat_satuan,
                    'image' => $image_produk,
                    'kondisi' => $kondisi,
                    'dilihat' => '1000',
                    'terjual' => '100',
                    'rating' => $rating,
                    'is_favorit' => $is_favorit,
                    'tahun_pembelian' => $tahun_pembelian,
                    'kondisi_barang' => $kondisi_barang,
                    'deskripsi_pemakaian' => $deskripsi_pemakaian
                );

                # push image gallery
                $image = $this->Main_Model->view_by_id('tb_image_barang', ['id_barang' => $id_barang, 'main' => 0], '', '', '', '', 'result');

                if ($image) {
                    foreach ($image as $img) {
                        $response['gallery'][]['image'] = $path.$img->image;
                    }
                } else {
                    $response['gallery'] = [];
                }

                # log 
                $exists_log = $this->Main_Model->view_by_id('counter_barang', [
                    'id_user' => $id_user, 
                    'id_barang' => $id_barang
                ]);
                if ($exists_log) {
                    $this->Main_Model->process_data('counter_barang', [
                        'update_at' => now(), 
                        'user_update' => $user_id
                    ], [
                        'id_user' => $id_user, 
                        'id_barang' => $id_barang
                    ]);
                } else {
                    $this->Main_Model->process_data('counter_barang', [
                        'id_user' => $id_user, 
                        'id_barang' => $id_barang, 
                        'user_insert' => $user_id
                    ]);
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }

    function review()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $id_barang = isset($params['id_barang']) ? $params['id_barang'] : '';
            $rating = isset($params['rating']) ? $params['rating'] : 0;

            $data = $this->Produk_Model->review_barang($id_barang, $start, $count, $rating);
            $total = $this->Produk_Model->total_review($id_barang, $rating);

            $arr_data = [];
            if ($data) {
                $arr_data['total_records'] = $total;
                foreach ($data as $row => $val) {
                    $arr_data['review'][$row] = array(
                        'id' => $val->id,
                        'profile_name' => $val->profile_name,
                        'foto' => $val->foto,
                        'waktu' => $val->waktu,
                        'rating' => $val->rating,
                        'deskripsi' => $val->deskripsi
                    );

                    $child = $this->Produk_Model->review_child($val->id);
                    if ($child) {
                        foreach ($child as $col) {
                            $arr_data['review'][$row]['child'][] = array(
                                'profile_name' => $col->profile_name,
                                'foto' => $col->foto,
                                'waktu' => $col->waktu,
                                'deskripsi' => $col->deskripsi
                            );
                        }
                    }
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $arr_data);
        }
    }

    function add_review()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $id_barang = isset($params['id_barang']) ? $params['id_barang'] : '';
            $id_user = iduser();
            $rating = isset($params['rating']) ? $params['rating'] : 0;
            $deskripsi = isset($params['deskripsi']) ? $params['deskripsi'] : '';
            $user_id = userid();
            $response = [];

            if ($id_barang == '' || $deskripsi == '') {
                $status = 404;
                $message = '';

                if ($id_barang == '') {
                    $message .= 'Masukkan Barang.';
                }

                if ($deskripsi == '') {
                    $message .= 'Masukkan Review.';
                }
            } else {
                $data = array(
                    'parent' => $id,
                    'id_barang' => $id_barang,
                    'id_user' => $id_user,
                    'rating' => $rating,
                    'deskripsi' => $deskripsi,
                    'user_insert' => $user_id
                );

                $simpan = $this->Main_Model->process_data('tb_review', $data);
                if ($simpan > 0) {
                    $status = 200;
                    $message = 'Data berhasil disimpan';


                    # update rating barang
                    $this->Produk_Model->update_rating($id_barang);

                    $barang = $this->Main_Model->view_by_id('ms_barang', ['id' => $id_barang]);
                    $rating = isset($barang->rating) ? $barang->rating : 0;

                    $response['rating'] = $rating;


                    # ms produk 
                    $ms_produk = $this->Produk_Model->penjual_produk($id_barang);
                    $id_penjual = isset($ms_produk->id_penjual) ? $ms_produk->id_penjual : '';
                    $fcm_id = isset($ms_produk->fcm_id) ? $ms_produk->fcm_id : '';
                    $nama_barang = isset($ms_produk->nama) ? $ms_produk->nama : '';

                    # ms user 
                    $ms_user = $this->Main_Model->view_by_id('tb_user', ['id' => $id_user]);
                    $nama = isset($ms_user->profile_name) ? $ms_user->profile_name : '';

                    # insert untuk riwayat
                    $this->Notif_Model->create_notif($id_penjual, $id_user, $nama.' telah mereview barang '.$nama_barang, 'barang');

                    $this->load->library('firebasenotif');
                    # notif nya
                    $this->firebasenotif->notif('Review Barang', $nama.' telah mereview barang anda.', $fcm_id);
                } else {
                    $status = 500;
                    $message = 'Gagal menyimpan data';
                }
            }

            print_json($status, $message, $response);
            log_api($params, $status, $message, $response);
        }
    }

    function add_produk()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $nama = isset($params['nama']) ? $params['nama'] : '';
            $harga = isset($params['harga']) ? $params['harga'] : 0;
            $berat = isset($params['berat']) ? $params['berat'] : 0;
            $satuan_berat = isset($params['satuan_berat']) ? $params['satuan_berat'] : '';
            $deskripsi = isset($params['deskripsi']) ? $params['deskripsi'] : '';
            $ukuran = isset($params['ukuran']) ? $params['ukuran'] : '';
            $foto = isset($params['foto']) ? $params['foto'] : '';
            $gallery = isset($params['gallery']) ? $params['gallery'] : [];
            $kondisi = isset($params['kondisi']) ? $params['kondisi'] : '1';
            $brand = isset($params['brand']) ? $params['brand'] : '';
            $kategori = isset($params['kategori']) ? $params['kategori'] : '';
            $jenis = isset($params['jenis']) ? $params['jenis'] : '';
            $pemakaian = isset($params['pemakaian']) ? $params['pemakaian'] : '';
            $satuan_pemakaian = isset($params['satuan_pemakaian']) ? $params['satuan_pemakaian'] : '';

            # new added 2019 03 19
            $donasi = isset($params['donasi']) ? $params['donasi'] : 0;
            $tahun_pembelian = isset($params['tahun_pembelian']) ? $params['tahun_pembelian'] : 0;
            $kondisi_barang = isset($params['kondisi_barang']) ? $params['kondisi_barang'] : 0;
            $deskripsi_pemakaian = isset($params['deskripsi_pemakaian']) ? $params['deskripsi_pemakaian'] : '';

            # is lelang
            $lelang = isset($params['lelang']) ? $params['lelang'] : 0;
            $start = isset($params['start']) ? $params['start'] : '';
            $end = isset($params['end']) ? $params['end'] : '';
 

            $id_user = iduser();
            $user_id = userid();
            $images = [];
            $response = [];

            if ($nama == '' || $harga == 0 || $harga == '') {
                $status = 404;
                $message = '';

                if ($nama == '') {
                    $message .= 'Masukkan Nama Barang.';
                }

                if ($harga == 0 || $harga == '') {
                    $message .= 'Masukkan Harga Barang.';
                }
            } else {
                $data[] = [$kategori, $id_user, $brand, '', $nama, $deskripsi, $harga, '0', $berat, $satuan_berat, $kondisi, $user_id, $lelang, $jenis, $pemakaian, $satuan_pemakaian, $donasi, $tahun_pembelian, $kondisi_barang, $deskripsi_pemakaian];

                $simpan = $this->Produk_Model->add_produk($data, $foto, $gallery, $nama, $lelang, $start, $end, $harga);
                if ($simpan > 0) {
                    $response['id_barang'] = $simpan;

                    $status = 200;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 500;
                    $message = 'Terjadi kesalahan saat menyimpan data, ulangi beberapa saat lagi.';
                }
            }

            print_json($status, $message, $response);
            log_api($params, $status, $message, $response);
        }
    }

    function add_image()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            # define image path
            $upload_path = 'assets/uploads/product/';

            $user_id = userid();
            $params = [];
            $arr_data = [];

            if (isset($_FILES['pic']['name'])) {
                $params['files'] = $_FILES['pic'];

                try {
                    move_uploaded_file($_FILES['pic']['tmp_name'], $upload_path.$_FILES['pic']['name']);

                    $exists = file_exists($upload_path.$_FILES['pic']['name']);
                    if ($exists == true) {
                        $data = array(
                            'id_barang' => '',
                            'path' => base_url('assets/uploads/product/'.$_FILES['pic']['name']),
                            'image' => $_FILES['pic']['name'],
                            'main' => '0',
                            'user_insert' => $user_id
                        );

                        $simpan = $this->Main_Model->process_data('tb_image_barang', $data);
                        if ($simpan > 0) {
                            $arr_data = ['id' => $simpan];

                            $status = 200;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 500;
                            $message = 'Terjadi kesalahan saat menyimpan data';
                        }
                    } else {
                        throw new Exception('Terjadi kesalahan saat mengupload file.');
                    }
                } catch (Exception $e) {
                    $status = 400;
                    $message = 'Terjadi kesalahan saat mengupload file';
                }
            } else {
                $status = 404;
                $message = 'File tidak ditemukan.';
            }

            print_json($status, $message, $arr_data);
            log_api($params, $status, $message, $arr_data);
        }
    }

    function diskusi()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $id_barang = isset($params['id_barang']) ? $params['id_barang'] : '';

            $data = $this->Produk_Model->diskusi_barang($id_barang, $start, $count);
            $total = $this->Produk_Model->total_diskusi($id_barang);

            $arr_data = [];
            if ($data) {
                $arr_data['total_records'] = $total;
                foreach ($data as $row => $val) {
                    $arr_data['review'][$row] = array(
                        'id' => $val->id,
                        'profile_name' => $val->profile_name,
                        'foto' => $val->foto,
                        'waktu' => $val->waktu,
                        'deskripsi' => $val->deskripsi
                    );

                    $child = $this->Produk_Model->diskusi_child($val->id);
                    if ($child) {
                        foreach ($child as $col) {
                            $arr_data['review'][$row]['child'][] = array(
                                'profile_name' => $col->profile_name,
                                'foto' => $col->foto,
                                'waktu' => $col->waktu,
                                'deskripsi' => $col->deskripsi
                            );
                        }
                    }
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $arr_data);
        }
    }

    function add_diskusi()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $id_barang = isset($params['id_barang']) ? $params['id_barang'] : '';
            $id_user = iduser();
            $deskripsi = isset($params['deskripsi']) ? $params['deskripsi'] : '';
            $user_id = userid();

            if ($id_barang == '' || $deskripsi == '') {
                $status = 404;
                $message = '';

                if ($id_barang == '') {
                    $message .= 'Masukkan Barang.';
                }

                if ($deskripsi == '') {
                    $message .= 'Masukkan Diskusi.';
                }
            } else {
                $data = array(
                    'parent' => $id,
                    'id_barang' => $id_barang,
                    'id_user' => $id_user,
                    'deskripsi' => $deskripsi,
                    'user_insert' => $user_id
                );

                $simpan = $this->Main_Model->process_data('tb_diskusi', $data);
                if ($simpan > 0) {
                    $status = 200;
                    $message = 'Data berhasil disimpan';

                    # ms produk 
                    $ms_produk = $this->Produk_Model->penjual_produk($id_barang);
                    $id_penjual = isset($ms_produk->id_penjual) ? $ms_produk->id_penjual : '';
                    $fcm_id = isset($ms_produk->fcm_id) ? $ms_produk->fcm_id : '';
                    $nama_barang = isset($ms_produk->nama) ? $ms_produk->nama : '';

                    # ms user 
                    $ms_user = $this->Main_Model->view_by_id('tb_user', ['id' => $id_user]);
                    $nama = isset($ms_user->profile_name) ? $ms_user->profile_name : '';

                    # insert untuk riwayat
                    $this->Notif_Model->create_notif($id_penjual, $id_user, $nama.' telah mengomentari barang '.$nama_barang, 'barang');

                    $this->load->library('firebasenotif');
                    # notif nya
                    $this->firebasenotif->notif('Diskusi Barang', $nama.' telah mengomentari barang anda.', $fcm_id);
                } else {
                    $status = 500;
                    $message = 'Gagal menyimpan data';
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }

    function jumlah_rating()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_barang = isset($params['id_barang']) ? $params['id_barang'] : '';

            $data = $this->Produk_Model->rating_barang($id_barang);

            $arr_data = [];
            if ($data) {
                $arr_data = $data;

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $arr_data);
        }
    }

    function hot_item()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            # kategori
            $kategori = isset($params['kategori']) ? $params['kategori'] : '';

            # data
            $data = $this->Produk_Model->hot_item($keyword, $kategori, $start, $count);

            # store array 
            $response = [];
            if ($data) {
                # path
                $path = path_produk();

                foreach ($data as $row => $val) {
                    $image = ($val->image != '') ? $path.$val->image : '';

                    $response[$row] = array(
                        'id_barang' => $val->id,
                        'kode' => $val->kode,
                        'nama' => $val->nama,
                        'category' => isset($val->category) ? $val->category : '',
                        'penjual' => isset($val->penjual) ? $val->penjual : '',
                        'foto_penjual' => isset($val->foto_penjual) ? $val->foto_penjual : '',
                        'rating' => isset($val->rating_penjual) ? $val->rating_penjual : 0,
                        'brand' => isset($val->brand) ? $val->brand : '',
                        'jenis' => isset($val->jenis) ? $val->jenis : '',
                        'harga' => $val->harga,
                        'diskon' => $val->diskon,
                        'image' => $image,
                        'donasi' => $val->donasi
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }

    function add_donasi()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : 0;
            $response = [];

            $barang = $this->customdb->view_by_id('ms_barang', ['id' => $id], 'row');

            if ($barang) {
                $update = $this->customdb->process_data('ms_barang', ['donasi' => 1], ['id' => $id]);
                if ($update > 0) {
                    $status = 1;
                    $message = 'Berhasil, barang anda menjadi barang charity';
                } else {
                    $status = 0;
                    $message = 'Gagal mengubah status barang';
                }
            } else {
                $status = 0;
                $message = 'Barang tidak ditemukan';
            }


            print_json($status, $message, $response);
        }
    }

    function remove_donasi()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : 0;
            $response = [];

            $barang = $this->customdb->view_by_id('ms_barang', ['id' => $id], 'row');

            if ($barang) {
                $update = $this->customdb->process_data('ms_barang', ['donasi' => 0], ['id' => $id]);
                if ($update > 0) {
                    $status = 1;
                    $message = 'Berhasil, barang anda sudah tidak menjadi barang charity';
                } else {
                    $status = 0;
                    $message = 'Gagal mengubah status barang';
                }
            } else {
                $status = 0;
                $message = 'Barang tidak ditemukan';
            }


            print_json($status, $message, $response);
        }
    }

    function stok_barang()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $jenis = isset($params['jenis']) ? $params['jenis'] : '';
            $donasi = isset($params['donasi']) ? $params['donasi'] : '';
            $aktif = isset($params['aktif']) ? $params['aktif'] : '';
            $id_user = iduser();

            # data
            $data = $this->Produk_Model->stok($id_user, $keyword, $start, $count, $jenis, $donasi, $aktif);

            # store array
            $response = [];
            if ($data) {
                $response = $data;

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }

    function stok_lelang()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $aktif = isset($params['aktif']) ? $params['aktif'] : '';
            $id_user = iduser();

            # data
            $data = $this->Produk_Model->stok_lelang($id_user, $keyword, $start, $count, $aktif);

            # store array
            $response = [];
            if ($data) {
                $response = $data;

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */
