<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lelang extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Lelang_Model');
        $this->load->model('api/Produk_Model');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id_penjual']) ? $params['id_penjual'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $kategori = isset($params['kategori']) ? $params['kategori'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $data = $this->Lelang_Model->view_lelang('', $id, $keyword, $kategori, $start, $count);
            $path = path_produk();

            $response = [];
            if ($data) {
                foreach ($data as $row => $val) {
                    $response[$row] = array(
                        'id' => $val->id,
                        'nama' => $val->nama,
                        'image' => $path.$val->image,
                        'bid_awal' => $val->bid_awal,
                        'bid_akhir' => $val->bid_akhir,
                        'start' => $val->start,
                        'end' => $val->end,
                        'penjual' => isset($val->profile_name) ? $val->profile_name : '',
                        'rating' => isset($val->rating) ? $val->rating : 0,
                        'foto' => isset($val->foto) ? $val->foto : '',
                        'donasi' => $val->donasi
                    );
                }

                $status = 200;
                $message = 'Berhasil.';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

    function details()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : 0;
            $id_user = iduser();
            $user_id = userid();

            $data = $this->Lelang_Model->view_lelang($id);
            $response = [];
            if ($data) {
                # path
                $path = path_produk();

                # path penjual
                $path_penjual = path_pelapak();


                $id_barang = isset($data->id_barang) ? $data->id_barang : '';
                $nama = isset($data->nama) ? $data->nama : '';
                $id_penjual = isset($data->id_penjual) ? $data->id_penjual : '';
                $deskripsi = isset($data->deskripsi) ? $data->deskripsi : '';
                $image = isset($data->image) ? $data->image : '';
                $bid_awal = isset($data->bid_awal) ? $data->bid_awal : 0;
                $bid_akhir = isset($data->bid_akhir) ? $data->bid_akhir : 0;
                $kondisi = isset($data->kondisi) ? $data->kondisi : '';
                $berat = isset($data->berat) ? $data->berat : '';
                $berat_satuan = isset($data->berat_satuan) ? $data->berat_satuan : '';
                $category = isset($data->category) ? $data->category : '';
                $brand = isset($data->brand) ? $data->brand : '';
                $rating = isset($data->rating) ? $data->rating : '';

                $start = isset($data->start) ? $data->start : '';
                $end = isset($data->end) ? $data->end : '';

                # ms_penjual
                $ms_penjual = $this->Produk_Model->data_penjual($id_penjual);
                $image_penjual = isset($ms_penjual->foto) ? $ms_penjual->foto : '';
                $nama_penjual = isset($ms_penjual->profile_name) ? $ms_penjual->profile_name : '';
                $kota = isset($ms_penjual->kota) ? $ms_penjual->kota : '';
                $uid = isset($ms_penjual->uid) ? $ms_penjual->uid : '';

                # is followed
                $follow = $this->Main_Model->view_by_id('tb_follower', ['follower' => $id_user, 'id_user' => $id_penjual]);
                $is_followed = (!empty($follow)) ? 1 : 0;

                # array of penjual
                $penjual = array(
                    'id' => $id_penjual,
                    'nama' => $nama_penjual,
                    'image' => $image_penjual,
                    'kota' => $kota,
                    'rating' => '4.2',
                    'respon' => '80',
                    'feedback' => '1000',
                    'last_login' => '2018-10-03 23:59',
                    'followed' => $is_followed,
                    'uid' => $uid
                );

                $response = array(
                    'id_barang' => $id_barang,
                    'nama' => $nama,
                    'penjual' => $penjual,
                    'deskripsi' => $deskripsi,
                    'image' => $path.$image,
                    'bid_awal' => $bid_awal,
                    'bid_akhir' => $bid_akhir,
                    'start' => $start,
                    'end' => $end,
                    'kondisi' => $kondisi,
                    'berat' => $berat,
                    'berat_satuan' => $berat_satuan,
                    'category' => $category,
                    'brand' => $brand,
                    'dilihat' => '1000',
                    'terjual' => '100',
                    'rating' => $rating
                );

                # push image gallery
                $image = $this->Main_Model->view_by_id('tb_image_barang', ['id_barang' => $id_barang, 'main' => 0], '', '', '', '', 'result');

                if ($image) {
                    foreach ($image as $img) {
                        $response['gallery'][]['image'] = $path.$img->image;
                    }
                } else {
                    $response['gallery'] = [];
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

    function flash_sale()
    {
        $auth = $this->auth->check_auth_client('GET', false);
        if ($auth == true) {
            $header = [];
            $detail = [];
            $response = [];

            $data_header = $this->Lelang_Model->header_sale();
            $start = isset($data_header->start) ? $data_header->start : '';
            $end = isset($data_header->end) ? $data_header->end : '';
            $id = isset($data_header->id) ? $data_header->id : '';

            $data_detail = $this->Lelang_Model->detail_sale($id);
            # path
            $path = path_produk();

            if ($data_detail) {
                $header = array(
                    'start' => $start,
                    'end' => $end,
                    'id' => enkrip_id($id)
                );

                foreach ($data_detail as $row => $val) {
                    $image = '';
                    if ($val->image != '') {
                        $image = $path.$val->image;
                    }

                    $detail[$row] = array(
                        'id' => enkrip_id($val->id_barang),
                        'nama' => $val->nama,
                        'harga' => $val->harga,
                        'image' => $image,
                        'hargadiskon' => $val->hargadiskon,
                        'stok' => $val->stok,
                        'terjual' => $val->terjual
                    );
                }

                $response = array(
                    'header' => $header,
                    'detail' => $detail
                );

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $response);
        }
    }

    function bid()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_lelang = isset($params['id_lelang']) ? $params['id_lelang'] : '';
            $nominal = isset($params['nominal']) ? $params['nominal'] : 0;
            $user_id = userid();

            if ($user_id == '') {
                $status = 401;
                $message = 'Silahkan Login untuk bid lelang';
            } else {
                if ($id_lelang == '' || $nominal == 0) {
                    $status = 404;
                    $message = '';

                    if ($id_lelang == '') {
                        $message .= 'Pilih barang yang akan di lelang';
                    }

                    if ($nominal == 0) {
                        $message .= 'Masukkan Nominal Lelang';
                    }
                } else {
                    $last_bid = $this->Main_Model->view_by_id('tb_lelang', ['id' => $id_lelang], 'bid_akhir', '', '', '', 'row');
                    $bid_akhir = isset($last_bid->bid_akhir) ? $last_bid->bid_akhir : 0;

                    if ($bid_akhir >= $nominal) {
                        $status = 400;
                        $message = 'Nominal Harus lebih besar dari bid terakhir';
                    } else {
                        $data = array(
                            'id_lelang' => $id_lelang,
                            'nominal' => $nominal,
                            'user_insert' => $user_id
                        );

                        $simpan = $this->Main_Model->process_data('detail_lelang', $data);
                        if ($simpan > 0) {
                            $status = 200;
                            $message = 'Bid Barang Berhasil';
                        } else {
                            $status = 500;
                            $message = 'Gagal bid barang, ulangi beberapa saat lagi';
                        }
                    }
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }

    function list_bid()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $response = [];

            $data = $this->Lelang_Model->list_bid($id, $start, $count);
            $total_records = $this->Lelang_Model->total_bid($id);
            if ($data) {
                foreach ($data as $row) {
                    $response['bid'][] = array(
                        'nominal' => $row->nominal,
                        'profile_name' => $row->profile_name,
                        'foto' => $row->foto
                    );
                }

                $response['total_records'] = $total_records;

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Lelang.php */
/* Location: ./application/controllers/Lelang.php */
