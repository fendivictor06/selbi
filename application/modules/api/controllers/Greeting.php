<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Greeting extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('api/Greeting_Model');
	}

	function index()
	{
		$auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';
            $artis = isset($params['artis']) ? $params['artis'] : '';
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $status = isset($params['status']) ? $params['status'] : '';
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $datestart = isset($params['datestart']) ? $params['datestart'] : '';
            $dateend = isset($params['dateend']) ? $params['dateend'] : '';
            $id_user = iduser();

            $data = $this->Greeting_Model->view_greeting($keyword, $start, $count, $id_user, $artis, $status, $id, $datestart, $dateend);

            $response = [];
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $data);
        }
	}

	function add_greeting()
	{
		$auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_user = iduser();
            $id = isset($params['id']) ? $params['id'] : '';
            $id_artis = isset($params['id_artis']) ? $params['id_artis'] : '';
            $request = isset($params['request']) ? $params['request'] : '';
            $response = [];

            if ($id_artis == '' || $request == '' || $id_user == '') {
            	$status = 404;
            	$message = '';
            	if ($id_artis == '') {
            		$message .= 'Public Figure tidak ditemukan';
            	}

            	if ($request == '') {
            		$message .= 'Silahkan masukkan deskripsi pesanan anda';
            	}

            	if ($id_user == '') {
            		$message .= 'Akun tidak ditemukan';
            	}
            } else {	
            	$user = ($id == '') ? 'user_insert' : 'user_update';
            	$time = ($id == '') ? 'insert_at' : 'update_at';

            	$now = now();
            	$user_id = userid();

            	$data = array(
            		'id_user' => $id_user,
            		'id_penjual' => $id_artis,
            		'request' => $request,
            		'status' => 1,
            		$user => $user_id,
            		$time => $now
            	);

            	$simpan = $this->Main_Model->process_data('tb_greeting', $data);
            	if ($simpan > 0) {
            		$status = 200;
            		$message = 'Data berhasil disimpan';
            	} else {
            		$status = 500;
            		$message = 'Gagal menyimpan data';
            	}
            }

            print_json($status, $message, $response);
        }
	}

	function update_status()
	{
		$auth = $this->auth->check_auth_client('POST', false);
		if ($auth == true) {
			$params = get_params();
			$id = isset($params['id']) ? $params['id'] : '';
			$status_input = isset($params['status']) ? $params['status'] : '';
			$keterangan = isset($params['keterangan']) ? $params['keterangan'] : '';
			$nominal = isset($params['nominal']) ? $params['nominal'] : 0;
			$user_id = userid();
			$response = [];

			if ($id == '' || $status_input == '') {
				$status = 404;
				$message = '';

				if ($id == '') {
					$message .= 'Pesanan tidak ditemukan';
				}

				if ($status_input == '') {
					$message .= 'Status tidak ditemukan';
				}
			} else {
				# ms greeting
				$ms_greeting = $this->Main_Model->view_by_id('tb_greeting', ['id' => $id]);
				$current_status = isset($ms_greeting->status) ? $ms_greeting->status : '';

				# jika cancel order / dibatalkan
				$cancel_order = true;
				if ($status_input == 4 && ($current_status != 1 || $current_status != 2)) {
					$cancel_order = false;
				}

				# pembatalan tanpa keterangan
				$cancel_reason = true;
				if (($status_input == 4 || $status_input == 3) && $keterangan == '') {
					$cancel_reason = false;
				}

				if ($cancel_order == true && $cancel_reason == true) {
					$data = array(
						'status' => $status_input,
						'update_at' => now(),
						'user_update' => $user_id
					);

					if ($status_input == 2) {
						$data['nominal'] = $nominal;
					}

					$update = $this->Main_Model->process_data('tb_greeting', $data, ['id' => $id]);
					if ($update > 0) {
						$status = 200;
						$message = 'Data berhasil diupdate';
					} else {
						$status = 500;
						$message = 'Gagal mengupdate data';
					}
				} else {
					$status = 400;
					$message = '';

					if ($cancel_order == false) {
						$message .= 'Gagal membatalkan order';
					}

					if ($cancel_reason == false) {
						$message .= 'Mohon masukkan Keterangan ketika membatalkan / menolak order.';
					}
				}
			}

			print_json($status, $message, $response);
		}
	}
}

/* End of file Greeting.php */
/* Location: ./application/controllers/Greeting.php */ ?>