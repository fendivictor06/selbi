<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jenis extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $limit = '';
            if ($count > 0) {
                $limit[0] = $start;
                $limit[1] = $count;
            }

            # get jenis data
            $jenis = $this->Main_Model->view_by_id('ms_jenis_barang', ['status' => 1], '', "'jenis', 'ASC'", [], $limit, 'result');

            $response = [];
            if ($jenis) {
                foreach ($jenis as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'jenis' => $row->jenis
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Jenis.php */
/* Location: ./application/controllers/Jenis.php */
