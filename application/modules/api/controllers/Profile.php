<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Profile_Model');
        $this->load->model('api/Notif_Model');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('GET');
        if ($auth == true) {
            $data = $this->Profile_Model->view_profile();

            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan.';
            }

            print_json($status, $message, $data);
        }
    }

    function edit_image()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $flag = $this->input->get('flag');

            # define image path
            if ($flag == 1) {
                $upload_path = 'assets/uploads/profile/foto/';
                $base_path = base_url('assets/uploads/profile/foto/');
                $file = 'foto';
            } else {
                $upload_path = 'assets/uploads/profile/sampul/';
                $base_path = base_url('assets/uploads/profile/sampul/');
                $file = 'sampul';
            }

            $user_id = userid();
            $params = [];
            $arr_data = [];

            if (isset($_FILES['pic']['name'])) {
                $params['files'] = $_FILES['pic'];

                try {
                    move_uploaded_file($_FILES['pic']['tmp_name'], $upload_path.$_FILES['pic']['name']);

                    $exists = file_exists($upload_path.$_FILES['pic']['name']);
                    if ($exists == true) {
                        $data = array(
                            $file => $base_path.$_FILES['pic']['name']
                        );

                        $simpan = $this->Main_Model->process_data('tb_user', $data, ['uid' => $user_id]);
                        if ($simpan > 0) {
                            $status = 200;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 500;
                            $message = 'Terjadi kesalahan saat menyimpan data';
                        }
                    } else {
                        throw new Exception('Terjadi kesalahan saat mengupload file.');
                    }
                } catch (Exception $e) {
                    $status = 400;
                    $message = 'Terjadi kesalahan saat mengupload file';
                }
            } else {
                $status = 404;
                $message = 'File tidak ditemukan.';
            }

            print_json($status, $message, $arr_data);
            log_api($params, $status, $message, $arr_data);
        }
    }

    function edit_profile()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $nama = isset($params['nama']) ? $params['nama'] : '';
            $alamat = isset($params['alamat']) ? $params['alamat'] : '';
            $no_telp = isset($params['no_telp']) ? $params['no_telp'] : '';
            $user_id = userid();
            
            $data = [];

            if ($nama != '') {
                $data['profile_name'] = $nama;
            }

            if ($alamat != '') {
                $data['alamat'] = $alamat;
            }

            if ($no_telp != '') {
                $data['no_telp'] = $no_telp;
            }

            $update = $this->Main_Model->process_data('tb_user', $data, ['uid' => $user_id]);
            $status = 200;
            $message = 'Data berhasil diupdate';

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }

    function follow()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_penjual = isset($params['id_penjual']) ? $params['id_penjual'] : '';
            $user_id = userid();
            $id_user = iduser();

            if ($id_penjual == '') {
                $status = 404;
                $message = 'Penjual tidak ditemukan';
            } else {
                if ($user_id == '') {
                    $status = 401;
                    $message = 'Login dahulu untuk follow artis favorit anda';
                } else {
                    $exists = $this->Main_Model->view_by_id('tb_follower', ['id_user' => $id_penjual, 'follower' => $id_user]);
                    if (empty($exists)) {
                        $data = array(
                            'id_user' => $id_penjual,
                            'follower' => $id_user
                        );

                        $simpan = $this->Main_Model->process_data('tb_follower', $data);
                        if ($simpan > 0) {
                            $status = 200;
                            $message = 'Follow Berhasil';

                            # data follower
                            $ms_user = $this->Main_Model->view_by_id('tb_user', ['id' => $id_user], '', '', [], [], 'row');
                            $nama_follower = isset($ms_user->profile_name) ? $ms_user->profile_name : '';

                            # data penjual
                            $ms_penjual = $this->Main_Model->view_by_id('tb_user', ['id' => $id_penjual], '', '', [], [], 'row');
                            $fcm_id = isset($ms_penjual->fcm_id) ? $ms_penjual->fcm_id : '';

                            # insert untuk riwayat
                            $this->Notif_Model->create_notif($id_penjual, $id_user, $nama_follower.' telah mengikuti anda.', 'sosial');

                            $this->load->library('firebasenotif');
                            # notif nya
                            $this->firebasenotif->notif('Follower Baru', $nama_follower.' telah mengikuti anda.', $fcm_id);
                        } else {
                            $status = 500;
                            $message = 'Gagal menyimpan data';
                        }
                    } else {
                        $hapus = $this->Main_Model->delete_data('tb_follower', ['id_user' => $id_penjual, 'follower' => $id_user]);
                        if ($hapus > 0) {
                            $status = 200;
                            $message = 'Berhasil Unfollow';

                            # data follower
                            $ms_user = $this->Main_Model->view_by_id('tb_user', ['id' => $id_user], '', '', [], [], 'row');
                            $nama_follower = isset($ms_user->profile_name) ? $ms_user->profile_name : '';

                            # data penjual
                            $ms_penjual = $this->Main_Model->view_by_id('tb_user', ['id' => $id_penjual], '', '', [], [], 'row');
                            $fcm_id = isset($ms_penjual->fcm_id) ? $ms_penjual->fcm_id : '';

                            # insert untuk riwayat
                            $this->Notif_Model->create_notif($id_penjual, $id_user, $nama_follower.' telah berhenti mengikuti anda.', 'sosial');

                            $this->load->library('firebasenotif');
                            # notif nya
                            $this->firebasenotif->notif('Follower Baru', $nama_follower.' telah berhenti mengikuti anda.', $fcm_id);
                        } else {
                            $status = 500;
                            $message = 'Unfollow gagal';
                        }
                    }
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */
