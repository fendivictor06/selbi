<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pekerjaan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;

            $limit = '';
            if ($count > 0) {
                $limit[0] = $start;
                $limit[1] = $count;
            }

            # get pekerjaan data
            $pekerjaan = $this->Main_Model->view_by_id('ms_pekerjaan', ['status' => 1], '', "'pekerjaan', 'ASC'", [], $limit, 'result');

            $response = [];
            if ($pekerjaan) {
                foreach ($pekerjaan as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'pekerjaan' => $row->pekerjaan
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Pekerjaan.php */
/* Location: ./application/controllers/Pekerjaan.php */
