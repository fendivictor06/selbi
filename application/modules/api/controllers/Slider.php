<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Slider extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('GET', false);
        if ($auth == true) {
            # get slider data
            $slider = $this->Main_Model->view_by_id('ms_slider', ['status' => 1, 'flag' => 1], '', "'insert_at', 'DESC'", [], '', 'result');

            # path image
            $path = base_url('assets/uploads/slider/');

            $response = [];
            if ($slider) {
                foreach ($slider as $row) {
                    $response[] = array(
                        'image' => $path.$row->image
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Slider.php */
/* Location: ./application/controllers/Slider.php */
