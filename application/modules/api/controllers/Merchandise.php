<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Merchandise extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Merchandise_Model');
        $this->load->model('api/Produk_Model');
    }

    function index()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $kategori = isset($params['kategori']) ? $params['kategori'] : '';

            $data = $this->Merchandise_Model->view_merchandise($start, $count, $keyword, $kategori);
            $response = [];
            if ($data) {
                foreach ($data as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'category' => $row->category,
                        'jenis' => $row->jenis,
                        'nama' => $row->nama,
                        'image' => $row->path.$row->image
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data ditemukan';
            }

            
            print_json($status, $message, $response);
        }
    }

    function details()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id']) ? $params['id'] : '';

            $data = $this->Main_Model->view_by_id('view_merchandise', ['id' => $id], '', '', [], [], 'row');
            $nama = isset($data->nama) ? $data->nama : '';
            $harga = isset($data->harga) ? $data->harga : 0;
            $deskripsi = isset($data->deskripsi) ? $data->deskripsi : '';
            $response = [];

            $image = $this->Main_Model->view_by_id('tb_image_merchandise', ['id_merchandise' => $id], 'path, image', '', [], [], 'result');
            if ($data) {
                $response = array(
                    'nama' => $nama,
                    'harga' => $harga,
                    'deskripsi' => $deskripsi,
                    'image' => $image
                );

                // push attribute
                $attr = $this->Merchandise_Model->get_attribute($id);
                $response['ukuran'] = isset($attr['ukuran']) ? $attr['ukuran']:array();
                $response['warna']  = isset($attr['warna']) ? $attr['warna']:array();

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }

    function order()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_order = isset($params['id_order']) ? $params['id_order']:'';
            $id_merchandise = isset($params['id_merchandise']) ? $params['id_merchandise']:'';
            $jumlah = isset($params['jumlah']) ? $params['jumlah']:'';
            $keterangan = isset($params['warna']) ? $params['keterangan']:'';
            $harga_input = isset($params['harga']) ? $params['harga'] : 0;

            // attribute
            $ukuran = isset($params['ukuran']) ? $params['ukuran']:'';
            $warna  = isset($params['warna']) ? $params['warna']:'';

            // get merchandise 
            $get_m  = $this->Main_Model->view_by_id('ms_merchandise', array('id'=>$id_merchandise));
            $harga  = isset($get_m->harga) ? $get_m->harga:0;
            $total  = $jumlah * $harga; 

            $response = [];
            if($jumlah == '' || $jumlah == '0' || $harga_input == '0' || $harga_input == '') {
                $status = 404;
                $message = 'Lengkapi attribute dan jumlah order';
            } else {
                if ($harga_input < $harga) {
                    $status = 400;
                    $message = 'Harga Input tidak boleh kurang dari harga dasar';
                } else {
                    if ($jumlah > 100) {
                        $status = 400;
                        $message = 'Order Maksimal 100';
                    } else {

                        $userid = userid();
                        // get id user
                        $get_u  = $this->Main_Model->view_by_id('tb_user', array('uid'=>$userid));
                        $id_user= isset($get_u->id) ? $get_u->id:'';

                        $data = array(
                                    'id_user' => $id_user,
                                    'id_merchandise' => $id_merchandise,
                                    'tgl' => date('Y-m-d'),
                                    'jumlah' => $jumlah,
                                    'harga' => $harga,
                                    'total' => $total,
                                    'hargajual' => $harga_input,
                                    'keterangan' => $keterangan
                                );

                        if($id_order == '') {
                            // push
                            $data['user_insert'] = $userid;
                        } else {
                            // push
                            $data['update_at'] = now();
                            $data['user_update'] = $userid;
                        }

                        $result_id = $this->Merchandise_Model->save_order($data, $id_order);

                        if($result_id != '') {
                            // insert delete attribute
                            $this->Main_Model->delete_data('temp_order_merchandise_detail', array('id_order'=>$result_id));
                            
                            $data_attr = array(
                                            'id_order' => $result_id
                                        );

                            $jml_attr  = count($warna);
                            if(count($ukuran) > count($warna)) {
                                $jml_attr = count($ukuran);
                                for ($i=0; $i < $jml_attr; $i++) {

                                    // if eksis push value
                                    if(array_key_exists($i, $ukuran)) {
                                        $data_attr['ukuran'] = $ukuran[$i];
                                    } else {
                                        $data_attr['ukuran'] = "";
                                    }
                                    // if eksis push value
                                    if(array_key_exists($i, $warna)) {
                                        $data_attr['warna'] = $warna[$i];
                                    } else {
                                        $data_attr['warna'] = "";
                                    }

                                    $this->Main_Model->process_data('temp_order_merchandise_detail', $data_attr);
                                }
                            }


                            $status = 200;
                            $message = 'Order berhasil tersimpan';
                        } else {
                            $status = 404;
                            $message = 'Simpan order gagal';
                        }
                    }
                }
            }

            print_json($status, $message, $response);
        }
    }

    function order_detail()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_order = isset($params['id_order']) ? $params['id_order']:'';

            $response = [];
            if($id_order == '') {
                $status = 404;
                $message = 'Parameter id order tidak boleh kosong ';
            } else {
                $data = $this->Merchandise_Model->detail_order($id_order);

                if(!empty($data)) {
                    $response = $data;

                    $attr = $this->Merchandise_Model->detail_order_attribute($id_order);
                    
                    $response->ukuran = isset($attr['ukuran']) ? $attr['ukuran']:'';
                    $response->warna  = isset($attr['warna']) ? $attr['warna']:'';

                    $status = 200;
                    $message = 'Berhasil';
                } else {
                    $status = 404;
                    $message = 'Data tidak ditemukan';
                }
            }
            print_json($status, $message, $response);
        }
    }

    function list_order()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start  = isset($params['start']) ? $params['start']:0;
            $count  = isset($params['count']) ? $params['count']:0;

            $data = $this->Merchandise_Model->list_order($start, $count);

            $response = [];
            if (!empty($data)) {
                $response= $data;

                $status  = 200;
                $message = 'Berhasil';
            } else {
                $status  = 404;
                $message = 'Tidak ada data ditemukan';
            }
            
            print_json($status, $message, $response);
        }
    }

    function list_penawaran()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_order = isset($params['id_order']) ? $params['id_order']:'';
            $id_merchandise = isset($params['id_merchandise']) ? $params['id_merchandise']:'';

            $userid = userid();
            // get id user
            $get_u  = $this->Main_Model->view_by_id('tb_user', array('uid'=>$userid));
            $id_user= isset($get_u->id) ? $get_u->id:'';

            $data = $this->Merchandise_Model->list_penawaran($id_order, $id_merchandise, $id_user);

            $response = [];
            if (!empty($data)) {
                $response= $data;

                $status  = 200;
                $message = 'Berhasil';
            } else {
                $status  = 404;
                $message = 'Tidak ada data ditemukan';
            }
            
            print_json($status, $message, $response);
        }
    }

    function proses_penawaran()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_penawaran = isset($params['id_penawaran']) ? $params['id_penawaran'] : ''; 
            $statusx = isset($params['status']) ? $params['status'] : '';

            $userid = userid();
            $id_user = iduser();
            $response = [];

            if (empty($id_penawaran) || $statusx == '') {
                $status = 404;
                $message = '';
                if (empty($id_penawaran)) {
                    $message = 'Id Penawaran tidak ditemukan'; 
                }   

                if ($statusx == '') {
                    $message = 'Status tidak ditemukan';
                }
            } else {
                $update = $this->Merchandise_Model->proses_penawaran($id_penawaran, $statusx);

                if ($update) {
                    $status = 200;
                    $message = 'Data berhasil diupdate';
                } else {
                    $status = 500;
                    $message = 'Gagal mengupdate data';
                }
            }

            print_json($status, $message, $response);
            log_api($params, $status, $message, $response);
        }
    }

    function proses_penawaran20190426()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id = isset($params['id_penawaran']) ? $params['id_penawaran']:'';
            $status = isset($params['status']) ? $params['status']:'';

            $userid = userid();
            // get id user
            $get_u  = $this->Main_Model->view_by_id('tb_user', array('uid'=>$userid));
            $id_user= isset($get_u->id) ? $get_u->id:'';

            $response = [];
            if($id == '' || $id == '0' || $status == '') {
                $status  = 404;
                $message = 'Parameter ID & status masih kosong';
            } else {
                $message = '';
                // get penawaran
                $get = $this->Main_Model->view_by_id('temp_penawaran', array('id'=>$id));
                $id_order = isset($get->id_order) ? $get->id_order:'';
                $id_merchandise = isset($get->id_merchandise) ? $get->id_merchandise:'';

                $data_tawar = array(
                                'status' => $status,
                                'update_at' => now(),
                                'user_update'=> $userid
                            );

                if($status == '3') {
                    $message = 'Penawaran telah di terima, barang merchandise telah menjadi koleksi anda';
                } else {
                    $message = 'Penolakan penawaran berhasil tersimpan, silahkan tunggu penawaran selanjutnya';
                }

                // update status temp penawaran
                $this->Main_Model->process_data('temp_penawaran', $data_tawar, array('id'=> $id));

                // jika di setujui pembelian
                if($status == '3') {                    
                    // get n update order
                    $get_temp_o = $this->Main_Model->view_by_id('temp_order_merchandise', array('id'=> $id_order));
                    if(!empty($get_temp_o)) {
                        $data_order = array(
                                'status' => 3,
                                'update_at' => now(),
                                'user_update'=> $userid
                            );

                        $this->Main_Model->process_data('temp_order_merchandise', $data_order, array('id'=> $id_order));
                    }

                    // get merchandise
                    $get_m  = $this->Main_Model->view_by_id('ms_merchandise', array('id'=>$id_merchandise));
                    $data_m =   array(
                            'id_jenis' => '2',
                            'id_category' => isset($get_m->id_category) ? $get_m->id_category:'',
                            'id_penjual' => $id_user,
                            'id_brand' => isset($get_m->id_brand) ? $get_m->id_brand:'',
                            'kode' => isset($get_m->id_brand) ? $get_m->kode:'',
                            'nama' => isset($get_m->nama) ? $get_m->nama:'',
                            'deskripsi' => isset($get_m->deskripsi) ? $get_m->deskripsi:'',
                            'harga' => isset($get_temp_o->hargajual) ? $get_temp_o->hargajual:'0',
                            'diskon' => isset($get_m->diskon) ? $get_m->diskon:'0',
                            'berat' => isset($get_m->berat) ? $get_m->berat:'0',
                            'berat_satuan' => isset($get_m->berat_satuan) ? $get_m->berat_satuan:'',
                            'kondisi' => isset($get_m->kondisi) ? $get_m->kondisi:'',
                            'rating' => isset($get_m->kondisi) ? $get_m->rating:'',
                            'lelang' => '0',
                            'status' => '1',
                            'insert_at' => now(),
                            'user_insert' => $userid
                        );

                    $simpan_merchandise = $this->Main_Model->process_data('ms_barang', $data_m);

                    // get img penawaran
                    $get_img = $this->Main_Model->view_by_id('temp_image_penawaran', array('id_penawaran'=>$id), '*', '', '', '', 'result');
                    if(!empty($get_img)) {
                        $data_img = array(
                                        'id_barang' => $simpan_merchandise,
                                        'insert_at' => now(),
                                        'user_insert' => $userid
                                    );

                        foreach ($get_img as $row => $val) {
                            if($row == '0') {
                                $data_img['main'] = '1';
                            } else {
                                $data_img['main'] = '0';
                            }
                            // old file & directory
                            $ex_name  = explode(".", $val->image);
                            $extension= $ex_name[1];
                            $old_file = $val->path.$val->image;

                            // new file & directory
                            $new_name = 'merchandise_'.rand().time().'.'.$extension;
                            $new_file = './assets/uploads/product/'.$new_name;
                            // push image value
                            $data_img['image'] = $new_name;

                            if(!copy($old_file, $new_file)) {
                                $message = 'Terjadi kesalahan memuat data.';
                            }
                            // insert detail image 
                            $this->Main_Model->process_data('tb_image_barang', $data_img);
                        }
                    }
                }

                $status  = 200;
            }

            print_json($status, $message, $response);
        }
    }

    function batal_order_merchandise()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $id_order = isset($params['id_order']) ? $params['id_order']:'';

            $userid = userid();
            // get id user
            $get_u  = $this->Main_Model->view_by_id('tb_user', array('uid'=>$userid));
            $id_user= isset($get_u->id) ? $get_u->id:'';

            $response = [];
            if($id_order == '') {
                $status  = 404;
                $message = 'Parameter ID masih kosong';
            } else {
                $data = array(
                            'status' => 4,
                            'update_at' => now(),
                            'user_update'=> $userid
                        );

                $this->Main_Model->process_data('temp_order_merchandise', $data, array('id'=>$id_order));

                $status  = 200;
                $message = 'Order berhasil di batalkan';
            }

            print_json($status, $message, $response);
        }
    }

    function suggestion()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $start = isset($params['start']) ? $params['start'] : 0;
            $count = isset($params['count']) ? $params['count'] : 0;
            $keyword = isset($params['keyword']) ? $params['keyword'] : '';
            $id_user = iduser();

            $data = $this->Merchandise_Model->view_suggestion($start, $count, $keyword, $id_user);
            $response = [];
            if ($data) {
                foreach ($data as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'category' => $row->category,
                        'jenis' => $row->jenis,
                        'nama' => $row->nama,
                        'image' => $row->path.$row->image
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Tidak ada data ditemukan';
            }

            
            print_json($status, $message, $response);
        }
    }

    function upload_merchandise()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            # define image path
            $upload_path = 'assets/uploads/product/';

            $user_id = userid();
            $params = [];
            $arr_data = [];

            if (isset($_FILES['pic']['name'])) {
                $params['files'] = $_FILES['pic'];

                try {
                    move_uploaded_file($_FILES['pic']['tmp_name'], $upload_path.$_FILES['pic']['name']);

                    $exists = file_exists($upload_path.$_FILES['pic']['name']);
                    if ($exists == true) {
                        $data = array(
                            'id_barang' => '',
                            'path' => base_url('assets/uploads/product/'.$_FILES['pic']['name']),
                            'image' => $_FILES['pic']['name'],
                            'main' => '0',
                            'user_insert' => $user_id
                        );

                        $simpan = $this->Main_Model->process_data('tb_image_barang', $data);
                        if ($simpan > 0) {
                            $arr_data = ['id' => $simpan];

                            $status = 200;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 500;
                            $message = 'Terjadi kesalahan saat menyimpan data';
                        }
                    } else {
                        throw new Exception('Terjadi kesalahan saat mengupload file.');
                    }
                } catch (Exception $e) {
                    $status = 400;
                    $message = 'Terjadi kesalahan saat mengupload file';
                }
            } else {
                $status = 404;
                $message = 'File tidak ditemukan.';
            }

            print_json($status, $message, $arr_data);
            log_api($params, $status, $message, $arr_data);
        }
    }

    function simpan_merchandise()
    {
        $auth = $this->auth->check_auth_client('POST', false);
        if ($auth == true) {
            $params = get_params();
            $gallery = isset($params['gallery']) ? $params['gallery'] : [];
            $id_order = isset($params['id_order']) ? $params['id_order'] : '';
            $foto = isset($params['foto']) ? $params['foto'] : '';

            $order = $this->Merchandise_Model->merchandise_order($id_order);

            $nama = isset($order->nama) ? $order->nama : '';
            $harga = isset($order->harga_jual) ? $order->harga_jual : 0;
            $berat = isset($order->berat) ? $order->berat : 0;
            $satuan_berat = isset($order->berat_satuan) ? $order->berat_satuan : 0;
            $deskripsi = isset($order->deskripsi) ? $order->deskripsi : '';
            $ukuran = '';

            $kondisi = isset($order->kondisi) ? $order->kondisi : '';
            $brand = isset($order->id_brand) ? $order->id_brand : '';
            $kategori = isset($order->id_category) ? $order->id_category : '';
            $jenis = isset($order->id_jenis) ? $order->id_jenis : '';
            $pemakaian = '';
            $satuan_pemakaian = '';
            $lelang = 0;
            $donasi = 0;
            $stok = isset($order->jumlah) ? $order->jumlah : 0;

            $id_user = iduser();
            $user_id = userid();

            if ($id_order == '') {
                $status = 404;
                $message = 'Order tidak ditemukan';
            } else {
                $data[] = [$kategori, $id_user, $brand, '', $nama, $deskripsi, $harga, '0', $berat, $satuan_berat, $kondisi, $user_id, $lelang, $jenis, $pemakaian, $satuan_pemakaian, $donasi, '', '', ''];

                $simpan = $this->Produk_Model->add_produk($data, $foto, $gallery, $nama, $lelang, '', '', $harga, $id_order, $stok);
                if ($simpan > 0) {
                    $response['id_barang'] = $simpan;

                    $status = 200;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 500;
                    $message = 'Terjadi kesalahan saat menyimpan data, ulangi beberapa saat lagi.';
                }
            }

            print_json($status, $message, $response);
            log_api($params, $status, $message, $response);
        }
    }
}

/* End of file Merchandise.php */
/* Location: ./application/controllers/Merchandise.php */
