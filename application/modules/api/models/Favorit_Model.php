<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Favorit_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_favorit($keyword = '', $start = 0, $count = 0)
    {
        $keyword = $this->db->escape_str($keyword);
        $start = $this->db->escape_str($start);
        $count = $this->db->escape_str($count);

        $id_user = iduser();

        $condition = '';
        if ($keyword != '') {
            $condition = " AND c.nama LIKE '%$keyword%' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        return $this->db->query("
    		SELECT a.*, c.`nama` AS barang, c.`harga`, c.`rating`, 
			d.`profile_name` AS penjual, e.image, d.foto
			FROM tb_favorit a
			INNER JOIN tb_user b ON a.`id_user` = b.`id`
			INNER JOIN ms_barang c ON c.`id` = a.`id_barang`
			INNER JOIN tb_user d ON d.`id` = c.`id_penjual`
			LEFT JOIN tb_image_barang e ON e.id_barang = a.id_barang
			WHERE a.id_user = '$id_user'
			AND e.main = 1
			$condition 
			$limit ")->result();
    }
}

/* End of file Favorit_Model.php */
/* Location: ./application/models/Favorit_Model.php */
