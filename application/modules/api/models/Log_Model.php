<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Log_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function log($request = '', $response = '', $user = '')
    {
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        $data = array(
            'api' => $class.'/'.$method,
            'request' => $request,
            'response' => $response,
            'user' => $user
        );

        $this->db->insert('log_trx_api', $data);
    }
}

/* End of file Log_Model.php */
/* Location: ./application/models/Log_Model.php */
