<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Feed_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function view_feed($jenis = '', $id = '', $id_penjual = '', $start = 0, $count = 0)
    {
        $condition = '';
        if ($jenis != '') {
            $condition .= " AND a.jenis = '$jenis' ";
        }

        if ($id != '') {
            $condition .= " AND a.id = '$id' ";
        }

        if ($id_penjual != '') {
            $condition .= " AND a.id_penjual = '$id_penjual' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        $query = $this->db->query("
            SELECT a.*, c.profile_name AS penjual, c.foto AS image_penjual, c.id AS id_penjual
            FROM tb_feed a
            INNER JOIN tb_user c ON c.id = a.id_penjual
            WHERE 1 = 1
            AND c.penjual = 1
            AND c.status = 1
            $condition 
            ORDER BY a.insert_at DESC
            $limit ")->result();

        $response = [];
        if ($query) {
            $col = 0;
            foreach ($query as $row => $val) {
                $response[$row] = array(
                    'id' => $val->id,
                    'jenis' => $val->jenis,
                    'id_penjual' => $val->id_penjual,
                    'penjual' => $val->penjual,
                    'image_penjual' => $val->image_penjual,
                    'timestamp' => $val->insert_at,
                    'tgl' => isset($val->tgl) ? $val->tgl : '',
                    'tempat' => isset($val->tempat) ? $val->tempat : '',
                    'deskripsi' => isset($val->deskripsi) ? $val->deskripsi : '',
                    'title' => $val->judul,
                    'lelang' => $val->lelang
                );

                if ($val->jenis == 1) {
                    $path = path_kegiatan();
                } elseif ($val->jenis == 2) {
                    $path = path_gallery();
                } elseif ($val->jenis == 4 || $val->jenis == 5) {
                    $path = path_produk();
                } else {
                    $path = '';
                }

                $images = $this->db->query("
                    SELECT a.*, b.lelang, c.id AS id_lelang, d.jenis
                    FROM tb_feed_image a 
                    LEFT JOIN ms_barang b ON a.id_barang = b.id 
                    LEFT JOIN tb_lelang c ON c.id_barang = b.id 
                    LEFT JOIN ms_jenis_barang d ON d.id = b.id_jenis
                    WHERE id_feed = '$val->id' ")->result();

                if ($images) {
                    foreach ($images as $img) {
                        $image = ($img->image != '') ? $path.$img->image : '';
                        $response[$row]['images'][] = array(
                            'id_barang' => $img->id_barang,
                            'id_lelang' => isset($img->id_lelang) ? $img->id_lelang : '',
                            'teks' => $img->teks,
                            'jenis' => isset($img->jenis) ? $img->jenis : '',
                            'image' => $image,
                            'lelang' => isset($img->lelang) ? $img->lelang : 0
                        );
                    }
                }

                $col++;
            }
        }

        return $response;
    }

    function view_feed20181121($jenis = '', $id = '', $id_penjual = '', $start = 0, $count = 0)
    {
        $condition = '';
        if ($jenis != '') {
            $condition .= " AND a.jenis = '$jenis' ";
        }

        if ($id != '') {
            $condition .= " AND a.id = '$id' ";
        }

        if ($id_penjual != '') {
            $condition .= " AND a.id_penjual = '$id_penjual' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        $query = $this->db->query("
    		SELECT a.*, c.nama AS penjual, c.image AS image_penjual, c.id AS id_penjual
			FROM tb_feed a
			INNER JOIN ms_penjual c ON c.id = a.id_penjual
			WHERE 1 = 1
			$condition 
			ORDER BY a.insert_at DESC
            $limit ")->result();

        $response = [];
        if ($query) {
            foreach ($query as $row => $val) {
                $image_penjual = ($val->image_penjual != '') ? path_pelapak().$val->image_penjual : '';
                $response[$row] = array(
                    'id' => $val->id,
                    'jenis' => $val->jenis,
                    'id_penjual' => $val->id_penjual,
                    'penjual' => $val->penjual,
                    'image_penjual' => $image_penjual,
                    'timestamp' => $val->insert_at,
                    'title' => $val->judul
                );

                $path = ($val->jenis == 1) ? path_kegiatan() : path_gallery();

                $images = $this->db->query("
    				SELECT * 
    				FROM tb_feed_image 
    				WHERE id_feed = '$val->id' ")->result();

                if ($images) {
                    foreach ($images as $img) {
                        $image = ($img->image != '') ? $path.$img->image : '';
                        $response[$row]['images'][] = array(
                            'image' => $image
                        );
                    }
                }
            }
        }

        return $response;
    }

    function view_gallery($id = '', $id_penjual = '')
    {
        $response = [];
        $path = path_gallery();
        if ($id != '') {
            $query = $this->db->query("
                        SELECT a.*, b.id_penjual, DATE(a.insert_at) AS tgl_input
                        FROM tb_feed_image a
                        INNER JOIN tb_feed b ON a.id_feed = b.id
                        WHERE 1 = 1
                        AND b.jenis = '2'
                        AND a.id_feed = '$id'
                        ORDER BY a.insert_at DESC ")->result();
            if ($query) {
                foreach ($query as $val) {
                    $image = ($val->image != '') ? $path.$val->image : '';

                    $response[] = array(
                        'id' => $val->id,
                        'id_penjual' => $val->id_penjual,
                        'id_feed' => $val->id_feed,
                        'image' => $image
                    );
                }
            }
        } else {
            $group = $this->db->query("
                SELECT DATE(a.`insert_at`) AS tgl
                FROM tb_feed a 
                WHERE a.`id_penjual` = '$id_penjual'
                AND a.jenis = '2'
                GROUP BY DATE(a.`insert_at`) ")->result();

            if ($group) {
                foreach ($group as $row => $key) {
                    $query = $this->db->query("
                        SELECT a.*, b.id_penjual, DATE(a.insert_at) AS tgl_input
                        FROM tb_feed_image a
                        INNER JOIN tb_feed b ON a.id_feed = b.id
                        WHERE 1 = 1
                        AND b.jenis = '2'
                        AND b.id_penjual = '$id_penjual'
                        AND DATE(b.insert_at) = '$key->tgl'
                        ORDER BY a.insert_at DESC ")->result();

                    $response[$row]['tanggal'] = $key->tgl;

                    if ($query) {
                        foreach ($query as $val) {
                            $image = ($val->image != '') ? $path.$val->image : '';

                            $response[$row]['gallery'][] = array(
                                'id' => $val->id,
                                'id_penjual' => $val->id_penjual,
                                'id_feed' => $val->id_feed,
                                'image' => $image
                            );
                        }
                    }
                }
            }
        }

        return $response;
    }

    function view_kegiatan($id = '', $id_penjual = '')
    {
        $condition = '';
        if ($id != '') {
            $condition .= " AND a.id = '$id' ";
        }

        if ($id_penjual != '') {
            $condition .= " AND a.id_penjual = '$id_penjual' ";
        }

        $query = $this->db->query("
            SELECT a.*
            FROM tb_feed a 
            WHERE 1 = 1
            AND a.jenis = '1'
            $condition 
            ORDER BY a.insert_at DESC ")->result();

        $response = [];
        if ($query) {
            foreach ($query as $row => $val) {
                $response[$row] = array(
                    'id' => $val->id,
                    'id_penjual' => $val->id_penjual,
                    'judul' => $val->judul,
                    'tgl' => $val->tgl,
                    'tempat' => $val->tempat,
                    'deskripsi' => $val->deskripsi
                );
            }
        }

        return $response;
    }

    function random_barang()
    {
        $result = [];
        $random_penjual = $this->db->query("
            SELECT *
            FROM tb_user a 
            WHERE a.penjual = 1
            AND a.status = 1
            ORDER BY RAND()
            LIMIT 1 ")->row();

        $id_penjual = isset($random_penjual->id) ? $random_penjual->id : '';
        $profile_name = isset($random_penjual->profile_name) ? $random_penjual->profile_name : '';
        $image_penjual = isset($random_penjual->foto) ? $random_penjual->foto : '';

        if ($id_penjual != '') {
            $result = array(
                'id' => '',
                'jenis' => 4,
                'id_penjual' => $id_penjual,
                'penjual' => $profile_name,
                'image_penjual' => $image_penjual,
                'timestamp' => now(),
                'title' => ''
            );

            $barang = $this->db->query("
                SELECT a.*, b.image
                FROM ms_barang a
                INNER JOIN tb_image_barang b ON a.id = b.id_barang
                WHERE a.`id_penjual` = '$id_penjual'
                AND a.status = '1'
                AND b.main = '1'
                ORDER BY a.`insert_at` DESC
                LIMIT 4 ")->result();

            if ($barang) {
                foreach ($barang as $row) {
                    $result['barang'][] = array(
                        'id' => $row->id,
                        'barang' => $row->nama,
                        'image' => $row->image
                    );
                }
            }
        }

        return $result;
    }
}

/* End of file Feed_Model.php */
/* Location: ./application/models/Feed_Model.php */
