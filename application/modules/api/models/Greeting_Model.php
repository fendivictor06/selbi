<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Greeting_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function view_greeting($keyword = '', $start = 0, $count = 0, $id_user = '', $artis = '', $status = '', $id = '', $datestart = '', $dateend = '')
	{
		$keyword = $this->db->escape_str($keyword);
		$start = $this->db->escape_str($start);
		$count = $this->db->escape_str($count);
		$artis = $this->db->escape_str($artis);
		$status = $this->db->escape_str($status);
		$id = $this->db->escape_str($id);
		$id_user = $this->db->escape_str($id_user);
		$datestart = $this->db->escape_str($datestart);
		$dateend = $this->db->escape_str($dateend);

		$condition = '';
		$limit = '';

		if ($keyword != '' && $artis != '') {
			$condition .= " AND b.profile_name LIKE '%$keyword%' ";
		}

		if ($keyword != '' && $id_user != '') {
			$condition .= " AND c.profile_name LIKE '%$keyword%' ";
		}

		if ($count > 0) {
			$limit .= " LIMIT $start, $count ";
		}

		if ($id_user != '') {
			$condition .= " AND b.id = '$id_user' ";
		}

		if ($artis != '') {
			$condition .= " AND c.id = '$artis' ";
		}

		if ($status != '') {
			$condition .= " AND a.status = '$status' ";
		}

		if ($id != '') {
			$condition .= " AND a.id = '$id' ";
		}

		if ($datestart != '' && $dateend != '') {
			$condition .= " AND DATE(a.insert_at) = BETWEEN '$datestart' AND '$dateend' ";
		}

		$query = $this->db->query("
			SELECT a.*, b.`profile_name` AS pemesan, c.`profile_name` AS artis,
			status_greeting(a.`status`) AS status_order, b.foto AS foto_pemesan,
			b.foto AS foto_artis
			FROM tb_greeting a
			INNER JOIN tb_user b ON a.`id_user` = b.`id` 
			INNER JOIN tb_user c ON c.id = a.`id_penjual`
			WHERE 1 = 1
			$condition 
			$limit ");

		if ($id == '') {
			return $query->result();
		} else {
			return $query->row();
		}
	}
}

/* End of file Greeting_Model.php */
/* Location: ./application/models/Greeting_Model.php */ ?>