<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_Model extends CI_Model
{
	
	function list_chat($start, $count)
	{
		$user_id = userid();
		return $this->db->query("SELECT * FROM (
										SELECT c.from, u.profile_name, u.foto, c.to AS uid, IFNULL(d.lastdate,'') AS lastdate,
											IFNULL(d.lastmessage,'') AS lastmessage, IFNULL(m.baru,0) AS baru
											FROM tb_chat c
											JOIN tb_user u ON c.from=u.uid 
											LEFT JOIN (
												SELECT `uniq_chat`, `from`, message AS lastmessage, 
													tanggal  AS lastdate 
													FROM tb_chat 
													ORDER BY id DESC
											) d ON c.uniq_chat=d.uniq_chat
											LEFT JOIN (
												SELECT `from`, COUNT(*) AS baru
													FROM tb_chat
													WHERE is_open=0 
													AND `to`='$user_id'
													GROUP BY `from`
											) m ON c.from=m.from
											WHERE c.`to`='$user_id'
											GROUP BY c.`from`
									) l ORDER BY lastdate ASC
									LIMIT $start,$count")->result();
	}

	function detail_chat($from='', $start, $count)
	{
		$user_id = userid();
		return $this->db->query("SELECT * FROM (
									SELECT * FROM (
											SELECT f.id AS id_chat, f.from, f.to, f.message, f.tanggal, f.timestamp 
												FROM tb_chat f
												WHERE f.from='$user_id'
												AND f.to='$from'
											UNION ALL
											SELECT f.id AS id_chat, f.from, f.to, f.message, f.tanggal, f.timestamp 
												FROM tb_chat f
												WHERE f.to='$user_id'
												AND f.from='$from'
										) chat
										ORDER BY id_chat DESC
										LIMIT $start,$count
									) s ORDER BY id_chat ASC
									")->result();
	}

	function update_chat_open($from='')
	{
		$user_id = userid();
		$data = array('is_open'=>'1');
		$condition = array(
						'from' => $from,
						'to' => $user_id
					);
		$this->db->query("UPDATE tb_chat SET is_open='1' 
							WHERE (`from` = '$from' AND `to`='$user_id' )
							OR (`from` = '$user_id' AND `to`='$from' )");
	}

	function check_eksis_chat($from='', $to='')
	{
		return $this->db->query("SELECT * FROM tb_chat
									WHERE (`from`='$from' AND `to`='$to') 
										OR (`from`='$to' AND `to`='$from')")->row();
	}
	
	function save_chat($data)
	{		
		$hasil = '';
		$this->db->insert('tb_chat', $data);
		
		if($this->db->affected_rows() >= 1) {
			$hasil = $this->db->insert_id();
		} else {
			$hasil = '';
		}	
		
		return $hasil;
	}
	
	function delete_chat($id = '')
	{	
		$q = $this->db->where('id', $id)->delete('tb_chat');
		
		if($this->db->affected_rows() > 0){
			return true;	
		}else{
			return false;
		}
	}

	function send_notif_chat($params)
	{
		$uid_from = isset($params['from']) ? $params['from']:'';
		$uid_to   = isset($params['to']) ? $params['to']:'';

		$get = $this->db->query("SELECT * FROM tb_user 
									WHERE uid='$uid_to'")->row();
		
		$get_from = $this->db->query("SELECT * FROM tb_user 
										WHERE uid='$uid_from'")->row();

		if(!empty($get)) {
			$name_from = isset($get_from->profile_name) ? $get_from->profile_name:'';
			
			$title = 'Ada pesan baru untuk anda dari '.$name_from;
            $to    = isset($get->fcm_id) ? $get->fcm_id:'';

            $data = [
                [
                    'title' => $title,
                    'body' => '',
                    'type' => 'notif_chat'
                ]
            ];

            $this->firebasenotif->notif($title, '', $to, '', $data, '', 'high');
		}
	}
}