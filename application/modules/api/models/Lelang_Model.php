<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lelang_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function header_sale()
    {
        return $this->db->query("
    		SELECT *
			FROM tb_flashsale a
			WHERE NOW() BETWEEN a.`start` AND a.`end`")->row();
    }

    function detail_sale($id = '')
    {
        return $this->db->query("
    		SELECT b.*, c.`nama`, IFNULL(d.image, '') AS image
			FROM tb_flashsale a
			INNER JOIN detail_flashsale b ON a.`id` = b.`id_flashsale`
			INNER JOIN ms_barang c ON c.`id` = b.`id_barang`
            INNER JOIN tb_image_barang d ON d.id_barang = c.id
			WHERE a.`id` = '$id'
            AND d.main = 1")->result();
    }

    function view_lelang($id = '', $id_penjual = '', $keyword = '', $kategori = '', $start = 0, $count = 0)
    {
        $condition = '';
        if ($id != '') {
            $condition .= " AND a.id = '$id' ";
        }

        if ($id_penjual != '') {
            $condition .= " AND b.id_penjual = '$id_penjual' ";
        }

        if ($keyword != '') {
            $condition .= " AND b.nama LIKE '%$keyword%' ";
        }

        if ($kategori != '') {
            $condition .= " AND d.id = '$kategori' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        $query = $this->db->query("
            SELECT a.*, b.`nama`, c.`image`, b.deskripsi, b.id_penjual,
            CASE WHEN (b.kondisi = 1)
                THEN 'BARU'
                ELSE 'BEKAS'
            END AS kondisi,
            b.berat, b.berat_satuan,
            d.category, e.brand, b.rating, f.profile_name, f.rating, f.foto,
            b.donasi
            FROM tb_lelang a
            INNER JOIN ms_barang b ON a.`id_barang` = b.`id`
            INNER JOIN tb_image_barang c ON c.`id_barang` = b.`id`
            LEFT JOIN ms_category d ON d.id = b.id_category
            LEFT JOIN ms_brand e ON e.id = b.id_brand
            LEFT JOIN tb_user f ON f.id = b.id_penjual
            WHERE NOW() BETWEEN a.`start` AND a.`end`
            AND c.`main` = 1
            AND b.lelang = 1
            $condition 
            $limit ");

        if ($id != '') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    function list_bid($id = '', $start = 0, $count = 0)
    {
        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        return $this->db->query("
            SELECT a.`nominal`, b.`profile_name`, b.foto
            FROM detail_lelang a
            INNER JOIN tb_user b ON a.`user_insert` = b.`uid`
            WHERE a.`id_lelang` = '$id'
            $limit ")->result();
    }

    function total_bid($id = '')
    {
        $query = $this->db->query("
            SELECT COUNT(a.id) AS jumlah
            FROM detail_lelang a
            INNER JOIN tb_user b ON a.`user_insert` = b.`uid`
            WHERE a.`id_lelang` = '$id' ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }
}

/* End of file Lelang_Model.php */
/* Location: ./application/models/Lelang_Model.php */
