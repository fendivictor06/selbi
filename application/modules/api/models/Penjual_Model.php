<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Penjual_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function is_update_random()
    {
        $query = $this->db->query("
            SELECT IFNULL(TIMESTAMPDIFF(MINUTE, MAX(update_urutan), NOW()), 0) AS waktu
            FROM tb_user ")->row();

        return isset($query->waktu) ? $query->waktu : 0;
    }

    function random_urutan($jumlah = 0)
    {
        $random = rand(1, $jumlah);
        $check = $this->db->where('penjual', 1)
                        ->where('urutan', $random)
                        ->get('tb_user')
                        ->row();

        if ($check) {
            return $this->random_urutan($jumlah);
        } else {
            return $random;
        }
    }

    function view_penjual($id = '', $start = 0, $count = 0, $keyword = '', $pekerjaan = '')
    {
        $condition = '';
        if ($id != '') {
            $id = $this->db->escape_str($id);
            $id_user = iduser();
            $user_id = userid();
            $condition .= " AND a.id = '$id' ";

            # log 
            $exists_log = $this->Main_Model->view_by_id('counter_artis', [
                'id_user' => $id_user, 
                'id_penjual' => $id
            ]);
            if ($exists_log) {
                $this->Main_Model->process_data('counter_artis', [
                    'update_at' => now(), 
                    'user_update' => $user_id
                ], [
                    'id_user' => $id_user, 
                    'id_penjual' => $id
                ]);
            } else {
                $this->Main_Model->process_data('counter_artis', [
                    'id_user' => $id_user, 
                    'id_penjual' => $id, 
                    'user_insert' => $user_id
                ]);
            }
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        if ($id == '' && $keyword != '') {
            $condition .= " AND a.profile_name LIKE '%$keyword%' ";
        }

        if ($pekerjaan != '') {
            $condition .= " AND a.id_pekerjaan = '$pekerjaan' ";
        }

        return $this->db->query("
            SELECT a.`id`, a.`profile_name` AS nama, a.`foto` AS image,
            a.`deskripsi`, DATE(a.`insert_at`) AS join_at, b.`kota`,
            a.id_pekerjaan, c.pekerjaan
            FROM tb_user a
            LEFT JOIN ms_kota b ON a.`id_kota` = b.`id`
            LEFT JOIN ms_pekerjaan c ON c.id = a.id_pekerjaan
            WHERE a.`penjual` = 1
            AND a.`status` = 1
            $condition 
            ORDER BY a.urutan ASC
            $limit ")->result();
    }

    function view_penjual20181121($id = '', $start = 0, $count = 0)
    {
        $condition = '';
        if ($id != '') {
            $id = $this->db->escape_str($id);
            // $id = dekrip_id($id);
            $condition = " AND a.id = '$id' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        return $this->db->query("
    		SELECT a.`id`, a.`nama`, a.`image`, a.`deskripsi`,
    		a.join_at, b.kota
			FROM ms_penjual a
			INNER JOIN ms_kota b ON a.id_kota = b.id
			WHERE a.`status` = 1
			$condition 
            $limit ")->result();
    }

    function total_penjual20181121()
    {
        $query = $this->db->query("
            SELECT COUNT(a.id) AS jumlah 
            FROM ms_penjual a
            INNER JOIN ms_kota b ON a.id_kota = b.id
            WHERE a.`status` = 1")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function total_penjual()
    {
        $query = $this->db->query("
            SELECT COUNT(a.`id`) AS jumlah
            FROM tb_user a
            LEFT JOIN ms_kota b ON a.`id_kota` = b.`id`
            WHERE a.`penjual` = 1
            AND a.`status` = 1 ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function view_following($id = '', $start = 0, $count = 0, $keyword = '', $pekerjaan = '')
    {
        $condition = '';
        if ($id != '') {
            $id = $this->db->escape_str($id);
            $condition .= " AND a.id = '$id' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        if ($id == '' && $keyword != '') {
            $condition .= " AND b.profile_name LIKE '%$keyword%' ";
        }

        if ($pekerjaan != '') {
            $condition .= " AND b.id_pekerjaan = '$pekerjaan' ";
        }

        $id_user = iduser();

        return $this->db->query("
            SELECT b.`id`, b.`profile_name` AS nama, b.`foto` AS image,
            b.`deskripsi`, DATE(b.`insert_at`) AS join_at, c.`kota`,
            b.id_pekerjaan, d.pekerjaan
            FROM tb_follower a 
            INNER JOIN tb_user b ON a.`id_user` = b.`id`
            LEFT JOIN ms_kota c ON c.`id` = b.`id_kota`
            LEFT JOIN ms_pekerjaan d ON d.id = b.id_pekerjaan
            WHERE a.`follower` = '$id_user'
            AND b.penjual = '1'
            AND b.`status` = 1
            $condition 
            $limit ")->result();
    }

    function view_follower($id = '', $start = 0, $count = 0, $keyword = '', $pekerjaan = '')
    {
        $condition = '';
        if ($id != '') {
            $id = $this->db->escape_str($id);
            $condition .= " AND a.id = '$id' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        if ($id == '' && $keyword != '') {
            $condition .= " AND b.profile_name LIKE '%$keyword%' ";
        }

        if ($pekerjaan != '') {
            $condition .= " AND b.id_pekerjaan = '$pekerjaan' ";
        }

        $id_user = iduser();

        return $this->db->query("
            SELECT b.`id`, b.`profile_name` AS nama, b.`foto` AS image,
            b.`deskripsi`, DATE(b.`insert_at`) AS join_at, c.`kota`,
            b.id_pekerjaan, d.pekerjaan
            FROM tb_follower a 
            INNER JOIN tb_user b ON a.`id_user` = b.`id`
            LEFT JOIN ms_kota c ON c.`id` = b.`id_kota`
            LEFT JOIN ms_pekerjaan d ON d.id = b.id_pekerjaan
            WHERE a.`id_user` = '$id_user'
            AND b.penjual = '1'
            AND b.`status` = 1
            $condition 
            $limit ")->result();
    }
}

/* End of file Penjual_Model.php */
/* Location: ./application/models/Penjual_Model.php */
