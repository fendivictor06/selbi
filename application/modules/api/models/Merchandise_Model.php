<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Merchandise_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_merchandise($start = 0, $count = 0, $keyword = '', $kategori = '')
    {
        $condition = '';
        if ($keyword != '') {
            $condition .= " AND nama LIKE '%$keyword%' ";
        }

        if ($kategori != '') {
            $condition .= " AND id_category = '$kategori' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        return $this->db->query("
    		SELECT *
    		FROM view_merchandise 
    		WHERE 1 = 1
    		$condition 
    		$limit ")->result();
    }

    function get_attribute($id_merchandise='')
    {
        $result = array();
        // get attribute merchandise
        $arr_attr = $this->db->query("SELECT d.*, a.attribute 
                                            FROM ms_merchandise_detail d
                                            JOIN ms_attribute a ON d.`id_attribute`=a.`id`
                                            WHERE d.id_merchandise='$id_merchandise'
                                            GROUP BY d.id_attribute")->result();
        if(!empty($arr_attr)) {
            foreach ($arr_attr as $row => $val) {
                $attr_name = strtolower($val->attribute);
                // get data attribute merchandise
                $data_attr = $this->db->query("SELECT d.`id_merchandise`, d.`id_attribute`, a.`attribute`, d.`detail_attribute` 
                                                    FROM ms_merchandise_detail d
                                                    JOIN ms_attribute a ON d.`id_attribute`=a.`id`
                                                    WHERE a.`status`=1
                                                    AND d.`id_merchandise`='$id_merchandise'
                                                    AND d.`id_attribute`='$val->id_attribute'")->result();
                $attr = array();
                if(!empty($data_attr)) {
                    foreach ($data_attr as $rows => $vals) {
                        foreach ($data_attr as $rows => $vals) {
                            $attr[$rows] =  $vals->detail_attribute;
                        }
                        // push nested attribute
                        $result[$attr_name] = $attr;
                    }
                }
            }
        }
        return $result;
    }

    function save_order($data, $id_order='')
    {
        $result = '';
        if($id_order == '') {
            $this->db->insert('temp_order_merchandise', $data);
            $result = $this->db->insert_id();
        } else {
            $this->db->where('id', $id_order)->update('temp_order_merchandise', $data);
            $result = $id_order;
        }

        return $result;
    }

    function list_order($start, $count)
    {
        $result = array();
        $uid  = userid();
        $data =  $this->db->query("SELECT a.id AS id_order, a.tgl, a.id_merchandise, m.`nama` AS merchandise , a.jumlah, a.harga, a.total, 
                                    status_order_merchandise(a.`status`) AS status_order, a.keterangan, a.status,
                                    IFNULL(i.path,'') AS path, IFNULL(i.image,'') AS image 
                                    FROM temp_order_merchandise a 
                                    JOIN ms_merchandise m ON a.`id_merchandise`=m.`id`
                                    LEFT JOIN (
                                        SELECT id_merchandise, path, image
                                            FROM tb_image_merchandise
                                            WHERE main=1
                                        ) i ON a.id_merchandise=i.id_merchandise
                                    LEFT JOIN (
                                        SELECT id_order, COUNT(*) AS jml_terima 
                                            FROM temp_penawaran
                                            WHERE `status`='5'
                                            GROUP BY id_order
                                    ) p ON a.id=p.id_order
                                    WHERE a.`user_insert`='$uid'
                                    AND IFNULL(p.jml_terima,0) = '0'
                                    AND a.`status` != '4'
                                    LIMIT $start,$count")->result();

        if(!empty($data)) {
            foreach ($data as $row => $val) {
                $attr = $this->detail_order_attribute($val->id_order);

                $ukuran = isset($attr['ukuran']) ? $attr['ukuran']:'';
                $warna = isset($attr['warna']) ? $attr['warna']:'';

                $result[$row] = array(
                                    'id_order' => $val->id_order,
                                    'tgl' => $val->tgl,
                                    'id_merchandise' => $val->id_merchandise,
                                    'merchandise' => $val->merchandise,
                                    'jumlah' => $val->jumlah,
                                    'harga' => $val->harga,
                                    'total' => $val->total,
                                    'keterangan' => $val->keterangan,
                                    'status' => $val->status,
                                    'status_order' => $val->status_order,
                                    'path' => $val->path,
                                    'image' => $val->image,
                                    'ukuran' => $ukuran,
                                    'warna' => $warna
                                );
            }
        }
        return $result;
    }

    function detail_order($id_order='')
    {
        return $this->db->query("SELECT t.`id` AS id_order, t.`tgl`, t.`id_merchandise`, m.`nama` AS merchandise, t.`jumlah`, t.`harga`, t.`total`,
                                    IFNULL(i.`path`,'') AS `path`, IFNULL(i.`image`,'') AS image 
                                    FROM temp_order_merchandise t
                                    JOIN ms_merchandise m ON t.`id_merchandise`=m.`id`
                                    LEFT JOIN (
                                        SELECT id_merchandise, `path`, image
                                            FROM tb_image_merchandise
                                            WHERE main=1
                                    ) i ON t.`id_merchandise`=i.`id_merchandise`
                                    WHERE t.`id`='$id_order'")->row();
    }

    function detail_order_attribute($id_order='')
    {
        $result = array();

        $attr = $this->db->query("SELECT * FROM temp_order_merchandise_detail
                                    WHERE id_order='$id_order'")->result();
        if(!empty($attr)) {
            $get_attr = array();
            foreach ($attr as $row => $val) {
                if($val->ukuran != '') {
                    $result['ukuran'][$row] = $val->ukuran;
                }
                if($val->warna != '') {
                    $result['warna'][$row] = $val->warna;
                }
            }
        }

        return $result;
    }

    function list_penawaran($id_order='', $id_merchandise='', $id_user='')
    {
        $result = array();

        $data = $this->db->query("SELECT *, status_order_merchandise(status) AS status_tawar  
                                    FROM temp_penawaran
                                    WHERE id_order='$id_order'
                                    AND id_merchandise='$id_merchandise'
                                    -- AND id_user='$id_user'
                                    ")->result();
        if(!empty($data)) {
            $no=1;
            foreach ($data as $row => $val) {
                $result_img = array();
                $img = $this->db->query("SELECT * FROM temp_image_penawaran
                                            WHERE id_penawaran='$val->id'")->result();
                foreach ($img as $rows => $vals) {
                    $result_img[$rows] = array(
                                            'path' => $vals->path,
                                            'image' => $vals->image
                                        );
                }

                $result['data'][$row] = array(
                                    'id_penawaran' => $val->id,
                                    'title' => 'Penawaran '.$no++,
                                    'keterangan' => $val->keterangan,
                                    'images' => $result_img,
                                    'status' => $val->status,
                                    'status_tawar' => $val->status_tawar
                                );
            }

            $result['button'] = array(
                'setuju' => 3,
                'tolak' => 9
            );
        }
        return $result;
    }

    function view_suggestion($start = 0, $count = 0, $keyword = '', $id_user = '')
    {
        $condition = '';
        if ($keyword != '') {
            $condition .= " AND b.nama LIKE '%$keyword%' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit .= " LIMIT $start, $count ";
        }

        return $this->db->query("
            SELECT b.*
            FROM tb_suggestion a
            INNER JOIN view_merchandise b ON a.`id_merchandise` = b.`id`
            WHERE a.`id_user` = '$id_user' 
            $condition 
            $limit ")->result();
    }

    function proses_penawaran($id_penawaran = [], $statusx = '')
    {
        $condition = '';
        $userid = userid();
        if (! empty($id_penawaran)) {
            $condition .= ' WHERE (';
            for ($i = 0; $i < count($id_penawaran); $i++) {
                $condition .= " id = '$id_penawaran[$i]' ";

                if ($id_penawaran[$i] != end($id_penawaran)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ' ) ';
        }

        $this->db->trans_begin();

        $this->db->query("
            UPDATE temp_penawaran 
            SET `status` = '$statusx',
            user_update = '$userid',
            update_at = NOW()
            $condition ");

        if ($statusx == 3) {
            $order = $this->db->query("
                SELECT *
                FROM temp_penawaran 
                $condition ")->result();

            if ($order) {
                foreach ($order as $row) {
                    $id_order = $row->id_order;

                    $this->db->query("
                        UPDATE temp_order_merchandise 
                        SET `status` = '$statusx' 
                        WHERE id = '$id_order' ");
                }
            }
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();

            return true;
        } else {
            $this->db->trans_rollback();

            return false;
        }
    }

    function merchandise_order($id_order = '')
    {   
        return $this->db->query("
            SELECT b.*, a.harga AS harga_jual, a.jumlah
            FROM temp_order_merchandise a
            INNER JOIN ms_merchandise b ON a.`id_merchandise` = b.`id`
            WHERE a.id = '$id_order' ")->row();
    }
}

/* End of file Merchandise_Model.php */
/* Location: ./application/models/Merchandise_Model.php */
