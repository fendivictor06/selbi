<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Transaksi_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function header_transaksi($keyword = '', $start = 0, $count = 0)
    {
        $keyword = $this->db->escape_str($keyword);
        $start = $this->db->escape_str($start);
        $count = $this->db->escape_str($count);

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        $condition = '';
        if ($keyword != '') {
            $condition = " AND (b.nama LIKE  '%$keyword%' 
                OR a.nobukti LIKE '%$keyword%'
                OR a.noresi LIKE '%$keyword%' )";
        }

        $id_user = iduser();

        $path = path_pelapak();

        return $this->db->query("
            SELECT a.`total`, a.`tgl`, a.`nobukti`, a.`noresi`, a.`status`, 
            status_transaksi(a.`status`) AS status_transaksi
            FROM tb_transaksi a
            WHERE a.id_user = '$id_user' 
            $condition
            $limit ")->result();
    }

    function detail_transaksi($id = '')
    {
        $response = [];
        $query = $this->db->query("
            SELECT a.*, status_transaksi(a.status) AS status_transaksi
            FROM tb_transaksi a 
            WHERE a.nobukti = '$id' ")->row();

        if ($query) {
            $response = array(
                'id' => isset($query->id) ? $query->id : '',
                'alamat' => isset($query->alamat) ? $query->alamat : '',
                'nobukti' => isset($query->nobukti) ? $query->nobukti : '',
                'status' => isset($query->status) ? $query->status : '',
                'status_transaksi' => isset($query->status_transaksi) ? $query->status_transaksi : '',
                'catatan' => isset($query->catatan) ? $query->catatan : '',
                'total' => isset($query->total) ? $query->total : 0
            );

            $barang = $this->db->query("
                SELECT a.`harga`, a.`jumlah`, IFNULL(b.`nama`, a.`name`) AS barang,
                IFNULL(c.image, '') AS image, IFNULL(b.id, '') AS id_barang,
                a.total
                FROM detail_transaksi a
                LEFT JOIN ms_barang b ON a.`id_barang` = b.`id`
                LEFT JOIN (
                    SELECT image, id_barang
                    FROM tb_image_barang 
                    WHERE main = 1
                    GROUP BY id_barang
                ) AS c ON c.id_barang = b.id
                WHERE a.`nobukti` = '$id' ")->result();

            if ($barang) {
                foreach ($barang as $row => $val) {
                    $path = path_produk();
                    $image = ($val->image != '') ? $path.$val->image : '';

                    $response['barang'][$row] = array(
                        'id' => $val->id_barang,
                        'nama' => $val->barang,
                        'jumlah' => $val->jumlah,
                        'harga' => $val->harga,
                        'total' => $val->total,
                        'image' => $image
                    );
                }
            }
        }

        return $response;
    }

    function log_transaksi($temporary = [], $temporary_detail = [])
    {
        if (! empty($temporary) && ! empty($temporary_detail)) {
            $this->db->trans_begin();

            $this->db->insert_batch('temp_transaksi_detail', $temporary_detail);
            $this->db->insert('temp_transaksi', $temporary);

            if ($this->db->trans_status()) {
                $this->db->trans_commit();

                return true;
            } else {
                $this->db->trans_rollback();

                return false;
            }
        }
    }

    function simpan_transaksi($order_id = '') 
    {
        $this->load->library('midtrans');
        # group by penjual
        // $penjual = $this->db->query("
        //     SELECT a.`id_penjual`
        //     FROM temp_transaksi_detail a
        //     WHERE a.order_id = '$order_id'
        //     GROUP BY a.`id_penjual`")->result();

        $flag = substr($order_id, 0, 3);
        $type = ($flag == 'SEL') ? 'buy' : 'bid';

        $header = [];
        $detail = [];
        $id_lelang = [];
        // if ($penjual) {
            // foreach ($penjual as $row) {
                $nobukti = $this->midtrans->invoice('user', 'INV');
                
                if ($flag == 'SEL') {
                    $detail_transaksi = $this->db->query("
                        SELECT a.*, b.`id_barang`, b.id_user, c.shipping_address,
                        '' AS id_lelang
                        FROM temp_transaksi_detail a
                        LEFT JOIN tb_keranjang b ON a.`id_cart` = b.`id` 
                        INNER JOIN temp_transaksi c ON c.order_id = a.order_id
                        WHERE a.`order_id` = '$order_id'  ")->result();
                } else {
                    $detail_transaksi = $this->db->query("
                        SELECT a.*,c.id AS id_user, b.id_barang,d.shipping_address,
                        b.id AS id_lelang   
                        FROM temp_transaksi_detail a
                        LEFT JOIN tb_lelang b ON a.id_cart = b.id
                        LEFT JOIN tb_user c ON b.user_insert = c.uid
                        INNER JOIN temp_transaksi d ON d.order_id = a.order_id
                        WHERE a.order_id = '$order_id' ")->result();
                }

                $id_user = '';
                $tgl = now();
                $total = 0;   
                $alamat = '';
                if ($detail_transaksi) {
                    foreach ($detail_transaksi as $val) {

                        if (!in_array($val->id_lelang, $id_lelang) && $type == 'bid') {
                            $id_lelang[] = $val->id_lelang;
                        }

                        if ($val->id_user != '') {
                            $id_user = $val->id_user;
                        }
                        $total = $total + ($val->price * $val->quantity);
                        $alamat = $val->shipping_address;

                        $detail[] = array(
                            'nobukti' => $nobukti,
                            'id_barang' => isset($val->id_barang) ? $val->id_barang : 0,
                            'id_jasa' => 0,
                            'name' => $val->name,
                            'harga' => $val->price,
                            'jumlah' => $val->quantity,
                            'total' => ($val->price * $val->quantity),
                            'kurir' => $val->kurir,
                            'id_penjual' => $val->id_penjual
                        );
                    }
                }

                $header[] = array(
                    'nobukti' => $nobukti,
                    'nomidtrans' => $order_id,
                    'id_user' => $id_user,
                    'tgl' => $tgl,
                    'noresi' => '',
                    'total' => $total,
                    'type' => $type,
                    'catatan' => '',
                    'alamat' => $alamat,
                    'status' => 1
                );
            // }
        // }

        if ($id_lelang) {
            for ($i = 0; $i < count($id_lelang); $i++) {
                $lelang_id = isset($id_lelang[$i]) ? $id_lelang[$i] : '';

                if ($lelang_id != '') {
                    $this->db->where('id_lelang', $lelang_id)->where('menang', 1)->update('detail_lelang', ['no_tagihan' => $nobukti,'bayar' => 1]);
                }
            }
        }

        $this->db->insert_batch('tb_transaksi', $header);
        $this->db->insert_batch('detail_transaksi', $detail);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();

            return true;
        } else {
            $this->db->trans_rollback();

            return false;
        }
    }   

    function simpan_bid($order_id = '') 
    {
        $this->load->library('midtrans');
        # group by penjual
        // $penjual = $this->db->query("
        //     SELECT a.`id_penjual`
        //     FROM temp_transaksi_detail a
        //     WHERE a.order_id = '$order_id'
        //     GROUP BY a.`id_penjual`")->result();

        $header = [];
        $detail = [];
        // if ($penjual) {
            // foreach ($penjual as $row) {
                $nobukti = $this->midtrans->invoice('lelang', 'BID');

                // $detail_transaksi = $this->db->query("
                //     SELECT a.*, b.`id_barang`, b.id_user, c.shipping_address
                //     FROM temp_transaksi_detail a
                //     LEFT JOIN tb_keranjang b ON a.`id_cart` = b.`id` 
                //     INNER JOIN temp_transaksi c ON c.order_id = a.order_id
                //     WHERE a.`order_id` = '$order_id'  ")->result();
                $detail_bid= $this->db->query("
                    SELECT a.*,c.id, b.id_barang as id_user  FROM temp_transaksi_detail a
                    LEFT JOIN tb_lelang b ON a.id_cart = b.id
                    LEFT JOIN tb_user c ON b.user_insert = c.uid
                    WHERE a.order_id='$order_id'
                    ")->result();

                $id_user = '';
                $tgl = now();
                $total = 0;   
                $alamat = '';
                if ($detail_bid) {
                    foreach ($detail_bid as $val) {
                        if ($val->id_user != '') {
                            $id_user = $val->id_user;
                        }
                        $total = $total + ($val->price * $val->quantity);
                        $alamat = $val->shipping_address;

                        $detail[] = array(
                            'nobukti' => $nobukti,
                            'id_barang' => isset($val->id_barang) ? $val->id_barang : 0,
                            'id_jasa' => 0,
                            'name' => $val->name,
                            'harga' => $val->price,
                            'jumlah' => $val->quantity,
                            'total' => ($val->price * $val->quantity),
                            'kurir' => $val->kurir,
                            'id_penjual' => $val->id_penjual
                        );
                    }
                }

                $header[] = array(
                    'nobukti' => $nobukti,
                    'nomidtrans' => $order_id,
                    'id_user' => $id_user,
                    'tgl' => $tgl,
                    'noresi' => '',
                    'total' => $total,
                    'catatan' => '',
                    'alamat' => $alamat,
                    'status' => 1
                );
            // }
        // }

        $this->db->insert_batch('tb_transaksi', $header);
        $this->db->insert_batch('detail_transaksi', $detail);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            
            return true;
        } else {
            $this->db->trans_rollback();

            return false;
        }
    }   
}

/* End of file Transaksi_Model.php */
/* Location: ./application/models/Transaksi_Model.php */
