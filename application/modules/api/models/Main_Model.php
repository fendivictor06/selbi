<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Main_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_by_id($table = '', $condition = '', $select = '*', $order = '', $group = [], $limit = [], $row = 'row')
    {
        # where condition
        if ($condition != '') {
            $this->db->where($condition);
        }

        # order
        if ($order != '') {
            $this->db->order_by($order);
        }

        # group by
        if (! empty($group)) {
            $this->db->group_by($group);
        }

        # limit
        if (! empty($limit)) {
            $start = isset($limit[0]) ? $limit[0] : 0;
            $end = isset($limit[1]) ? $limit[1] : 0;

            $this->db->limit($end, $start);
        }

        # select
        $this->db->select($select);
        

        if ($row == 'row') {
            # if result row();
            return $this->db->get($table)->row();
        } else {
            # if result result();
            return $this->db->get($table)->result();
        }
    }

    function process_data($table = '', $data = '', $condition = '')
    {
        if ($condition) {
            $this->db->where($condition)->update($table, $data);
            return $this->db->affected_rows();
        } else {
            $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }

    function delete_data($table = '', $condition = '')
    {
        $this->db->where($condition)->delete($table);
        return $this->db->affected_rows();
    }

    function jobs_id()
    {
        $date  = date_create();
        $value = date_timestamp_get($date);

        $method   = 'aes-256-cbc';
        $password = substr(hash('sha256', 's3lbi', true), 0, 32);
        $iv = chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0) .chr(0x0).chr(0x0).chr(0x0);
        $encrypted = base64_encode(openssl_encrypt($value, $method, $password, OPENSSL_RAW_DATA, $iv));
        return $encrypted;
    }
}

/* End of file Main_Model.php */
/* Location: ./application/models/Main_Model.php */
