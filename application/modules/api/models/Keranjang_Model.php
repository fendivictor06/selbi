<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Keranjang_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_keranjang()
    {
        $id_user = iduser();

        $result = [];
        $path = path_produk();

        # array penjual
        $penjual = $this->db->query("
            SELECT a.`id_penjual`, b.`profile_name` AS penjual,
            b.id_kota, b.id_provinsi
            FROM tb_keranjang a
            INNER JOIN tb_user b ON a.`id_penjual` = b.`id`
            WHERE a.`id_user` = '$id_user'
            AND a.flag = 1
            GROUP BY b.`id` ")->result();

        if ($penjual) {
            foreach ($penjual as $row => $key) {
                # barang
                $barang = $this->db->query("
                    SELECT a.`id`, a.`id_penjual`, c.`profile_name` AS penjual,
                    b.`nama` AS barang, b.`harga`, a.`jumlah`, d.`image`
                    FROM tb_keranjang a
                    INNER JOIN ms_barang b ON a.`id_barang` = b.`id`
                    INNER JOIN tb_user c ON c.`id` = a.`id_penjual`
                    INNER JOIN tb_image_barang d ON d.`id_barang` = b.`id`
                    WHERE d.`main` = 1
                    AND b.`status` = 1 
                    AND a.flag = 1
                    AND a.jenis = 'barang'
                    AND a.id_penjual = '$key->id_penjual'
                    AND a.id_user = '$id_user' ")->result();

                $result[$row] = array(
                    'id_penjual' => $key->id_penjual,
                    'penjual' => $key->penjual,
                    'id_kota' => $key->id_kota,
                    'id_provinsi' => $key->id_provinsi
                );

                foreach ($barang as $val => $foo) {
                    $result[$row]['barang'][$val] = array(
                        'id' => $foo->id,
                        'barang' => $foo->barang,
                        'harga' => $foo->harga,
                        'jumlah' => $foo->jumlah,
                        'image' => $path.$foo->image
                    );
                }
            }
        }

        return $result;
    }
}

/* End of file Keranjang_Model.php */
/* Location: ./application/models/Keranjang_Model.php */
