<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function view_alamat($keyword = '', $start = 0, $count = 0, $id = '')
	{
		$keyword = $this->db->escape_str($keyword);
		$start = $this->db->escape_str($start);
		$count = $this->db->escape_str($count);
		$id = $this->db->escape_str($id);
		$id_user = iduser();

		$condition = '';
		$limit = '';

		$limit = ($count > 0) ? " LIMIT $start, $count " : "";
		$condition .= ($keyword != '') ? " AND a.alamat LIKE '%$keyword%' " : "";
		$condition .= ($id != '') ? " AND a.id = '$id' " : "";

		$query = $this->db->query("
			SELECT a.id, a.label, a.penerima, a.alamat, 
			a.ref_provinsi, b.provinsi, a.ref_kota, c.kota, 
			a.kodepos, a.telepon, a.latitude, a.longitude
			FROM tb_alamat_user a 
			INNER JOIN ms_provinsi b ON a.ref_provinsi = b.id
			INNER JOIN ms_kota c ON a.ref_kota = c.id
			WHERE  1 = 1 
			AND a.id_user = '$id_user'
			$condition 
			$limit ");

		if ($id != '') {
			return $query->row();
		} else {
			return $query->result();
		}
	}
}

/* End of file User_Model.php */
/* Location: ./application/models/User_Model.php */ ?>