<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notif_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function view_notif($tipe = '', $start = 0, $count = 0) 
	{
		$id_user = iduser();
		$limit = '';
		if ($count > 0) {
			$limit = " LIMIT $start, $count ";
		}

		return $this->db->query("
			SELECT a.*, IFNULL(b.foto, c.foto) AS foto
			FROM tb_notif a 
			LEFT JOIN tb_user b ON a.from = b.id
			INNER JOIN tb_user c ON a.to = c.id
			WHERE a.tipe = '$tipe' 
			AND a.to = '$id_user' 
			ORDER BY a.insert_at DESC
			$limit ")->result();
	}

	function create_notif($to = '', $from = '', $teks = '', $tipe = '')
	{
		$user_id = userid();

		$data = array(
			'to' => $to,
			'from' => $from,
			'teks' => $teks,
			'tipe' => $tipe,
			'user_insert' => $user_id
		);

		$this->db->insert('tb_notif', $data);
	}
}

/* End of file Notif_Model.php */
/* Location: ./application/models/Notif_Model.php */ ?>