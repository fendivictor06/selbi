<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function view_profile()
    {
        $user_id = userid();

        return $this->db->query("
    		SELECT a.`id`, a.`profile_name`, a.`email`, a.`foto`, 
			a.`uid`, a.`type`, a.`fcm_id`, IFNULL(a.alamat, '') AS alamat,
			IFNULL(a.`no_telp`, '') AS no_telp, IFNULL(a.`sampul`, a.`foto`) AS sampul,
            jumlah_follower(a.id) AS jumlah_follower, jumlah_following(a.id) AS jumlah_following,
            IFNULL(a.`deskripsi`,'') AS deskripsi
			FROM tb_user a
			WHERE a.`uid` = '$user_id' ")->row();
    }
}

/* End of file Profile_Model.php */
/* Location: ./application/models/Profile_Model.php */
