<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hot_News_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function view_news($keyword = '', $start = 0, $count = 0, $id = '')
	{
		$condition = '';
		$limit = '';
		$path = path_hotnews();

		if ($id != '') {
			$condition .= " AND a.id = '$id' ";
		} else {
			if ($keyword != '') {
				$condition .= " AND a.judul LIKE '%$keyword%' ";
			}

			if ($count > 0) {
				$limit .= " LIMIT $start, $count ";
			}
		}

		$query = $this->db->query("
			SELECT a.`id`, a.`judul`, a.`image`, a.`teks`, 
			tgl_indo(a.`insert_at`, 1) AS waktu, '$path' AS `path`
			FROM tb_news a
			WHERE 1 = 1 
			$condition 
			$limit " );

		$result = ($id == '') ? $query->result() : $query->row();

		return $result;
	}
}

/* End of file Hot_News_Model.php */
/* Location: ./application/models/Hot_News_Model.php */ ?>