<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Produk_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Notif_Model');
    }

    function view_produk($start = 0, $count = 0, $keyword = '', $brand = '', $kategori = '', $penjual = '', $jenis = '', $donasi = '', $lelang = '')
    {
        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count";
        }

        $condition = '';
        if ($keyword != '') {
            $condition .= " AND a.nama LIKE '%$keyword%' ";
        }

        # where brand is not null
        if ($brand != '') {
            $condition .= " AND a.id_brand = '$brand' ";
        }

        # where kategori is not null
        if ($kategori != '') {
            $condition .= " AND a.id_category = '$kategori' ";
        }

        # where penjual is not null
        if ($penjual != '') {
            $condition .= " AND c.id = '$penjual' ";
        }

        # where jenis is not null
        if ($jenis != '') {
            $condition .= " AND f.id = '$jenis' ";
        }

        if ($donasi != '' && $donasi != '') {
            $condition .= " AND (a.donasi = 1 OR a.lelang = 1) ";
        } else {
            # where donasi is not null
            if ($donasi != '') {
                $condition .= " AND a.donasi = 1 ";
            }

            # where lelang is not null
            if ($lelang != '') {
                $condition .= " AND a.lelang = 1 "; 
            }
        }

        # id user
        $id_user = iduser();

        return $this->db->query("
			SELECT a.`id` AS id_barang, b.`category`, c.`profile_name` AS penjual, 
			a.`kode`, a.`nama`, a.`deskripsi`, a.`harga`, 
			a.diskon, a.`berat`,
			a.`berat_satuan`, 
			IFNULL(barang_image.image, '') AS image,
			e.brand, IFNULL(favorit.is_favorit, '0') AS is_favorit,
            a.id_jenis , f.jenis, c.foto, a.pemakaian, a.satuan_pemakaian,
            a.donasi
			FROM ms_barang a
			LEFT JOIN ms_category b ON a.`id_category` = b.`id`
			INNER JOIN tb_user c ON a.`id_penjual` = c.`id`
			LEFT JOIN (
				SELECT *
				FROM tb_image_barang
				WHERE main = 1
				GROUP BY id_barang
			) AS barang_image ON barang_image.id_barang = a.id
			LEFT JOIN ms_brand e ON e.id = a.id_brand
			LEFT JOIN (
				SELECT id_barang, '1' AS is_favorit
				FROM tb_favorit
				WHERE id_user = '$id_user'
			) AS favorit ON favorit.id_barang = a.id
            INNER JOIN ms_jenis_barang f ON f.id = a.id_jenis
			WHERE a.`status` = 1 
			$condition 
			$limit ")->result();
    }

    function view_produk20181121($start = 0, $count = 0, $keyword = '', $brand = '', $kategori = '', $penjual = '')
    {
        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count";
        }

        $condition = '';
        if ($keyword != '') {
            $condition .= " AND a.nama LIKE '%$keyword%' ";
        }

        # where brand is not null
        if ($brand != '') {
            $condition .= " AND a.id_brand = '$brand' ";
        }

        # where kategori is not null
        if ($kategori != '') {
            $condition .= " AND a.id_category = '$kategori' ";
        }

        # where penjual is not null
        if ($penjual != '') {
            $condition .= " AND c.id = '$penjual' ";
        }

        # id user
        $id_user = iduser();

        return $this->db->query("
			SELECT a.`id` AS id_barang, b.`category`, c.`nama` AS penjual, 
			a.`kode`, a.`nama`, a.`deskripsi`, a.`harga`, 
			a.diskon, a.`berat`,
			a.`berat_satuan`, 
			CASE WHEN (a.kondisi = 1)
			THEN 'BARU'
			ELSE 'BEKAS'
			END AS kondisi, 
			IFNULL(barang_image.image, '') AS image,
			e.brand, IFNULL(favorit.is_favorit, '0') AS is_favorit
			FROM ms_barang a
			INNER JOIN ms_category b ON a.`id_category` = b.`id`
			INNER JOIN ms_penjual c ON a.`id_penjual` = c.`id`
			LEFT JOIN (
				SELECT *
				FROM tb_image_barang
				WHERE main = 1
				GROUP BY id_barang
			) AS barang_image ON barang_image.id_barang = a.id
			INNER JOIN ms_brand e ON e.id = a.id_brand
			LEFT JOIN (
				SELECT id_barang, '1' AS is_favorit
				FROM tb_favorit
				WHERE id_user = '$id_user'
			) AS favorit ON favorit.id_barang = a.id
			WHERE a.`status` = 1 
			AND a.lelang = 0
			$condition 
			$limit ")->result();
    }

    function produk_detail($id = '')
    {
        $id_user = iduser();

        return $this->db->query("
			SELECT a.`id` AS id_barang, b.`category`,
			a.`kode`, a.`nama`, a.`deskripsi`, a.`harga`, 
			a.diskon, a.`berat`, a.id_penjual,
			a.`berat_satuan`, f.kondisi, 
			IFNULL(barang_image.image, '') AS image,
			e.brand, a.rating, IFNULL(favorit.is_favorit, '0') AS is_favorit,
            a.tahun_pembelian, a.kondisi_barang, a.deskripsi_pemakaian
			FROM ms_barang a
			LEFT JOIN ms_category b ON a.`id_category` = b.`id`
			
			LEFT JOIN (
				SELECT *
				FROM tb_image_barang
				WHERE main = 1
				GROUP BY id_barang
			) AS barang_image ON barang_image.id_barang = a.id

			LEFT JOIN (
				SELECT id_barang, '1' AS is_favorit
				FROM tb_favorit
				WHERE id_user = '$id_user'
			) AS favorit ON favorit.id_barang = a.id
			
			LEFT JOIN ms_brand e ON e.id = a.id_brand
            INNER JOIN ms_kondisi f ON f.id = a.kondisi

			WHERE a.`status` = 1 
			AND a.id = '$id' ")->row();
    }

    function data_penjual($id = '')
    {
        return $this->db->query("
			SELECT a.*, b.kota
			FROM tb_user a
			INNER JOIN ms_kota b ON a.id_kota = b.id
			WHERE a.id = '$id' ")->row();
    }

    function data_penjual20181121($id = '')
    {
        return $this->db->query("
			SELECT a.*, b.kota
			FROM ms_penjual a
			INNER JOIN ms_kota b ON a.id_kota = b.id
			WHERE a.id = '$id' ")->row();
    }

    function review_barang($id_barang = '', $start = 0, $count = 0, $rating = 0)
    {
        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        $condition = '';
        if ($id_barang != '') {
            $id_barang = $this->db->escape_str($id_barang);

            $condition .= " AND a.id_barang = '$id_barang' ";
        }

        if ($rating > 0) {
            $condition .= " AND a.rating = '$rating' ";
        }
 
        return $this->db->query("
    		SELECT b.`profile_name`, b.`foto`, a.`insert_at` AS waktu,
			a.`rating`, a.`deskripsi`, a.id
			FROM tb_review a
			INNER JOIN tb_user b ON a.`id_user` = b.`id`
			INNER JOIN ms_barang c ON c.`id` = a.`id_barang`
			WHERE b.status = 1 
            AND a.parent = 0
			$condition 
			$limit ")->result();
    }

    function total_review($id_barang = '', $rating = 0)
    {
        $condition = '';
        if ($id_barang != '') {
            $id_barang = $this->db->escape_str($id_barang);

            $condition .= " AND a.id_barang = '$id_barang' ";
        }

        if ($rating > 0) {
            $condition .= " AND a.rating = '$rating' ";
        }

        $query = $this->db->query("
    		SELECT COUNT(a.id) AS jumlah
			FROM tb_review a
			INNER JOIN tb_user b ON a.`id_user` = b.`id`
			INNER JOIN ms_barang c ON c.`id` = a.`id_barang`
			WHERE b.status = 1 
            AND a.parent = 0
			$condition ")->row();

        $total = isset($query->jumlah) ? $query->jumlah : 0;

        return $total;
    }

    function review_child($id = '')
    {
        return $this->db->query("
            SELECT b.`profile_name`, b.`foto`, a.`insert_at` AS waktu,
            a.`rating`, a.`deskripsi`, a.id
            FROM tb_review a
            INNER JOIN tb_user b ON a.`id_user` = b.`id`
            INNER JOIN ms_barang c ON c.`id` = a.`id_barang`
            WHERE b.status = 1 
            AND a.parent = '$id' ")->result();
    }

    function add_produk($data = [], $foto = '', $gallery = [], $nama_barang = '', $lelang = 0, $start = '', $end = '', $harga = 0, $id_order = '', $stok = 0)
    {
        $id_user = iduser();
        $user_id = userid();
        if (! empty($data)) {
            $string_data = array_to_query($data);

            $this->db->trans_begin();

            # insert barang
            $this->db->query("
            	INSERT INTO ms_barang (id_category, id_penjual, id_brand, kode, nama, deskripsi, harga, diskon, berat, berat_satuan, kondisi, user_insert, lelang, id_jenis, pemakaian, satuan_pemakaian, donasi, tahun_pembelian, kondisi_barang, deskripsi_pemakaian) 
            	VALUES $string_data ");

            $id_barang = $this->db->insert_id();

            # generate kode barang
            switch (strlen($id_barang)) {
                case '1':
                    $kode_barang = '0000'.$id_barang;
                    break;
                case '2':
                    $kode_barang = '000'.$id_barang;
                    break;
                case '3':
                    $kode_barang = '00'.$id_barang;
                    break;
                case '4':
                    $kode_barang = '0'.$id_barang;
                    break;
                default:
                    $kode_barang = $id_barang;
                    break;
            }

            # update barang
            $this->db->query("
            	UPDATE ms_barang 
            	SET kode = '$kode_barang'
            	WHERE id = '$id_barang' ");

            # insert gallery
            $main_image = '';
            if ($foto != '') {
                $this->db->query("
        			UPDATE tb_image_barang 
        			SET id_barang = '$id_barang',
        			main = 1 
        			WHERE id = '$foto' ");

                # main foto
                $img = $this->db->query("
                	SELECT *
                	FROM tb_image_barang a 
                	WHERE a.id = '$foto' ")->row();

                $main_image = isset($img->image) ? $img->image : '';
            }

            if (! empty($gallery)) {
                for ($i = 0; $i < count($gallery); $i++) {
                    $images = isset($gallery[$i]) ? $gallery[$i] : '';
                    if ($images != '') {
                        $this->db->query("
	            			UPDATE tb_image_barang 
	            			SET id_barang = '$id_barang' 
	            			WHERE id = '$images' ");
                    }
                }
            }

            if ($lelang == 1) {
                $jenis = 5;
            } else {
                $jenis = 4;
            }

            # insert into feed
            $this->db->query("
            	INSERT INTO tb_feed (id_penjual, jenis, judul, user_insert, lelang)
            	VALUES ('$id_user', '$jenis', '', '$user_id', '$lelang')");
            $id_feed = $this->db->insert_id();

            $this->db->query("
            	INSERT INTO tb_feed_image (id_feed, id_barang, teks, image, user_insert)
            	VALUES ('$id_feed', '$id_barang', '$nama_barang', '$main_image', '$user_id')");

            if ($lelang == 1) {
                $this->db->query("
                    INSERT INTO tb_lelang (id_barang, `start`, `end`, bid_awal, bid_akhir, user_insert)
                    VALUES ('$id_barang', '$start', '$end', '$harga', '$harga', '$user_id')");

                $teks = 'Berhasil Menambahkan Barang Lelang';
            } else {
                # insert random
                $this->db->query("
                    INSERT INTO tb_feed_image (id_feed, teks, id_barang, image, user_insert)
                    (   SELECT '$id_feed', ms_barang.`nama`, ms_barang.id, tb_image_barang.`image`, tb_image_barang.`user_insert`
                        FROM ms_barang 
                        INNER JOIN tb_image_barang ON ms_barang.`id` = tb_image_barang.`id_barang` 
                        WHERE ms_barang.`status` = 1
                        AND tb_image_barang.`main` = 1
                        AND tb_image_barang.`id_barang` != '$id_barang'
                        AND tb_image_barang.`user_insert` = '$user_id'
                        AND ms_barang.lelang = 0
                        ORDER BY RAND()
                        LIMIT 2
                    )");

                $teks = 'Berhasil Menambahkan Barang';
            }

            if ($id_order != '') {
                $this->db->where('id', $id_order)
                        ->update('temp_order_merchandise', [
                                'status' => 5,
                                'update_at' => now(),
                                'user_update'=> $user_id
                            ]);

                $this->db->where('id_order', $id_order)
                        ->update('temp_penawaran', [
                                'status' => 5,
                                'update_at' => now(),
                                'user_update'=> $user_id
                            ]);
            }

            if ($stok > 0) {
                $this->db->insert('ms_stok', [
                    'id_barang' => $id_barang, 
                    'stok' => $stok, 
                    'user_insert' => $user_id
                ]);
            }

            # insert untuk riwayat
            $this->Notif_Model->create_notif($id_user, '', $teks, 'barang');

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();

                $result = $id_barang;
            } else {
                $this->db->trans_commit();

                $result = 1;
            }
        } else {
            $result = 0;
        }

        return $result;
    }

    function diskusi_barang($id_barang = '', $start = 0, $count = 0)
    {
        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        $condition = '';
        if ($id_barang != '') {
            $id_barang = $this->db->escape_str($id_barang);

            $condition = " AND a.id_barang = '$id_barang' ";
        }

        return $this->db->query("
            SELECT b.`profile_name`, b.`foto`, a.`insert_at` AS waktu, 
            a.`deskripsi`, a.id
            FROM tb_diskusi a
            INNER JOIN tb_user b ON a.`id_user` = b.`id`
            INNER JOIN ms_barang c ON c.`id` = a.`id_barang`
            WHERE b.status = 1 
            AND a.parent = 0
            $condition 
            $limit ")->result();
    }

    function diskusi_child($id = '')
    {
        return $this->db->query("
            SELECT b.`profile_name`, b.`foto`, a.`insert_at` AS waktu, 
            a.`deskripsi`, a.id
            FROM tb_diskusi a
            INNER JOIN tb_user b ON a.`id_user` = b.`id`
            INNER JOIN ms_barang c ON c.`id` = a.`id_barang`
            WHERE b.status = 1 
            AND a.parent = '$id' ")->result();
    }

    function total_diskusi($id_barang = '')
    {
        $condition = '';
        if ($id_barang != '') {
            $id_barang = $this->db->escape_str($id_barang);

            $condition = " AND a.id_barang = '$id_barang' ";
        }

        $query = $this->db->query("
            SELECT COUNT(a.id) AS jumlah
            FROM tb_diskusi a
            INNER JOIN tb_user b ON a.`id_user` = b.`id`
            INNER JOIN ms_barang c ON c.`id` = a.`id_barang`
            WHERE b.status = 1 
            AND a.parent = 0
            $condition ")->row();

        $total = isset($query->jumlah) ? $query->jumlah : 0;

        return $total;
    }

    function update_rating($id_barang = '')
    {
        $this->db->query("CALL update_rating('$id_barang');");
    }

    function rating_barang($id_barang = '')
    {
        return $this->db->query("CALL rating_barang('$id_barang');")->row();
    }

    function hot_item($keyword = '', $kategori = '', $start = 0, $count = 0)
    {
        // return $this->db->query("CALL hot_item('$keyword', '$kategori', '$start', '$count'); ")->result();
        $condition = '';
        if ($kategori <> '') {
            $condition .= " AND a.id_category = '$kategori' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit .= " LIMIT $start, $count ";
        }

        return $this->db->query("
            SELECT *
            FROM view_barang a 
            WHERE a.nama LIKE '%$keyword%' 
            $condition 
            ORDER BY a.rating DESC
            $limit ")->result();
    }

    function penjual_produk($id_barang = '')
    {
        return $this->db->query("
            SELECT a.id_penjual, b.fcm_id, a.nama
            FROM ms_barang a 
            INNER JOIN tb_user b ON a.id_penjual = b.id 
            WHERE a.id = '$id_barang' ")->row();
    }

    function stok($id_user = '', $keyword = '', $start = 0, $count = 0, $jenis = '', $donasi = 0, $aktif = 1)
    {
        $path = path_produk();

        $condition = '';
        if ($keyword != '') {
            $condition .= " AND a.nama LIKE '%$keyword%' ";
        }

        if ($donasi == 0) {
            if ($jenis != '') {
                $condition .= " AND a.id_jenis = '$jenis' ";
            }
        } else {
            $condition .= " AND a.donasi = 1 ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        if ($aktif == 0) {
            $condition .= " AND IFNULL(b.stok, 0) <= 0 ";
        }

        return $this->db->query("
            SELECT a.`id`, a.`kode`, a.`nama`, 
            IFNULL(CONCAT('$path', image.image), '') AS image, 
            IFNULL(b.`stok`, 0) AS stok, 
            IFNULL(terjual.jumlah, 0) AS terjual,
            a.harga 
            FROM ms_barang a
            LEFT JOIN ms_stok b ON a.`id` = b.id_barang
            LEFT JOIN (
                SELECT SUM(d.`jumlah`) AS jumlah, d.`id_barang`
                FROM tb_transaksi c 
                INNER JOIN detail_transaksi d ON c.`nobukti` = d.`nobukti`
                WHERE c.`status` = 3
            ) AS terjual ON terjual.id_barang = a.`id`
            INNER JOIN (
                SELECT *
                FROM tb_image_barang e
                WHERE e.`main` = 1
            ) AS image ON image.id_barang = a.`id`
            WHERE a.`status` = 1
            AND a.`id_penjual` = '$id_user' 
            $condition 
            $limit ")->result();   
    }

    function stok_lelang($id_user = '', $keyword = '', $start = 0, $count = 0, $aktif = 1)
    {
        $path = path_produk();
        $condition = '';
        if ($keyword != '') {
            $condition .= " AND b.nama LIKE '%$keyword%' ";
        }

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        if ($aktif == 0) {
            $condition .= " AND a.`end` < NOW() ";
        } else {
            $condition .= " AND NOW() BETWEEN a.`start` AND a.`end` ";
        }

        return $this->db->query("
            SELECT b.`id`, b.`kode`, b.`nama`, 
            IFNULL(CONCAT('$path', image.image), '') AS image,
            a.bid_akhir AS harga
            FROM tb_lelang a
            INNER JOIN ms_barang b ON a.`id_barang` = b.`id`
            LEFT JOIN (
                SELECT *
                    FROM tb_image_barang e
                    WHERE e.`main` = 1
            ) AS image ON image.id_barang = b.`id`
            WHERE 1 = 1
            AND b.id_penjual = '$id_user' 
            $condition 
            $limit ")->result();
    }
}

/* End of file Produk_Model.php */
/* Location: ./application/models/Produk_Model.php */
