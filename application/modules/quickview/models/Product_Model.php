<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function details($id)
	{
		$q = $this->db->where(['id' => $id])->get('ms_barang')->row();

		$data['nama'] = isset($q->nama) ? $q->nama : '';
		$data['presentase'] = isset($q->rating) ? (($q->rating / 5) * 100) : 0;
		$data['price'] = isset($q->harga) ? $q->harga : 0;
		$data['deskripsi'] = isset($q->deskripsi) ? $q->deskripsi : '';

		$image = $this->db->where(['id_barang' => $id])->get('tb_image_barang')->result(); 

		$data['images'] = $image;

		return $data;
	}
}

/* End of file Product_Model.php */
/* Location: ./application/models/Product_Model.php */ ?>