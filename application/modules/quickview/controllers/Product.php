<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('quickview/Product_Model', 'Product');
	}

	function details()
	{
		$id = $this->input->get('id');

		if ($this->input->is_ajax_request()) {
			$result = '';

			$data = $this->Product->details($id);

			if ($data) {
				$result .= $this->load->view('quickview/product-details', $data, true);
			}

			echo $result;
		}
	}
}

/* End of file Product.php */
/* Location: ./application/controllers/Product.php */ ?>