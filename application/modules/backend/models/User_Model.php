<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function data_user($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['profile_name', 'email', 'no_telp', 'type', 'waktu_daftar'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'profile_name', '2' => 'email', '3' => 'no_telp', '4' => 'type', '5' => 'foto', '6' => 'insert_at'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
        	SELECT * 
        	FROM (
	        	SELECT *, tgl_indo(a.`insert_at`, 1) AS waktu_daftar
				FROM tb_user a 
			) AS `user`
			WHERE penjual = 0
			$condition 
			$order 
			LIMIT $start, $length")->result();
        return $query;
    }

    function total_user($search = '')
    {
        $kolom = ['profile_name', 'email', 'no_telp', 'type', 'waktu_daftar'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah 
        	FROM (
	        	SELECT *, tgl_indo(a.`insert_at`, 1) AS waktu_daftar
				FROM tb_user a 
			) AS `user`
			WHERE penjual = 0
			$condition ")->row();
        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_user($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_user($search);
        $data = [];
        $req  = $this->data_user($start, $length, $search, $column, $dir);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_delete = btn_delete($val->id);
                $btn_edit = btn_custom($val->id, 'update', 'javascript:;', 'btn-info', 'fa fa-star', 'Daftarkan Sebagai Public Figure');

                if ($val->status == 1) {
                    $btn_ban = btn_custom($val->id, 'banned', 'javascript:;', 'btn-danger', 'fa fa-ban', 'Non Aktifkan User ini');
                    $btn_group = [$btn_edit, $btn_ban];
                } else {
                    $btn_ban = btn_custom($val->id, 'banned', 'javascript:;', 'btn-success', 'fa fa-check', 'Aktifkan User ini');
                    $btn_group = [$btn_ban];
                }

                $data[] = array(
                    $no++,
                    $val->profile_name,
                    $val->email,
                    $val->no_telp,
                    $val->type,
                    '<img src="'.$val->foto.'" width="48" class="image-modal" style="cursor:pointer;">',
                    $val->waktu_daftar,
                    btn_group($btn_group)
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }

    function data_selebriti($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['profile_name', 'email', 'no_telp', 'type', 'waktu_daftar'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'profile_name', '2' => 'email', '3' => 'no_telp', '4' => 'type', '5' => 'foto', '6' => 'insert_at'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
        	SELECT * 
        	FROM (
	        	SELECT *, tgl_indo(a.`insert_at`, 1) AS waktu_daftar
				FROM tb_user a 
			) AS `user`
			WHERE penjual = 1
			$condition 
			$order 
			LIMIT $start, $length")->result();
        return $query;
    }

    function total_selebriti($search = '')
    {
        $kolom = ['profile_name', 'email', 'no_telp', 'type', 'waktu_daftar'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah 
        	FROM (
	        	SELECT *, tgl_indo(a.`insert_at`, 1) AS waktu_daftar
				FROM tb_user a 
			) AS `user`
			WHERE penjual = 1
			$condition ")->row();
        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_selebriti($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_selebriti($search);
        $data = [];
        $req  = $this->data_selebriti($start, $length, $search, $column, $dir);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_delete = btn_delete($val->id);
                $btn_edit = btn_custom($val->id, 'update', 'javascript:;', 'btn-warning', 'fa fa-pencil', 'Edit Data');
                $btn_ktp = btn_custom($val->id, 'ktp', $val->upload_ktp, 'btn-info', 'fa fa-vcard-o', 'Download File KTP');
                $btn_lampiran = '';
                if ($val->lampiran != '') {
                    $btn_lampiran = btn_custom($val->id, 'lampiran', $val->lampiran, 'btn-info', 'fa fa-paperclip', 'Download File Lampiran');
                }

                if ($val->status == 1) {
                    $btn_ban = btn_custom($val->id, 'banned', 'javascript:;', 'btn-danger', 'fa fa-ban', 'Non Aktifkan User ini');
                    $btn_group = [$btn_edit, $btn_ban, $btn_ktp, $btn_lampiran];
                } else {
                    $btn_ban = btn_custom($val->id, 'banned', 'javascript:;', 'btn-success', 'fa fa-check', 'Aktifkan User ini');
                    $btn_group = [$btn_ban, $btn_ktp, $btn_lampiran];
                }

                $data[] = array(
                    $no++,
                    $val->profile_name,
                    $val->email,
                    $val->no_telp,
                    $val->type,
                    '<img src="'.$val->foto.'" width="48" class="image-modal" style="cursor:pointer;">',
                    $val->waktu_daftar,
                    btn_group($btn_group)
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }
}

/* End of file User_Model.php */
/* Location: ./application/models/User_Model.php */
