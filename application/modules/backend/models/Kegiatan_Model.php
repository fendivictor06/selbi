<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Kegiatan_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function data_kegiatan($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['penjual', 'judul', 'tanggal', 'tempat', 'deskripsi'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'penjual', '2' => 'judul', '3' => 'tgl', '4' => 'tempat', '5' => 'deskripsi'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
        	SELECT *
        	FROM view_kegiatan a
        	WHERE 1 = 1
			$condition 
			$order 
			LIMIT $start, $length")->result();
        return $query;
    }

    function total_kegiatan($search = '')
    {
        $kolom = ['penjual', 'judul', 'tanggal', 'tempat', 'deskripsi'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
        	SELECT COUNT(id) AS jumlah
        	FROM view_kegiatan a
        	WHERE 1 = 1
			$condition ")->row();
        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_kegiatan($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_kegiatan($search);
        $data = [];
        $req  = $this->data_kegiatan($start, $length, $search, $column, $dir);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_edit = btn_edit($val->id);
                $btn_delete = btn_delete($val->id);

                $data[] = array(
                    $no++,
                    $val->penjual,
                    $val->judul,
                    $val->tanggal,
                    $val->tempat,
                    limit_str($val->deskripsi),
                    btn_group([$btn_delete])
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }
}

/* End of file Kegiatan_Model.php */
/* Location: ./application/models/Kegiatan_Model.php */
