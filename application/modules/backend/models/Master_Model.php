<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_kota()
    {
        return $this->db->query("
    		SELECT a.*, b.provinsi
    		FROM ms_kota a 
    		INNER JOIN ms_provinsi b ON a.id_provinsi = b.id
    		WHERE a.status = 1 ")->result();
    }

    function ajax_kota($provinsi = '', $search = '')
    {
        return $this->db->query("
            SELECT *
            FROM ms_kota 
            WHERE id_provinsi = '$provinsi' 
            AND kota LIKE '%$search%'")->result();
    }
}

/* End of file Master_Model.php */
/* Location: ./application/models/Master_Model.php */
