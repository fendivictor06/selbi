<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Complaint_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function data_complaint($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['email', 'judul', 'deskripsi', 'tanggal'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'email', '2' => 'judul', '3' => 'deskripsi', '4' => 'tgl'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
        	SELECT * 
        	FROM (
	        	SELECT *, tgl_indo(a.tgl, 2) AS tanggal
	        	FROM tb_komplain a
	        ) AS komplain
        	WHERE 1 = 1
			$condition 
			$order 
			LIMIT $start, $length")->result();
        return $query;
    }

    function total_complaint($search = '')
    {
        $kolom = ['email', 'judul', 'deskripsi', 'tanggal'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
        	SELECT COUNT(id) AS jumlah
        	FROM (
	        	SELECT *, tgl_indo(a.tgl, 2) AS tanggal
	        	FROM tb_komplain a
	        ) AS komplain
        	WHERE 1 = 1
			$condition ")->row();
        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_complaint($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_complaint($search);
        $data = [];
        $req  = $this->data_complaint($start, $length, $search, $column, $dir);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_edit = btn_edit($val->id);
                $btn_delete = btn_delete($val->id);

                $data[] = array(
                    $no++,
                    $val->email,
                    $val->judul,
                    limit_str($val->deskripsi),
                    $val->tanggal,
                    '<img src="'.$val->path.$val->gambar.'" width="48" class="image-modal" style="cursor:pointer;">',
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }
}

/* End of file Complaint_Model.php */
/* Location: ./application/models/Complaint_Model.php */
