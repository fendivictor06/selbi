<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Merchandise_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function data_merchandise($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.nama', 'a.deskripsi', 'a.harga', 'c.category'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'a.nama', '2' => 'a.deskripsi', '3' => 'a.harga', '4' => 'c.category'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
        	SELECT a.*, c.`category`, IFNULL(image.image, '') AS image
			FROM ms_merchandise a
			LEFT JOIN ms_category c ON c.`id` = a.`id_category`
            LEFT JOIN (
                SELECT id, id_merchandise, image
                FROM tb_image_merchandise 
                WHERE main = 1
            ) AS image ON image.id_merchandise = a.`id`
			WHERE a.status = 1
			$condition 
			$order 
			LIMIT $start, $length")->result();

        return $query;
    }

    function total_merchandise($search = '')
    {
        $kolom = ['a.nama', 'a.deskripsi', 'a.harga', 'c.category'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
        	SELECT COUNT(a.id) AS jumlah
			FROM ms_merchandise a
			LEFT JOIN ms_category c ON c.`id` = a.`id_category`
            LEFT JOIN (
                SELECT id, id_merchandise, image
                FROM tb_image_merchandise 
                WHERE main = 1
            ) AS image ON image.id_merchandise = a.`id`
			WHERE a.status = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_merchandise($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_merchandise($search);
        $data = [];
        $req  = $this->data_merchandise($start, $length, $search, $column, $dir);
        $path = path_merchandise();
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_edit = btn_edit($val->id);
                $btn_delete = btn_delete($val->id);

                $data[] = array(
                    $no++,
                    $val->nama,
                    '<img src="'.$path.$val->image.'" width="48" class="image-modal" style="cursor:pointer;">',
                    limit_str($val->deskripsi),
                    uang($val->harga),
                    $val->category,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }

    function data_order($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['pembeli', 'merchandise', 'tanggal', 'harga', 'jumlah', 'total', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'pembeli', '2' => 'merchandise', '3' => 'tgl', '4' => 'harga', '5' => 'jumlah', '6' => 'total', '7' => 'keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
            SELECT * 
            FROM (
                SELECT a.`id`, a.`id_user`, c.`profile_name` AS pembeli,
                a.`id_merchandise`, b.`nama` AS merchandise,
                a.`harga`, a.`jumlah`, a.`keterangan`, a.`total`, a.`insert_at`,
                IFNULL(image.image, '') AS image, a.tgl, DATE_FORMAT(a.tgl, '%d/%m/%Y') AS tanggal,
                status_order_merchandise(a.status) AS status
                FROM temp_order_merchandise a
                INNER JOIN ms_merchandise b ON a.`id_merchandise` = b.`id`
                INNER JOIN tb_user c ON c.`id` = a.`id_user`
                LEFT JOIN (
                    SELECT id, id_merchandise, image
                    FROM tb_image_merchandise 
                    WHERE main = 1
                ) AS image ON image.id_merchandise = a.`id_merchandise`
            ) AS order_merchandise
            WHERE 1 = 1
            $condition 
            $order 
            LIMIT $start, $length")->result();

        return $query;
    }

    function total_order($search = '')
    {
        $kolom = ['pembeli', 'merchandise', 'tanggal', 'harga', 'jumlah', 'total', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
            SELECT COUNT(id) AS jumlah 
            FROM (
                SELECT a.`id`, a.`id_user`, c.`profile_name` AS pembeli,
                a.`id_merchandise`, b.`nama` AS merchandise,
                a.`harga`, a.`jumlah`, a.`keterangan`, a.`total`, a.`insert_at`,
                IFNULL(image.image, '') AS image, a.tgl, DATE_FORMAT(a.tgl, '%d/%m/%Y') AS tanggal,
                status_order_merchandise(a.status) AS status
                FROM temp_order_merchandise a
                INNER JOIN ms_merchandise b ON a.`id_merchandise` = b.`id`
                INNER JOIN tb_user c ON c.`id` = a.`id_user`
                LEFT JOIN (
                    SELECT id, id_merchandise, image
                    FROM tb_image_merchandise 
                    WHERE main = 1
                ) AS image ON image.id_merchandise = a.`id_merchandise`
            ) AS order_merchandise
            WHERE 1 = 1
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_order($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_order($search);
        $data = [];
        $req  = $this->data_order($start, $length, $search, $column, $dir);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_edit = btn_edit($val->id);
                $btn_delete = btn_delete($val->id);

                $btn_order = btn_custom($val->id, 'orders', 'javascript:;', 'btn-success', 'fa fa-list', 'Input Desain');
                // $btn_view = btn_custom($val->id, 'views', 'javascript:;', 'btn-info', 'fa fa-eye', 'Lihat Detail');

                $data[] = array(
                    $no++,
                    $val->pembeli,
                    $val->merchandise,
                    $val->tanggal,
                    uang($val->harga),
                    $val->jumlah,
                    uang($val->total),
                    limit_str($val->keterangan),
                    $val->status,
                    btn_group([$btn_order])
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }

    function add_merchandise($data = [], $unik = '', $attribute = [], $suggest = [])
    {
        $user_id = username();

        $this->db->trans_begin();

        # simpan merchandise
        $this->db->insert('ms_merchandise', $data);
        $id_barang = $this->db->insert_id();
        # generate kode barang
        switch (strlen($id_barang)) {
            case '1':
                $kode_barang = '0000'.$id_barang;
                break;
            case '2':
                $kode_barang = '000'.$id_barang;
                break;
            case '3':
                $kode_barang = '00'.$id_barang;
                break;
            case '4':
                $kode_barang = '0'.$id_barang;
                break;
            default:
                $kode_barang = $id_barang;
                break;
        }
        # update kode
        $this->db->where('id', $id_barang)->update('ms_merchandise', ['kode' => $kode_barang]);

        # insert attribute
        if ($attribute) {
            for ($i = 0; $i < count($attribute); $i++) {
                $id_attribute = isset($attribute[$i]['id']) ? $attribute[$i]['id'] : '';
                if ($id_attribute != '') {
                    $value_id = isset($attribute[$i]['value']) ? $attribute[$i]['value'] : [];
                    if ($value_id) {
                        for ($x = 0; $x < count($value_id); $x++) {
                            $id_item = isset($value_id[$x]) ? $value_id[$x] : '';

                            $item_detail = $this->db->where('id', $id_item)->get('ms_item_attribute')->row();
                            $value_item = isset($item_detail->value) ? $item_detail->value : '';
                            $kode_warna = isset($item_detail->kode_warna) ? $item_detail->kode_warna : '';
                            $thumbnail = isset($item_detail->thumbnail) ? $item_detail->thumbnail : '';

                            $attribute_merchandise = array(
                                'id_merchandise' => $id_barang,
                                'id_item' => $id_item,
                                'id_attribute' => $id_attribute,
                                'detail_attribute' => $value_item,
                                'kode_warna' => $kode_warna,
                                'thumbnail' => $thumbnail
                            );

                            $this->db->insert('ms_merchandise_detail', $attribute_merchandise);
                        }
                    }
                }
            }
        }

        # imagenya
        $images = $this->db->where(['user_insert' => $user_id, 'unik' => $unik])
                    ->get('temp_image_merchandise')
                    ->result();

        if ($images) {
            $i = 0;
            foreach ($images as $img) {
                $file_type = explode('.', $img->image);
                $file_name = isset($file_type[0]) ? $file_type[0] : '';
                $file_type = end($file_type);
                
                $new_name = date('YmdHis').$file_name.'.'.$file_type;

                $main = ($i == 0) ? '1' : '0';

                $old_path = './assets/uploads/merchandise/temp/';
                $new_path = './assets/uploads/merchandise/';

                $image = array(
                    'id_merchandise' => $id_barang,
                    'path' => path_merchandise(),
                    'image' => $new_name,
                    'main' => $main,
                    'user_insert' => $user_id
                );

                if (copy($old_path.$img->image, $new_path.$new_name) == true) {
                    $this->db->insert('tb_image_merchandise', $image);

                    if (unlink($old_path.$img->image) == true) {
                        $this->db->where(['user_insert' => $user_id, 'unik' => $unik, 'image' => $img->image])
                            ->delete('temp_image_merchandise');
                    }
                }

                $i++;
            }
        }

        if ($suggest) {
            # delete suggest 
            $this->db->where('id_merchandise', $id_barang)
                    ->delete('tb_suggestion');

            for ($i = 0; $i < count($suggest); $i++) {
                $this->db->insert('tb_suggestion', [
                    'id_merchandise' => $id_barang, 
                    'id_user' => $suggest[$i], 
                    'user_insert' => $user_id
                ]);
            }
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $result = false;
        } else {
            $this->db->trans_commit();

            $result = true;
        }

        return $result;
    }

    function update_merchandise($data = [], $unik = '', $id = '', $attribute = [], $suggest = [])
    {
        $user_id = username();

        $this->db->trans_begin();

        # simpan merchandise
        $this->db->where('id', $id)
            ->update('ms_merchandise', $data);

        # hapus attribute
        $this->db->where('id_merchandise', $id)->delete('ms_merchandise_detail');

        # insert attribute
        if ($attribute) {
            for ($i = 0; $i < count($attribute); $i++) {
                $id_attribute = isset($attribute[$i]['id']) ? $attribute[$i]['id'] : '';
                if ($id_attribute != '') {
                    $value_id = isset($attribute[$i]['value']) ? $attribute[$i]['value'] : [];
                    if ($value_id) {
                        for ($x = 0; $x < count($value_id); $x++) {
                            $id_item = isset($value_id[$x]) ? $value_id[$x] : '';

                            $item_detail = $this->db->where('id', $id_item)->get('ms_item_attribute')->row();
                            $value_item = isset($item_detail->value) ? $item_detail->value : '';
                            $kode_warna = isset($item_detail->kode_warna) ? $item_detail->kode_warna : '';
                            $thumbnail = isset($item_detail->thumbnail) ? $item_detail->thumbnail : '';

                            $attribute_merchandise = array(
                                'id_merchandise' => $id,
                                'id_attribute' => $id_attribute,
                                'id_item' => $id_item,
                                'detail_attribute' => $value_item,
                                'kode_warna' => $kode_warna,
                                'thumbnail' => $thumbnail
                            );

                            $this->db->insert('ms_merchandise_detail', $attribute_merchandise);
                        }
                    }
                }
            }
        }

        # imagenya
        $images = $this->db->where(['user_insert' => $user_id, 'unik' => $unik])
                    ->get('temp_image_merchandise')
                    ->result();

        # exists main image
        $exists = $this->db->where(['id_merchandise' => $id, 'main' => 1])
                    ->get('temp_image_merchandise')
                    ->row();

        if ($images) {
            $i = 0;
            foreach ($images as $img) {
                $file_type = explode('.', $img->image);
                $file_name = isset($file_type[0]) ? $file_type[0] : '';
                $file_type = end($file_type);
                
                $new_name = date('YmdHis').$file_name.'.'.$file_type;

                $main = ($i == 0 && empty($exists)) ? '1' : '0';

                $old_path = './assets/uploads/merchandise/temp/';
                $new_path = './assets/uploads/merchandise/';

                $image = array(
                    'id_merchandise' => $id,
                    'path' => path_merchandise(),
                    'image' => $new_name,
                    'main' => $main,
                    'user_insert' => $user_id
                );

                if (copy($old_path.$img->image, $new_path.$new_name) == true) {
                    $this->db->insert('tb_image_merchandise', $image);

                    if (unlink($old_path.$img->image) == true) {
                        $this->db->where(['user_insert' => $user_id, 'unik' => $unik, 'image' => $img->image])
                            ->delete('temp_image_merchandise');
                    }
                }

                $i++;
            }
        }

        if ($suggest) {
            # delete suggest 
            $this->db->where('id_merchandise', $id)
                    ->delete('tb_suggestion');

            for ($i = 0; $i < count($suggest); $i++) {
                $this->db->insert('tb_suggestion', [
                    'id_merchandise' => $id, 
                    'id_user' => $suggest[$i], 
                    'user_insert' => $user_id
                ]);
            }
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $result = false;
        } else {
            $this->db->trans_commit();

            $result = true;
        }

        return $result;
    }

    function add_penawaran($id = '', $deskripsi = '', $unik = '')
    {
        $user_id = username();

        $this->db->trans_begin();

        $order = $this->db->where('id', $id)
                    ->get('temp_order_merchandise')
                    ->row();

        $id_user = isset($order->id_user) ? $order->id_user : '';
        $id_merchandise = isset($order->id_merchandise) ? $order->id_merchandise : '';

        # update status 2
        $this->db->where('id', $id)->update('temp_order_merchandise', ['status' => 2]);
 
        $data = array(
            'id_order' => $id,
            'id_merchandise' => $id_merchandise,
            'id_user' => $id_user,
            'status' => 1,
            'keterangan' => $deskripsi,
            'user_insert' => $user_id
        );

        # simpan merchandise
        $this->db->insert('temp_penawaran', $data);
        $simpan = $this->db->insert_id();

        # imagenya
        $images = $this->db->where(['user_insert' => $user_id, 'unik' => $unik])
                    ->get('temp_image_merchandise')
                    ->result();

        if ($images) {
            $i = 0;
            foreach ($images as $img) {
                $file_type = explode('.', $img->image);
                $file_name = isset($file_type[0]) ? $file_type[0] : '';
                $file_type = end($file_type);
                
                $new_name = date('YmdHis').$file_name.'.'.$file_type;

                $old_path = './assets/uploads/merchandise/temp/';
                $new_path = './assets/uploads/penawaran/';

                $image = array(
                    'id_penawaran' => $simpan,
                    'path' => path_penawaran(),
                    'image' => $new_name,
                    'user_insert' => $user_id
                );

                if (copy($old_path.$img->image, $new_path.$new_name) == true) {
                    $this->db->insert('temp_image_penawaran', $image);

                    if (unlink($old_path.$img->image) == true) {
                        $this->db->where(['user_insert' => $user_id, 'unik' => $unik, 'image' => $img->image])
                            ->delete('temp_image_merchandise');
                    }
                }

                $i++;
            }
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $result = false;
        } else {
            $this->db->trans_commit();

            $result = true;
        }

        return $result;
    }

    function detail_penawaran($id = '')
    {
        $penawaran = $this->db->query("
            SELECT a.*, status_order_merchandise(a.status) AS progress,
            b.profile_name AS admin, c.profile_name AS userid,
            DATE_FORMAT(a.insert_at, '%d/%m/%Y %H:%i') AS waktu
            FROM temp_penawaran a 
            LEFT JOIN tb_admin b ON b.username = a.user_insert
            LEFT JOIN tb_user c ON c.uid = a.user_insert
            WHERE a.id_order = '$id' 
            ORDER BY a.insert_at DESC")->result();

        $response = [];
        if ($penawaran) {
            foreach ($penawaran as $row => $val) {
                $id_penawaran = $val->id;
                $response[$row] = array(
                    'keterangan' => $val->keterangan,
                    'status' => $val->status,
                    'progress' => $val->progress,
                    'admin' => $val->admin,
                    'userid' => $val->userid,
                    'waktu' => $val->waktu
                );

                $images = $this->db->query("
                    SELECT *
                    FROM temp_image_penawaran a 
                    WHERE a.id_penawaran = '$id_penawaran'")->result();

                if ($images) {
                    foreach ($images as $img) {
                        $response[$row]['images'][] = $img->path.$img->image;
                    }
                }
            }
        }

        return $response;
    }

    function select_figure($search = '', $start = 0, $limit = 0)
    {
        $search = $this->db->escape_str($search);
        $start = $this->db->escape_str($start);
        $limit = $this->db->escape_str($limit);

        $start = $start * $limit;
        $condition = '';
        if ($search != '') {
            $condition .= " AND profile_name LIKE '%$search%' ";
        }

        return $this->db->query("
            SELECT id, profile_name AS text 
            FROM tb_user 
            WHERE status = 1
            AND penjual = 1
            $condition 
            LIMIT $start, $limit")->result();
    }   

    function count_figure($search = '') 
    {
        $search = $this->db->escape_str($search);

        $condition = '';
        if ($search != '') {
            $condition .= " AND profile_name LIKE '%$search%' ";
        }

        $query = $this->db->query("
            SELECT COUNT(id) AS jumlah
            FROM tb_user 
            WHERE status = 1
            AND penjual = 1
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function merchandise_suggestion($id_merchandise = '')
    {
        return $this->db->query("
            SELECT b.id, b.profile_name AS text
            FROM tb_suggestion a 
            INNER JOIN tb_user b ON a.id_user = b.id 
            WHERE a.id_merchandise = '$id_merchandise' ")->result();
    }
}

/* End of file Merchandise_Model.php */
/* Location: ./application/models/Merchandise_Model.php */
