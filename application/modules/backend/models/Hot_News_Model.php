<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hot_News_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function json_news($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
	{
		$start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_news($search);
        $data = [];
        $req  = $this->data_news($start, $length, $search, $column, $dir);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
            	$btn_view = btn_custom($val->id, 'view-news', 'javascript:;', 'btn-info', 'fa fa-eye');
                $btn_edit = btn_edit($val->id);
                $btn_delete = btn_delete($val->id);

                $image = ($val->image != '') ? '<img src="'.path_hotnews().$val->image.'" width="150" class="image-modal" style="cursor:pointer" />' : '';

                $data[] = array(
                    $no++,
                    $val->judul,
                    $image,
                    $val->waktu,
                    btn_group([$btn_edit, $btn_delete, $btn_view])
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
	}

	function data_news($start = 0, $length = 0, $search = '', $column = '', $dir = '')
	{
		$kolom = ['judul', 'teks', 'waktu'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'judul', '3' => 'waktu'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
        	SELECT *
			FROM (
				SELECT a.`id`, a.`judul`, a.`image`, a.`teks`, 
				tgl_indo(a.`insert_at`, 1) AS waktu
				FROM tb_news a
			) AS hotnews
        	WHERE 1 = 1
			$condition 
			$order 
			LIMIT $start, $length")->result();

        return $query;
	}

	function total_news($search = '')
	{
		$kolom = ['judul', 'teks', 'waktu'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
        	SELECT COUNT(id) AS jumlah
			FROM (
				SELECT a.`id`, a.`judul`, a.`image`, a.`teks`, 
				tgl_indo(a.`insert_at`, 1) AS waktu
				FROM tb_news a
			) AS hotnews
        	WHERE 1 = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
	}
}

/* End of file Hot_News_Model.php */
/* Location: ./application/models/Hot_News_Model.php */ ?>