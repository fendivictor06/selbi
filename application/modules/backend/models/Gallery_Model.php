<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Gallery_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_gallery($start = 0, $count = 0, $search = '')
    {
        $condition = '';
        if ($search != '') {
            $condition .= "AND (a.judul LIKE '%$search%' OR c.profile_name LIKE '%$search%')";
        }

        return $this->db->query("
    		SELECT a.*, IFNULL(image_feed.image, '') AS image,
    		c.`profile_name`, DATE_FORMAT(a.insert_at, '%d/%m/%Y %H:%i') AS tgl
			FROM tb_feed a
			LEFT JOIN (
				SELECT *
				FROM tb_feed_image 
				GROUP BY id_feed
			) AS image_feed ON image_feed.id_feed = a.`id`
			INNER JOIN tb_user c ON c.`id` = a.`id_penjual`
			WHERE (a.`jenis` = '2'
			OR a.`jenis` = '3') 
			$condition 
			ORDER BY a.insert_at DESC
			LIMIT $start, $count ")->result();
    }

    function view_feed($id_feed = '')
    {
        return $this->db->query("
    		SELECT a.id, b.profile_name, DATE_FORMAT(a.insert_at, '%d/%m/%Y %H:%i') AS tgl
    		, a.judul
    		FROM tb_feed a 
    		INNER JOIN tb_user b ON a.id_penjual = b.id 
    		WHERE a.id = '$id_feed' ")->row();
    }

    function hapus_gallery($id = '')
    {
        $path_gallery = './assets/uploads/feed/gallery/';
        $arr_image = [];

         # unlink image
        $image = $this->db->where('id_feed', $id)
                    ->get('tb_feed_image')
                    ->result_array();
        if ($image) {
            foreach ($image as $row) {
                $arr_image[] = $row['image'];
            }
        }

        $this->db->trans_begin();
        # delete data
        $this->db->where('id_feed', $id)
            ->delete('tb_feed_image');
        $this->db->where('id', $id)
            ->delete('tb_feed');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $result = false;
        } else {
            $this->db->trans_commit();

            # delete images
            if ($arr_image) {
                for ($i = 0; $i < count($arr_image); $i++) {
                    $picture = isset($arr_image[$i]) ? $arr_image[$i] : '';
                    if ($picture != '') {
                        unlink($path_gallery.$picture);
                    }
                }
            }

            $result = true;
        }

        return $result;
    }
}

/* End of file Gallery_Model.php */
/* Location: ./application/models/Gallery_Model.php */
