<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Auth_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function is_valid_user($username = '', $password = '')
    {
        return $this->db->query("
			SELECT *
			FROM tb_admin a
			WHERE a.`username` = '$username'
			AND a.`password` = AES_ENCRYPT('$password', a.`key`)
			AND a.`status` = 1")->row();
    }

    function update_session($id = '', $remember = '')
    {
        $now = now();
        $session = md5(generate_random(4));
        $ip = $this->input->ip_address();

        $data = array(
            'session' => $session,
            'last_login' => $now,
            'last_ip' => $ip
        );

        $this->db->where('id', $id)
            ->update('tb_admin', $data);

        if ($remember == 1) {
            $this->create_cookies($session);
        }

        return $session;
    }

    function create_cookies($session = '')
    {
        $name = 'selbi-cookie';
        $expire = '8640000';
        $domain = base_url();
        
        delete_cookie($name);

        $cookie = array(
            'name' => $name,
            'value' => $session,
            'expire' => $expire
        );

        $this->input->set_cookie($cookie);
    }

    function login($username = '', $password = '', $remember = '')
    {
        $is_valid_user = $this->is_valid_user($username, $password);

        $data = [];
        if (! empty($is_valid_user)) {
            $status = true;
            $message = 'Login berhasil.';
            $data = $is_valid_user;

            $id = isset($data->id) ? $data->id : '';
            $username = isset($data->username) ? $data->username : '';
            $level = isset($data->level) ? $data->level : '';
            $profile_name = isset($data->profile_name) ? $data->profile_name : '';
            $session = $this->update_session($id, $remember);

            $set_session = array(
                'username' => $username,
                'level' => $level,
                'profile_name' => $profile_name,
                'session' => $session
            );
            
            $this->session->set_userdata($set_session);
        } else {
            $status = false;
            $message = 'Username atau password salah. silahkan ulangi lagi.';
        }

        $result = array(
            'status' => $status,
            'message' => $message
        );

        return $result;
    }

    function is_valid_cookie()
    {
        $get_cookie = get_cookie('selbi-cookie', true);
        $is_valid = $this->db->where('session', $get_cookie)
                        ->get('tb_admin')
                        ->row();

        $result = (! empty($is_valid)) ? true : false;

        if ($result == true && $get_cookie != '' && $get_cookie != null) {
            $username = isset($is_valid->username) ? $is_valid->username : '';
            $level = isset($is_valid->level) ? $is_valid->level : '';
            $profile_name = isset($is_valid->profile_name) ? $is_valid->profile_name : '';
            $session = $get_cookie;

            $set_session = array(
                'username' => $username,
                'level' => $level,
                'profile_name' => $profile_name,
                'session' => $session
            );
            
            $this->session->set_userdata($set_session);
        }

        return $result;
    }

    function is_valid_session()
    {
        $username = $this->session->userdata('username');
        $level = $this->session->userdata('level');
        $profile_name = $this->session->userdata('profile_name');
        $session = $this->session->userdata('session');

        $where = array(
            'username' => $username,
            'level' => $level,
            'profile_name' => $profile_name,
            'session' => $session
        );

        $is_valid = $this->db->where($where)
                        ->get('tb_admin')
                        ->row();

        $result = (! empty($is_valid)) ? true : false;

        return $result;
    }

    function is_login()
    {
        $valid_cooke = $this->is_valid_cookie();
        $valid_session = $this->is_valid_session();

        $cookie_status = false;
        $session_status = false;
            
        if ($valid_cooke == true) {
            $valid_session = $this->is_valid_session();
            if ($valid_session == true) {
                $cookie_status = true;
            } else {
                $cookie_status = false;
            }
        }

        if ($valid_session == true) {
            $session_status = true;
        }

        return ($cookie_status == true || $session_status == true) ? true : redirect('backend/Login');
    }
}

/* End of file Auth_Model.php */
/* Location: ./application/models/Auth_Model.php */
