<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lelang_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function data_lelang($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['kode', 'nama', 'deskripsi', 'bid_awal', 'waktu'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'kode', '2' => 'nama', '3' => 'deskripsi', '4' => 'bid_awal', '5' => 'insert_at'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
        	SELECT *
        	FROM view_lelang a
        	WHERE flag_approved = 0
			$condition 
			$order 
			LIMIT $start, $length")->result();
        return $query;
    }

    function total_lelang($search = '')
    {
        $kolom = ['kode', 'nama', 'deskripsi', 'bid_awal', 'waktu'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
        	SELECT COUNT(id) AS jumlah
        	FROM view_lelang a
        	WHERE flag_approved = 0
			$condition ")->row();
        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_lelang($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_lelang($search);
        $data = [];
        $req  = $this->data_lelang($start, $length, $search, $column, $dir);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_edit = btn_edit($val->id);
                $btn_delete = btn_delete($val->id);

                // $btn_approve = btn_custom($val->id, 'approve', 'javascript:;', 'btn-info', 'fa fa-check', 'Setujui Lelang');
                $btn_details = btn_custom($val->id, 'details', 'javascript:;', 'btn-success', 'fa fa-check', 'Setujui Lelang');

                $btn_group = [];

                $data[] = array(
                    $no++,
                    $val->kode,
                    $val->nama,
                    limit_str($val->deskripsi),
                    uang($val->bid_awal),
                    $val->waktu,
                    btn_group([$btn_details])
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }

    function detail_lelang($id = '')
    {
        $head = $this->db->query("
            SELECT *
            FROM view_lelang a 
            WHERE a.id = '$id' ")->row();
        $path = path_produk();

        $response = [];
        if ($head) {
            $id_barang = isset($head->id_barang) ? $head->id_barang : '';
            $nama = isset($head->nama) ? $head->nama : '-';
            $category = isset($head->category) ? $head->category : '-';
            $brand = isset($head->brand) ? $head->brand : '-';
            $jenis = isset($head->jenis) ? $head->jenis : '-';
            $berat = isset($head->berat) ? $head->berat : 0;
            $berat_satuan = isset($head->berat_satuan) ? $head->berat_satuan : '-';
            $kondisi = isset($head->kondisi_barang) ? $head->kondisi_barang : '-';
            $penjual = isset($head->penjual) ? $head->penjual : '-';
            $deskripsi = isset($head->deskripsi) ? $head->deskripsi : '-';
            $bid_awal = isset($head->bid_awal) ? $head->bid_awal : 0;
            $status = isset($head->status_lelang) ? $head->status_lelang : '';
            $waktu = isset($head->waktu) ? $head->waktu : '-';

            $images = [];
            $image = $this->db->query("
                SELECT *
                FROM tb_image_barang a 
                WHERE a.id_barang = '$id_barang' ")->result();
            if ($image) {
                foreach ($image as $img) {
                    $images[] = $path.$img->image;
                }
            }

            $response = array(
                'id_barang' => $id_barang,
                'nama' => $nama,
                'category' => $category,
                'brand' => $brand,
                'jenis' => $jenis,
                'berat' => $berat,
                'berat_satuan' => $berat_satuan,
                'kondisi' => $kondisi,
                'penjual' => $penjual,
                'deskripsi' => $deskripsi,
                'bid_awal' => $bid_awal,
                'status' => $status,
                'waktu' => $waktu,
                'images' => $images
            );
        }

        return $response;
    }
}

/* End of file Lelang_Model.php */
/* Location: ./application/models/Lelang_Model.php */
