<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function jumlah_artis()
    {
        $query = $this->db->query("
    		SELECT COUNT(a.`id`) AS jumlah
			FROM tb_user a
			WHERE a.`penjual` = 1")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function permintaan_merchandise()
    {
        $query = $this->db->query("
            SELECT jumlah_permintaan() AS jumlah ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function new_user()
    {
        return $this->db->query("
            SELECT *
            FROM tb_user a
            ORDER BY a.`insert_at` DESC
            LIMIT 10 ")->result();
    }
}

/* End of file Dashboard_Model.php */
/* Location: ./application/models/Dashboard_Model.php */
