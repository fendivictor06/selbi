<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attribute_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function view_item()
	{
		$query = $this->db->query("
			SELECT a.*, b.`attribute`
			FROM ms_item_attribute a
			INNER JOIN ms_attribute b ON a.`id_attribute` = b.`id` 
			WHERE a.status = 1
			ORDER BY a.id_attribute ASC ")->result();

		return $query;
	}
}

/* End of file Attribute_Model.php */
/* Location: ./application/models/Attribute_Model.php */ ?>