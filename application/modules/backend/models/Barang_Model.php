<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Barang_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function data_barang($start = 0, $length = 0, $search = '', $column = '', $dir = '', $cond = '')
    {
        $kolom = ['a.kode', 'a.nama', 'a.deskripsi', 'a.harga', 'c.category', 'b.brand'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'kode', '2' => 'nama', '3' => 'deskripsi', '4' => 'harga', '5' => 'category', '6' => 'brand'];
        $order = order_datatable($kolom_order, $column, $dir);

        if (!empty($cond)) {
            $condition .= " AND ".$cond;
        }

        $query = $this->db->query("SELECT a.`id`, a.`kode`, a.`nama`, a.`deskripsi`, a.`harga`, c.`category`, b.`brand`, 
                                    p.`nama` AS penjual
                                    FROM ms_barang a 
                                    JOIN ms_category c ON a.`id_category`=c.`id`
                                    JOIN ms_brand b ON a.`id_brand`=b.`id`
                                    LEFT JOIN ms_penjual p ON a.`id_penjual`=p.`id`
                                    WHERE a.`status`=1
                                    AND a.`lelang`=0 
                                    $condition                                    
                                    $order
                                    LIMIT $start, $length ")->result();
        return $query;
    }

    function total_data_barang($search = '', $cond = '')
    {
        $result=0;
        $kolom = ['a.kode', 'a.nama', 'a.deskripsi', 'a.harga', 'c.category', 'b.brand'];
        $condition = search_datatable($kolom, $search);

        if (!empty($cond)) {
            $condition .= " AND ".$cond;
        }

        $query = $this->db->query("SELECT COUNT(*) AS jumlah
                                    FROM ms_barang a 
                                    JOIN ms_category c ON a.`id_category`=c.`id`
                                    JOIN ms_brand b ON a.`id_brand`=b.`id`
                                    LEFT JOIN ms_penjual p ON a.`id_penjual`=p.`id`
                                    WHERE a.`status`=1
                                    AND a.`lelang`=0 
                                    $condition ")->row();
        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_barang($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $condition = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_data_barang($search, $condition);
        $data = [];
        $req  = $this->data_barang($start, $length, $search, $column, $dir, $condition);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_edit = btn_edit($val->id);
                $btn_delete = btn_delete($val->id);

                $data[] = array(
                    $no++,
                    $val->kode,
                    $val->nama,
                    limit_str($val->deskripsi),
                    uang($val->harga),
                    $val->category,
                    $val->brand,
                    btn_group([$btn_delete])
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }

    function data_barang_id($id = '', $id_jenis = '')
    {
        return $this->db->query("SELECT a.*, c.`category`, b.`brand`, p.`nama` AS penjual
                                    FROM ms_barang a 
                                    JOIN ms_category c ON a.`id_category`=c.`id`
                                    JOIN ms_brand b ON a.`id_brand`=b.`id`
                                    LEFT JOIN ms_penjual p ON a.`id_penjual`=p.`id`
                                    WHERE a.`status`=1
                                        AND a.`id_jenis`='$id_jenis'
                                        AND a.`lelang`=0
                                        AND a.`id`='$id'")->row();
    }
}

/* End of file Barang_Model.php */
/* Create by - Akbar */
