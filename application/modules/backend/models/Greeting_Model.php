<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Greeting_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function json_greeting($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_greeting($search);
        $data = [];
        $req  = $this->data_greeting($start, $length, $search, $column, $dir);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {
                $btn_edit = btn_edit($val->id);
                $btn_delete = btn_delete($val->id);

                $data[] = array(
                    $no++,
                    $val->pemesan,
                    $val->artis,
                    $val->request,
                    $val->nominal,
                    $val->keterangan,
                    $val->status_order,
                    btn_group([$btn_delete])
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
    }

	function data_greeting($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['b.`profile_name`', 'c.`profile_name`', 'a.request', 'a.nominal', 'c.kategori', 'status_greeting(a.`status`)'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'b.`profile_name`', '2' => 'c.`profile_name`', '3' => 'a.request', '4' => 'a.nominal', '5' => 'c.kategori', '6' => 'status_greeting(a.`status`)'];
        $order = order_datatable($kolom_order, $column, $dir);

        $query = $this->db->query("
        	SELECT a.*, b.`profile_name` AS pemesan, c.`profile_name` AS artis,
			status_greeting(a.`status`) AS status_order
			FROM tb_greeting a
			INNER JOIN tb_user b ON a.`id_user` = b.`id` 
			INNER JOIN tb_user c ON c.id = a.`id_penjual`
			WHERE 1 = 1
            $condition                                    
            $order
            LIMIT $start, $length ")->result();

        return $query;
    }

    function total_greeting($search = '')
    {
        $kolom = ['1' => 'b.`profile_name`', '2' => 'c.`profile_name`', '3' => 'a.request', '4' => 'a.nominal', '5' => 'c.kategori', '6' => 'status_greeting(a.`status`)'];
        $condition = search_datatable($kolom, $search);

        $query = $this->db->query("
        	SELECT COUNT(a.id) AS jumlah
			FROM tb_greeting a
			INNER JOIN tb_user b ON a.`id_user` = b.`id` 
			INNER JOIN tb_user c ON c.id = a.`id_penjual`
			WHERE 1 = 1
            $condition ")->row();
        return isset($query->jumlah) ? $query->jumlah : 0;
    }

}

/* End of file Greeting_Model.php */
/* Location: ./application/models/Greeting_Model.php */ ?>