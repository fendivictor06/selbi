<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function json_transaksi($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '')
	{
		$start  = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);
        $datestart = $this->db->escape_str($datestart);
        $dateend = $this->db->escape_str($dateend);

        $datestart = convert_tgl($datestart);
        $dateend = convert_tgl($dateend);

        $total_filtered = $this->total_transaksi($search, $datestart, $dateend);
        $data = [];
        $req = $this->view_transaksi($start, $length, $search, $column, $dir, $datestart, $dateend);
        if (!empty($req)) {
            $no = $start + 1;
            foreach ($req as $row => $val) {

                $data[] = array(
                    $no++,
                    $val->nomidtrans,
                    $val->nobukti,
                    $val->profile_name,
                    $val->tgl,
                    $val->alamat,
                    uang($val->total),
                    $val->status_transaksi
                );
            }
        }
        return response_datatable($draw, $total_filtered, $data);
	}

	function view_transaksi($start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '')
	{
		$kolom = ['nomidtrans', 'nobukti', 'profile_name', 'tgl', 'alamat', 'total', 'status_transaksi'];
		$condition = search_datatable($kolom, $search);

		$kolom_order = ['1' => 'nomidtrans', '2' => 'nobukti', '3' => 'profile_name', '4' => 'tgl', '5' => 'alamat', '6' => 'total', '7' => 'status_transaksi'];
		$order = order_datatable($kolom_order, $column, $dir);

		return $this->db->query("
			SELECT *
			FROM (
				SELECT a.`nomidtrans`, a.`nobukti`, b.`profile_name`,
				tgl_indo(a.`tgl`, 1) AS tgl, a.`alamat`, a.`total`, a.`status`, 
				status_transaksi(a.`status`) AS status_transaksi, DATE(a.tgl) AS `date`	
				FROM tb_transaksi a
				INNER JOIN tb_user b ON a.`id_user` = b.`id`
			) AS transaksi  
			WHERE `date` BETWEEN '$datestart' AND '$dateend'
			$condition 
			$order
			LIMIT $start, $length ")->result();
	}

	function total_transaksi($search = '', $datestart = '', $dateend = '')
	{
		$kolom = ['nomidtrans', 'nobukti', 'profile_name', 'tgl', 'alamat', 'total', 'status_transaksi'];
		$condition = search_datatable($kolom, $search);

		$query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM (
				SELECT a.`nomidtrans`, a.`nobukti`, b.`profile_name`,
				tgl_indo(a.`tgl`, 1) AS tgl, a.`alamat`, a.`total`, a.`status`, 
				status_transaksi(a.`status`) AS status_transaksi	
				FROM tb_transaksi a
				INNER JOIN tb_user b ON a.`id_user` = b.`id`
				WHERE DATE(a.tgl) BETWEEN '$datestart' AND '$dateend'
			) AS transaksi  
			WHERE 1 = 1
			$condition ")->row();

		return isset($query->jumlah) ? $query->jumlah : 0;
	}
}

/* End of file Transaksi_Model.php */
/* Location: ./application/models/Transaksi_Model.php */ ?>