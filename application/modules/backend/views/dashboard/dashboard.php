<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php // echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <h3>
                            Selamat Datang, 
                            <small><?php echo profile_name(); ?></small>
                        </h3>
                        <div class="m-portlet__body  m-portlet__body--no-padding">
                            <div class="row m-row--no-padding m-row--col-separator-xl">
                                <?php echo isset($widget) ? $widget : ''; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>

            <div class="col-xl-6">
                <!--begin:: Widgets/New Users-->
                <div class="m-portlet m-portlet--full-height">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    User Baru
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="m_widget4_tab1_content">
                                <!--begin::Widget 14-->
                                <div class="m-widget4">
                                    <div id="new_user"> </div>
                                </div>
                                <!--end::Widget 14-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/New Users-->
            </div>
        </div>
    </div>
</div>
