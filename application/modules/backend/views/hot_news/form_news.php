<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Hot News
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Form Hot News
                            </span>
                        </a>
                    </li>
                </ul>       
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Form Hot News
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="form-data">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <input type="hidden" name="id" id="id" value="<?php echo isset($edit->id) ? $edit->id : ''; ?>">
                                <div class="col-lg-6">
                                    <label>
                                        Judul: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="judul" id="judul" class="form-control m-input" placeholder="Judul Berita" required="required" value="">
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Image: 
                                    </label>
                                    <input type="file" name="image" id="image" class="form-control">
                                </div>
                                <div class="col-lg-12" style="margin-top: 20px;">
                                    <label>
                                        Deskripsi: <span class="m--font-danger">*</span>
                                    </label>
                                    <div id="deskripsi"></div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                        <button type="reset" class="btn btn-secondary" id="clearform">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
