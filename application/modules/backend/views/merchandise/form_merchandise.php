<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Merchandise
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Form Merchandise
                            </span>
                        </a>
                    </li>
                </ul>       
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Form Merchandise
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="form-data">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <input type="hidden" name="id" id="id" value="<?php echo isset($edit->id) ? $edit->id : ''; ?>">
                                <div class="col-lg-6">
                                    <label>
                                        Nama Barang: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="nama" id="nama" class="form-control m-input" placeholder="Nama Merchandise" required="required" value="<?php echo isset($edit->nama) ? $edit->nama : ''; ?>">
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Harga: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="number" name="harga" id="harga" class="form-control m-input" placeholder="Harga Merchandise" required="required" value="<?php echo isset($edit->harga) ? $edit->harga : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Berat: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="number" name="berat" id="berat" class="form-control m-input" placeholder="Berat Merchandise" required="required" value="<?php echo isset($edit->berat) ? $edit->berat : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Satuan: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php $berat_satuan = isset($edit->berat_satuan) ? $edit->berat_satuan : ''; ?>
                                    <?php echo form_dropdown('satuan', $satuan, $berat_satuan, 'id="satuan" class="form-control"'); ?>
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Kategori: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php $id_category = isset($edit->id_category) ? $edit->id_category : ''; ?>
                                    <?php echo form_dropdown('category', $category, $id_category, 'id="category" class="form-control"'); ?>
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Deskripsi: <span class="m--font-danger">*</span>
                                    </label>
                                    <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5" required="required"><?php echo isset($edit->deskripsi) ? $edit->deskripsi : ''; ?></textarea>
                                </div>
                            </div>
                            
                            <div id="attribute-result"></div>

                            <div id="gallery-result"> </div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="">
                                        Sarankan Kepada:
                                    </label>
                                    <select name="suggest[]" id="suggest" class="form-control" multiple="multiple"></select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-12 col-sm-12">
                                    Upload Foto
                                </label>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="clsbox-1" runat="server"  >
                                        <div class="m-dropzone dropzone dz-clickable" id="mydropzone">
                                            <div class="m-dropzone__msg dz-message needsclick">
                                                <h3 class="m-dropzone__msg-title">
                                                    Drop File ke sini
                                                </h3>
                                                <span class="m-dropzone__msg-desc">
                                                    Masukkan file anda 
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
