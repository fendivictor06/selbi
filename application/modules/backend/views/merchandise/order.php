<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        
                        <!--end: Search Form -->
                        <table id="tb-data" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pembeli</th>
                                    <th>Merchandise</th>
                                    <th>Tanggal</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Total</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Form Input Desain
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data">
                <div class="modal-body">
                    <div id="results"> </div>
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label class="form-control-label">
                            Deskripsi:
                        </label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5"></textarea>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-12 col-sm-12">
                            Upload Foto
                        </label>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="clsbox-1" runat="server"  >
                                <div class="m-dropzone dropzone dz-clickable" id="mydropzone">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Drop File ke sini
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
                                            Masukkan file anda 
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->


