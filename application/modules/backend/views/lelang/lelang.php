<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1"> </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <!-- <a href="#" id="add_new" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                        <span>
                                            <i class="la la-plus-circle"></i>
                                            <span>
                                                Tambah Data
                                            </span>
                                        </span>
                                    </a> -->
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
                        <table id="tb-data" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th>Bid Awal</th>
                                    <th>Waktu Input</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Detail Barang
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data">
                <div class="modal-body">
                    <h5 id="nama-barang"></h5>
                    <p><b>Penjual</b> : <span id="penjual-barang"></span></p>
                    <p><b>Kategori</b> : <span id="category-barang"></span></p>
                    <p><b>Brand</b> : <span id="brand-barang"></span></p>
                    <p><b>Jenis</b> : <span id="jenis-barang"></span></p>
                    <p><b>Kondisi</b> : <span id="kondisi-barang"></span></p>
                    <p><b>Berat</b> : <span id="berat-barang"></span></p>
                    <p><b>Deskripsi</b> : </p>
                    <div id="deskripsi-barang" style="margin-bottom: 10px;"></div>
                    <p><b>Status</b> : <span id="status-barang"></span> </p>
                    <p><b>Waktu Order</b> : <span id="waktu-barang"></span></p>
                    <p><b>Images : </b></p>
                    <div id="images-barang"></div>
                    <div class="clearfix"> </div>
                    <input type="hidden" name="id" id="id">
                    <div class="m-form__group form-group" style="margin-top: 20px;">
                        <label for="">
                            Persetujuan Lelang: <span class="m--font-danger">*</span>
                        </label>
                        <div class="m-radio-inline">
                            <label class="m-radio">
                                <input type="radio" name="approve" value="1">
                                Setuju
                                <span></span>
                            </label>
                            <label class="m-radio">
                                <input type="radio" name="approve" value="9">
                                Tolak
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="form-control-label">
                            Keterangan: <span class="m--font-danger">*</span>
                        </label>
                        <textarea class="form-control" name="keterangan" id="keterangan" rows="5" required="required" placeholder="Masukkan Keterangan"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
