<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table id="tb-data" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>Sosmed</th>
                                    <th>Foto</th>
                                    <th>Waktu <br> Daftar</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Form Pendaftaran Public Figure
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label class="form-control-label">
                            Nama Profile: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="nama" name="nama" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Foto: 
                        </label>
                        <input type="file" name="foto" id="foto" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            No Telepon: <span class="m--font-danger">*</span>
                        </label>
                        <input type="number" class="form-control" id="telepon" name="telepon" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            No KTP: <span class="m--font-danger">*</span>
                        </label>
                        <input type="number" class="form-control" id="ktp" name="ktp" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Upload KTP: <span class="m--font-danger">*</span>
                        </label>
                        <input type="file" name="file_ktp" id="file_ktp" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Provinsi: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('provinsi', $provinsi, '', 'id="provinsi" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Kota: <span class="m--font-danger">*</span>
                        </label>
                        <select name="kota" id="kota" class="form-control m-select2" required="required" style="width:100%;"> </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Alamat: <span class="m--font-danger">*</span>
                        </label>
                        <textarea class="form-control" name="alamat" id="alamat" rows="5" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Deskripsi:
                        </label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Pekerjaan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('pekerjaan', $pekerjaan, '', 'id="pekerjaan" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Lampiran: 
                        </label>
                        <input type="file" name="lampiran" id="lampiran" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
