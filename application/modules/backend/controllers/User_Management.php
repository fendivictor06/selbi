<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_Management extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Admin_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        $arr_level = array(
            '1' => 'Admin'
        );

        $data = array(
            'level' => $arr_level
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/user/admin.js'
        );

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/user/admin', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Admin_Model->json_user($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_user()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $level = $this->input->post('level');
            $profile_name = $this->input->post('profile_name');
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            // check mandatory field
            if ($level == '' || $username == '' || $password == '') {
                $message = '';
                $status = 0;

                $message .= ($level == '') ? 'Field Level wajib diisi. <br>' : '';
                $message .= ($username == '') ? 'Field Username wajib diisi. <br>' : '';
                $message .= ($password == '') ? 'Field Password wajib diisi. <br>' : '';
            } else {
                $message = '';
                $check_field = true;
                if ($level == 1) {
                    if ($profile_name == '') {
                        $check_field = false;
                        $message .= 'Field Profile Name wajib diisi. <br>';
                    }
                }

                $check_exists = true;
                $user_exists = [];
                if ($level == 1) {
                    $user_exists = $this->Admin_Model->user_exist($username, $level);
                }

                if ($id == '') {
                    if ($user_exists == true) {
                        $check_exists = false;
                    } else {
                        $check_exists = true;
                    }
                }

                if ($check_field == true) {
                    if ($check_exists == true) {
                        $simpan = $this->Admin_Model->add_user($level, '', '', $username, $password, $profile_name, $id);

                        if ($simpan > 0) {
                            $status = 1;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 0;
                            $message = 'Gagal menyimpan data';
                        }
                    } else {
                        $status = 0;
                        $message = 'Username sudah terdaftar';
                    }
                } else {
                    $status = 0;
                    $message = $message;
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function user_id()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('tb_admin', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->delete_data('tb_admin', ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }
}

/* End of file User_Management.php */
/* Location: ./application/controllers/User_Management.php */
