<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Complaint extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Complaint_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/complaint/complaint.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/complaint/complaint', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_complaint()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $json = $this->Complaint_Model->json_complaint($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
    }
}

/* End of file Complaint.php */
/* Location: ./application/controllers/Complaint.php */
