<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Slider extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css') 
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/slider/slider.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/slider/slider', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_slider()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_slider', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);

                    $tipe = ($val->flag == 1) ? 'Mobile Apps' : 'Web';
                    
                    $path = path_slider();
                    $image = '';
                    if ($val->image != '') {
                        $image = '<img src="'.$path.$val->image.'" width="48" class="image-modal" style="cursor:pointer;">';
                    }

                    $arr[$row] = [
                        $no++,
                        $image,
                        $tipe,
                        btn_group([$btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function delete_slider()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('ms_slider', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_slider()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $type = $this->input->post('tipe');
            $path = './assets/uploads/slider/';

            $config = array(
                'upload_path' => $path,
                'allowed_types' => 'jpg|png|gif',
                'file_name' => date('YmdHis')
            );

            $this->load->library('upload', $config);
            $this->upload->do_upload('image');
            $data = $this->upload->data();
            $error = $this->upload->display_errors();
            $filename = isset($data['file_name']) ? $data['file_name'] : '';

            $simpan = 0;
            if ($id == '') {
                if ($error == '' && file_exists($path.$filename) == true) {
                    $simpan = 1;
                } else {
                    $simpan = 0;
                } 
            } else {
                $simpan = 1;
            }

            if ($simpan == 1) {
                $username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

                $data = array(
                    'flag' => $type,
                    $time => $now,
                    $user => $username
                );

                if ($filename != '' && file_exists($path.$filename) == true) {
                    $data['image'] = $filename;
                }

                $condition = ($id != '') ? ['id' => $id] : [];

                $proses = $this->customdb->process_data('ms_slider', $data, $condition);
                if ($proses > 0) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
            } else {
                $status = 0;
                $message = 'Gagal mengupload file';
            }

            $result = array(
                'error' => $this->upload->display_errors(),
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Slider.php */
/* Location: ./application/controllers/Slider.php */
