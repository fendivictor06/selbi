<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hot_News extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('backend/Hot_News_Model');
	}

	function index()
	{
		$this->Auth_Model->is_login();
		$header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/hot_news/hot_news.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/hot_news/hot_news', $data);
        $this->load->view('backend/template/footer', $footer);
	}

	function form_news($id = '')
	{
		$this->Auth_Model->is_login();
		$header = array(
			'title' => 'Form Hot News',
            'styles' => datatable('css')
        );

        $level = level_user();

        $extends_js = '
        	<script> 
				var id = "'.$id.'";
        	</script>';
        
        $footer = array(
            'script' => datatable('js').$extends_js,
            'app' => 'backend/hot_news/form_news.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/hot_news/form_news', $data);
        $this->load->view('backend/template/footer', $footer);
	}

	function data_news()
	{
		$this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $json = $this->Hot_News_Model->json_news($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
	}

	function add_news()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');
			$judul = $this->input->post('judul');
			$teks = $this->input->post('teks');

			$this->form_validation->set_rules('judul', 'Judul', 'required', ['required' => 'Masukkan %s']);
			$this->form_validation->set_rules('teks', 'Deskripsi', 'required', ['required' => 'Masukkan %s']);

			if ($this->form_validation->run() == false) {
				$status = 0;
				$message = validation_errors();
			} else {
				$path = './assets/uploads/news/';
	            $config = array(
	                'upload_path' => $path,
	                'allowed_types' => 'jpg|png|gif',
	                'file_name' => date('YmdHis')
	            );

	            $this->load->library('upload', $config);
	            $this->upload->do_upload('image');
	            $data = $this->upload->data();
	            $error = $this->upload->display_errors();
	            $filename = isset($data['file_name']) ? $data['file_name'] : '';

	            $username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

	            $data = array(
	            	'judul' => $judul,
	            	'teks' => $teks,
	            	$time => $now,
                    $user => $username
	            );

	            if ($filename != '' && file_exists($path.$filename) == true) {
	                $data['image'] = $filename;
	            }

	            $condition = ($id != '') ? ['id' => $id] : [];
                $proses = $this->customdb->process_data('tb_news', $data, $condition);

                if ($proses > 0) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
			}

			$result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
		} else {
			show_404();
		}
	}

	function id_news()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');

			$data = $this->customdb->view_by_id('tb_news', ['id' => $id], 'row');
			echo json_encode($data);
		} else {
			show_404();
		}
	}

	function delete_news()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');

			$delete = $this->customdb->delete_data('tb_news', ['id' => $id]);
			if ($delete > 0) {
				$status = 1;
				$message = 'Data berhasil dihapus';
			} else {
				$status = 0;
				$message = 'Gagal menghapus data';
			}

			$result = array(
				'status' => $status,
				'message' => $message
			);

			echo json_encode($result);
		} else {
			show_404();
		}
	}

	function view_news($id = '')
	{
		$this->Auth_Model->is_login();
		$header = array(
			'title' => 'View Hot News',
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => ''
        );

        $news = $this->customdb->view_by_id('tb_news', ['id' => $id], 'row');

        $data = array(
        	'news' => $news
        );

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/hot_news/view_news', $data);
        $this->load->view('backend/template/footer', $footer);
	}
}

/* End of file Hot_News.php */
/* Location: ./application/controllers/Hot_News.php */ ?>