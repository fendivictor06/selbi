<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lelang extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Lelang_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/lelang/lelang.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/lelang/lelang', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_lelang()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $json = $this->Lelang_Model->json_lelang($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function detail_lelang()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->Lelang_Model->detail_lelang($id);
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function approve()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $approve = $this->input->post('approve');
            $keterangan = $this->input->post('keterangan');

            if ($approve == '' || $keterangan == '') {
                $status = 0;
                $message = '';

                if ($approve == '') {
                    $message .= 'Pilih Persetujuan Lelang <br>';
                }

                if ($keterangan == '') {
                    $message .= 'Masukkan Keterangan <br>';
                }
            } else {
                $data = array(
                    'flag_approved' => $approve,
                    'keterangan' => $keterangan
                );

                $simpan = $this->customdb->process_data('tb_lelang', $data, ['id' => $id]);
                if ($simpan > 0) {
                    $status = 1;
                    $message = 'Data berhasil diupdate';
                } else {
                    $status = 0;
                    $message = 'Gagal mengupdate data';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Lelang.php */
/* Location: ./application/controllers/Lelang.php */
