<?php
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Barang extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Barang_Model');
    }

    function index()
    {
        /*not use*/
    }

    function preloved()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/barang/preloved.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/barang/data_preloved', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function merchandise()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/barang/merchandise.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/barang/data_merchandise', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    /* Function Preloved */
    function form_preloved()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'barang/'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/barang/form_preloved', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function ajax_data_preloved()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $condition = '';
            $arr = array();

            // example push condition
            $arr[] = " a.id_jenis=1 "; // jenis preloved

            // create push condition new here


            // build condition
            if (!empty($arr)) {
                for ($i = 0; $i < count($arr); $i++) {
                    $condition .= $arr[$i];
                    if (end($arr) != $arr[$i]) {
                        $condition .= ' AND ';
                    } else {
                        $condition .= ' ';
                    }
                }
            }

            $json = $this->Barang_Model->json_barang($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $condition);
            echo json_encode($json);
        } else {
            show_404();
        }
    }
    /* End Function Preloved */

    /* Function Merchandise */
    function ajax_data_merchandise()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $condition = '';
            $arr = array();

            // example push condition
            $arr[] = " a.id_jenis=2 "; // jenis merchandise

            // create push condition new here


            // build condition
            if (!empty($arr)) {
                for ($i = 0; $i < count($arr); $i++) {
                    $condition .= $arr[$i];
                    if (end($arr) != $arr[$i]) {
                        $condition .= ' AND ';
                    } else {
                        $condition .= ' ';
                    }
                }
            }

            $json = $this->Barang_Model->json_barang($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $condition);
            echo json_encode($json);
        } else {
            show_404();
        }
    }
    /* End Function Merchandise */

    function delete_barang()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $update = $this->customdb->process_data('ms_barang', ['status' => 0], ['id' => $id]);
            if ($update > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}
/* End of file Barang.php */
/* Create by - Akbar */
