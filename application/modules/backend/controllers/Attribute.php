<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attribute extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('backend/Attribute_Model');
	}

	function index()
	{
		$this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/attribute.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/attribute', $data);
        $this->load->view('backend/template/footer', $footer);
	}

	function data_attribute()
	{
		$this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_attribute', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);

                    $arr[$row] = [
                        $no++,
                        $val->attribute,
                        btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
	}

	function add_attribute()
	{
		$this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
        	$id = $this->input->post('id');
        	$attribute = $this->input->post('attribute');

        	$this->form_validation->set_rules('attribute', 'Attribute', 'required', ['required' => 'Masukkan %s']);

        	if ($this->form_validation->run() == false) {
        		$status = 0;
        		$message = validation_errors();
        	} else {
        		$username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

                $data = array(
                	'attribute' => $attribute,
                	$user => $username,
                	$time => $now
                );

                $condition = ($id != '') ? ['id' => $id] : [];
                $proses = $this->customdb->process_data('ms_attribute', $data, $condition);
                if ($proses > 0) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
        	}

        	$result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
	}

	function id_attribute()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $arr = [];
            $brand = $this->customdb->view_by_id('ms_attribute', ['id' => $id], 'row');
            if ($brand) {
                $arr = $brand;
            }

            echo json_encode($arr);
        }
    }

    function delete_attribute()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('ms_attribute', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function item()
    {
    	$this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/item.js'
        );


        $ms_attribute = $this->customdb->view_by_id('ms_attribute', ['status' => 1], 'result');
        $arr_attribute = [];
        if ($ms_attribute) {
        	foreach ($ms_attribute as $row) {
        		$arr_attribute[$row->id] = $row->attribute; 
        	}
        }

        $data = array(
        	'attribute' => $arr_attribute
        );

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/item', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_item()
    {
    	$this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->Attribute_Model->view_item();
            $path = path_attribute();

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);

                    $image = ($val->thumbnail) ? '<img src="'.$path.$val->thumbnail.'" width="48px" class="image-modal" style="cursor:pointer" />' : '';

                    $arr[$row] = [
                        $no++,
                        $val->attribute,
                        $val->value,
                        $val->kode_warna,
                        $image,
                        btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function add_item()
    {
    	$this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
        	$id = $this->input->post('id');
        	$attribute = $this->input->post('attribute');
        	$item = $this->input->post('item');
			$kode_warna = $this->input->post('kode_warna');

        	$this->form_validation->set_rules('attribute', 'Attribute', 'required', ['required' => 'Masukkan %s']);
        	$this->form_validation->set_rules('item', 'Item', 'required', ['required' => 'Masukkan %s']);

        	if ($this->form_validation->run() == false) {
        		$status = 0;
        		$message = validation_errors();
        	} else {
        		$path = './assets/uploads/item/';
	            $config = array(
	                'upload_path' => $path,
	                'allowed_types' => 'jpg|png|gif|jpeg',
	                'file_name' => date('YmdHis')
	            );

	            $this->load->library('upload', $config);
	            $this->upload->do_upload('image');
	            $data = $this->upload->data();
	            $error = $this->upload->display_errors();
	            $filename = isset($data['file_name']) ? $data['file_name'] : '';

        		$username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

                $data = array(
                	'id_attribute' => $attribute,
                	'value' => $item,
                	'kode_warna' => $kode_warna,
                	$user => $username,
                	$time => $now
                );

                if ($filename != '' && file_exists($path.$filename) == true) {
                    $data['thumbnail'] = $filename;
                }

                $condition = ($id != '') ? ['id' => $id] : [];
                $proses = $this->customdb->process_data('ms_item_attribute', $data, $condition);
                if ($proses > 0) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
        	}

        	$result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function id_item()
    {
    	$this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $item = $this->customdb->view_by_id('ms_item_attribute', ['id' => $id], 'row');

            echo json_encode($item);
        }
    }

    function delete_item()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('ms_item_attribute', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Attribute.php */
/* Location: ./application/controllers/Attribute.php */ ?>