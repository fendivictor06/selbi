<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Gallery extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Gallery_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/feed/gallery.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/feed/gallery', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_gallery()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $start = $this->input->get('start');
            $count = $this->input->get('count');
            $keyword = $this->input->get('keyword');
            
            $data = $this->Gallery_Model->view_gallery($start, $count, $keyword);
            $result = '';
            $path = path_gallery();
            if ($data) {
                foreach ($data as $row) {
                    $btn_delete = btn_delete($row->id);
                    $img = '';
                    if ($row->image != '') {
                        $img = '<img class="card-img-top hover-image details" data-refid="'.$row->id.'" src="'.$path.$row->image.'" alt="'.$row->judul.'">';
                    }

                    $result .= '<div class="card">
								  	'.$img.'
								  	<div class="card-body">
								    	<h5 class="card-title details" data-refid="'.$row->id.'" style="cursor:pointer">'.$row->judul.'</h5>
								    	<p class="card-text">'.$row->tgl.' - '.$row->profile_name.'</p>
                                        '.$btn_delete.'
								  	</div>
								</div>';
                }
            }

            if (empty($data) && $start == 0) {
                $result = 'Tidak ada data ditemukan';
            }

            echo $result;
        }
    }

    function gallery_id($id = '')
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $feed = $this->Gallery_Model->view_feed($id);

            $result = '';
            if ($feed) {
                $judul = isset($feed->judul) ? $feed->judul : '';
                $tgl = isset($feed->tgl) ? $feed->tgl : '';
                $nama = isset($feed->profile_name) ? $feed->profile_name : '';

                $result .= '<h3>'.$judul.'</h3>';
                $result .= '<p>'.$nama.' - '.$tgl.'</p>';
                $result .= '<div class="row">';

                $gallery = $this->customdb->view_by_id('tb_feed_image', ['id_feed' => $id], 'result');
                if ($gallery) {
                    foreach ($gallery as $row) {
                        $path = path_gallery();
                        $result .= '<div class="col-md-4 col-sm-6 col-xs-12 text-center">';
                        $result .= '<img src="'.$path.$row->image.'" class="img-responsive" width="100%" style="margin-bottom: 20px;">';
                        $result .= btn_custom($row->id, 'details-delete', 'javascript:;', 'btn-danger', 'fa fa-trash-o', 'Delete Image');
                        $result .= '</div>';
                    }
                }

                $result .= '</div>';
            }

            echo $result;
        }
    }

    function delete_gallery()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            
            $delete = $this->Gallery_Model->hapus_gallery($id);
            if ($delete == true) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function delete_image()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $path_gallery = './assets/uploads/feed/gallery/';

            $image = $this->customdb->view_by_id('tb_feed_image', ['id' => $id], 'row');
            $picture = isset($image->image) ? $image->image : '';
            $id_feed = isset($image->id_feed) ? $image->id_feed : '';
            
            $delete = $this->customdb->delete_data('tb_feed_image', ['id' => $id]);
            if ($delete > 0) {
                if ($picture != '') {
                    unlink($path_gallery.$picture);
                }

                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message,
                'id_feed' => $id_feed
            );

            echo json_encode($result);
        }
    }
}

/* End of file Gallery.php */
/* Location: ./application/controllers/Gallery.php */
