<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Greeting extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('backend/Greeting_Model');
	}

	function index()
	{
		$this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/barang/greeting.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/barang/data_greeting', $data);
        $this->load->view('backend/template/footer', $footer);
	}

	function ajax_data_greeting()
	{
		$this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Greeting_Model->json_greeting($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
	}

	function delete_greeting()
	{
		$this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $update = $this->customdb->delete_data('tb_greeting', ['id' => $id]);
            if ($update > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
	}
}

/* End of file Greeting.php */
/* Location: ./application/controllers/Greeting.php */ ?>