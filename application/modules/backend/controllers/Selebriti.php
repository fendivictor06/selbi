<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Selebriti extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/User_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/user/selebriti.js'
        );

        $ms_provinsi = $this->customdb->view_by_id('ms_provinsi', ['status' => 1], 'result');
        $provinsi = [];
        if ($ms_provinsi) {
            foreach ($ms_provinsi as $row) {
                $provinsi[$row->id] = $row->provinsi;
            }
        }

        $ms_pekerjaan = $this->customdb->view_by_id('ms_pekerjaan', ['status' => 1], 'result');
        $pekerjaan = [];
        if ($ms_pekerjaan) {
            foreach ($ms_pekerjaan as $row) {
                $pekerjaan[$row->id] = $row->pekerjaan;
            }
        }

        $data = array(
            'provinsi' => $provinsi,
            'pekerjaan' => $pekerjaan
        );

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/user/selebriti', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function ajax_data_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $json = $this->User_Model->json_selebriti($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
    }
}

/* End of file Selebriti.php */
/* Location: ./application/controllers/Selebriti.php */
