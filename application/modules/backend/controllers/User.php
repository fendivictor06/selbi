<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/User_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/user/user.js'
        );

        $ms_provinsi = $this->customdb->view_by_id('ms_provinsi', ['status' => 1], 'result');
        $provinsi = [];
        if ($ms_provinsi) {
            foreach ($ms_provinsi as $row) {
                $provinsi[$row->id] = $row->provinsi;
            }
        }

        $ms_pekerjaan = $this->customdb->view_by_id('ms_pekerjaan', ['status' => 1], 'result');
        $pekerjaan = [];
        if ($ms_pekerjaan) {
            foreach ($ms_pekerjaan as $row) {
                $pekerjaan[$row->id] = $row->pekerjaan;
            }
        }

        $data = array(
            'provinsi' => $provinsi,
            'pekerjaan' => $pekerjaan
        );

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/user/user', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function ajax_data_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $json = $this->User_Model->json_user($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function ban_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('refid');

            $user = $this->customdb->view_by_id('tb_user', ['id' => $id], 'row');
            $status = isset($user->status) ? $user->status : 0;

            if ($status == 0) {
                $data = ['status' => 1];
                $teks = 'diaktifkan';
            } else {
                $data = ['status' => 0];
                $teks = 'dinonaktifkan';
            }

            $update = $this->customdb->process_data('tb_user', $data, ['id' => $id]);
            if ($update > 0) {
                $status = 1;
                $message = 'User berhasil di'.$teks;
            } else {
                $status = 0;
                $message = 'User gagal di'.$teks;
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_selebriti()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $nama = $this->input->post('nama');
            $foto = $_FILES['foto'];
            $telepon = $this->input->post('telepon');
            $ktp = $this->input->post('ktp');
            $file_ktp = $_FILES['file_ktp'];
            $provinsi = $this->input->post('provinsi');
            $kota = $this->input->post('kota');
            $alamat = $this->input->post('alamat');
            $deskripsi = $this->input->post('deskripsi');
            $pekerjaan = $this->input->post('pekerjaan');
            $lampiran = $_FILES['lampiran'];
            $update = $this->input->post('update');

            $path_foto = './assets/uploads/profile/foto/';
            $path_ktp = './assets/uploads/profile/ktp/';
            $path_lampiran = './assets/uploads/profile/lampiran/';

            // check mandatory field
            if ($nama == '' || $ktp == '' || $telepon == '' || $pekerjaan == '' || $id == '') {
                $message = '';
                $status = 0;

                // jika field provinsi kosong
                $message .= ($nama == '') ? 'Field Nama wajib diisi. <br>' : '';
                $message .= ($ktp == '') ? 'Field KTP wajib diisi. <br>' : '';
                $message .= ($telepon == '') ? 'Field No Telepon wajib diisi. <br>' : '';
                $message .= ($pekerjaan == '') ? 'Field Pekerjaan wajib diisi. <br>' : '';
                $message .= ($id == '') ? 'User tidak ditemukan. <br>' : '';
            } else {
                // buat data utk disimpan
                $data = array(
                    'profile_name' => $nama,
                    'no_telp' => $telepon,
                    'no_ktp' => $ktp,
                    'id_kota' => $kota,
                    'id_provinsi' => $provinsi,
                    'alamat' => $alamat,
                    'deskripsi' => $deskripsi,
                    'id_pekerjaan' => $pekerjaan,
                    'penjual' => 1,
                    'update_at' => now()
                );

                $status_foto = false;
                $foto_ext = isset($foto['name']) ? $foto['name'] : '';
                $foto_ext = file_ext($foto_ext);

                $foto_type = isset($foto['type']) ? $foto['type'] : '';
                $foto_type = file_type($foto_type);

                if ($foto_type == 'image') {
                    $file_foto = date('YmdHis').'.'.$foto_ext;
                    move_uploaded_file($foto['tmp_name'], $path_foto.$file_foto);

                    if (file_exists($path_foto.$file_foto)) {
                        $status_foto = true;
                        $data['foto'] = base_url('assets/uploads/profile/foto/'.$file_foto);
                    }
                }


                $status_ktp = false;
                $ktp_ext = isset($file_ktp['name']) ? $file_ktp['name'] : '';
                $ktp_ext = file_ext($ktp_ext);

                $ktp_type = isset($file_ktp['type']) ? $file_ktp['type'] : '';
                $ktp_type = file_type($ktp_type);

                if ($ktp_type == 'image') {
                    $filename_ktp = date('YmdHis').'.'.$ktp_ext;
                    move_uploaded_file($file_ktp['tmp_name'], $path_ktp.$filename_ktp);

                    if (file_exists($path_ktp.$filename_ktp)) {
                        $status_ktp = true;
                        $data['upload_ktp'] = base_url('assets/uploads/profile/ktp/'.$filename_ktp);
                    }
                }

                $status_lampiran = false;
                $lampiran_ext = isset($lampiran['name']) ? $lampiran['name'] : '';
                $lampiran_ext = file_ext($lampiran_ext);

                $lampiran_type = isset($lampiran['type']) ? $lampiran['type'] : '';
                $lampiran_type = file_type($lampiran_type);

                if ($lampiran_type == 'image') {
                    $file_lampiran = date('YmdHis').'.'.$lampiran_ext;
                    move_uploaded_file($lampiran['tmp_name'], $path_lampiran.$file_lampiran);

                    if (file_exists($path_lampiran.$file_lampiran)) {
                        $status_lampiran = true;
                        $data['lampiran'] = base_url('assets/uploads/profile/lampiran/'.$file_lampiran);
                    }
                }

                if (($status_ktp == true && $update == 0) || $update == 1) {
                    // if id exists or not
                    $condition = ['id' => $id];
                    // simpan action
                    $simpan = $this->customdb->process_data('tb_user', $data, $condition);
                    // message ketika insert
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Data berhasil diupdate';
                    } else {
                        $status = 0;
                        $message = 'Gagal mengupdate data';
                    }
                } else {
                    $status = 0;
                    $message = '';

                    if ($status_ktp == false) {
                        $message .= 'Gagal mengupload ktp. <br>';
                    }
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('tb_user', ['id' => $id], 'row');
            echo json_encode($data);
        }
    }
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
