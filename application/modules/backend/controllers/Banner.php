<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Banner extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css') 
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/banner/banner.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/banner/banner', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_banner()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_banner', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);

                    $tipe = $val->kategori;
                    
                    $path = path_banner();
                    $image = '';
                    if ($val->gambar != '') {
                        $image = '<img src="'.$path.$val->gambar.'" width="48" class="image-modal" style="cursor:pointer;">';
                    }

                    $arr[$row] = [
                        $no++,
                        $image,
                        $tipe,
                        btn_group([$btn_edit])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function delete_slider()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('ms_slider', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
    function id_banner()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $banner = $this->customdb->view_by_id('`ms_banner`', ['id' => $id], 'row');

            echo json_encode($banner);
        }
    }

	public function update_banner_()
	{	
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
			$data = array_merge($_POST);
			// unset($data['simpan']);
			if (isset($_FILES['gambar'])) {

				$fileName = time().$_FILES['gambar']['name'];
		        $config['upload_path'] = './assets/uploads/banner';
		        $config['file_name'] = $fileName;
		        $config['allowed_types'] = 'gif|jpg|png';
		        $config['max_size'] = 10000;
		         
		        $this->load->library('upload');
		        $this->upload->initialize($config);

	            $this->load->library('upload', $config);

	            if ( !$this->upload->do_upload('gambar'))
	            {
	                    print_r($this->upload->display_errors());
	            }
	            else
	            {
	                    $media = $this->upload->data();
	                    $data['gambar'] = $media['file_name'];
	            }
	        }

            $condition = $data['id'];

            $proses = $this->customdb->process_data('ms_banner', $data, $condition);
			if ($proses > 0) {
                $status = 1;
                $message = 'Data berhasil disimpan';
            } else {
                $status = 0;
                $message = 'Gagal menyimpan data';
            }
            $result = array(
                'error' => $this->upload->display_errors(),
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
		}
		
	}

    function update_banner()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $type = $this->input->post('type');
            $path = './assets/uploads/banner/';

            $config = array(
                'upload_path' => $path,
                'allowed_types' => 'jpg|png|gif',
                'file_name' => date('YmdHis')
            );

            $this->load->library('upload', $config);
            $this->upload->do_upload('gambar');
            $data = $this->upload->data();
            $error = $this->upload->display_errors();
            $filename = isset($data['file_name']) ? $data['file_name'] : '';

            $simpan = 0;
            if ($id == '') {
                if ($error == '' && file_exists($path.$filename) == true) {
                    $simpan = 1;
                } else {
                    $simpan = 0;
                }
            } else {
                $simpan = 1;
            }

            if ($simpan == 1) {
                $username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

                $data = array(
                    'flag' => $type,
                    $time => $now,
                    $user => $username
                );

                if ($filename != '' && file_exists($path.$filename) == true) {
                    $data['gambar'] = $filename;
                }

                $condition = ($id != '') ? ['id' => $id] : [];

                $proses = $this->customdb->process_data('ms_banner', $data, $condition);
                if ($proses > 0) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
            } else {
                $status = 0;
                $message = 'Gagal mengupload file';
            }

            $result = array(
                'error' => $this->upload->display_errors(),
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Slider.php */
/* Location: ./application/controllers/Slider.php */
