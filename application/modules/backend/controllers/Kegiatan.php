<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Kegiatan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Kegiatan_Model');
        $this->load->model('backend/Gallery_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/feed/kegiatan.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/feed/kegiatan', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_kegiatan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $json = $this->Kegiatan_Model->json_kegiatan($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function delete_kegiatan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $delete = $this->Gallery_Model->hapus_gallery($id);
            if ($delete) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Kegiatan.php */
/* Location: ./application/controllers/Kegiatan.php */
