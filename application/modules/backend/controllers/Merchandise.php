<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Merchandise extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Merchandise_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/merchandise/merchandise.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/merchandise/merchandise', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_merchandise()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $json = $this->Merchandise_Model->json_merchandise($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function form_merchandise($id = '')
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css'),
            'title' => 'Form Merchandise'
        );

        $level = level_user();

        # suggest
        $suggestion = $this->Merchandise_Model->merchandise_suggestion($id);
        $suggestion = json_encode($suggestion);
        
        $footer = array(
            'script' => datatable('js')
            .'<script>
                var public_figure = '.$suggestion.';
              </script>',
            'app' => 'backend/merchandise/form_merchandise.js'
        );

        $arr_category = [];
        $ms_category = $this->customdb->view_by_id('ms_category', ['status' => 1], 'result');
        if ($ms_category) {
            foreach ($ms_category as $cat) {
                $arr_category[$cat->id] = $cat->category;
            }
        }

        $edit = [];
        $gallery = [];
        if ($id != '') {
            $edit = $this->customdb->view_by_id('ms_merchandise', ['id' => $id], 'row');
        }

        $satuan = array(
            'gram' => 'gram',
            'kilogram' => 'kilogram'
        );

        $data = array(
            'category' => $arr_category,
            'satuan' => $satuan,
            'edit' => $edit,
            'gallery' => $gallery
        );

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/merchandise/form_merchandise', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function load_attribute($id_merchandise = '')
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {

            $response = [];
            $ms_attribute = $this->customdb->view_by_id('ms_attribute', ['status' => 1], 'result');
            if ($ms_attribute) {
                foreach ($ms_attribute as $row => $att) {
                    $response[$row] = array(
                        'id' => $att->id,
                        'attribute' => $att->attribute
                    );

                    $ms_item = $this->customdb->view_by_id('ms_item_attribute', ['status' => 1, 'id_attribute' => $att->id], 'result');
                    if ($ms_item) {
                        foreach ($ms_item as $item) {
                            $detail_merch = $this->customdb->view_by_id('ms_merchandise_detail', ['id_merchandise' => $id_merchandise, 'id_item' => $item->id], 'row');

                            $selected = 0;
                            if ($detail_merch) {
                                $selected = 1;
                            }

                            $response[$row]['item'][] = array(
                                'value' => $item->value,
                                'id' => $item->id,
                                'selected' => $selected
                            );
                        }
                    }
                }
            }

            echo json_encode($response);
        }
    }

    function load_gallery($id = '')
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $result = '';
            if ($id != '') {
                $gallery = $this->customdb->view_by_id('tb_image_merchandise', ['id_merchandise' => $id], 'result');
                if ($gallery) {
                    $result .= '<div class="form-group m-form__group row">';
                    foreach ($gallery as $row) {
                        $result .= '<div class="col-lg-2 col-md-2 col-sm-2">';
                        $result .= '<img src="'.$row->path.$row->image.'" class="img-thumbnail" >';
                        $result .= '<p class="text-center">';
                        $result .= '<a href="javascript:;" data-img="'.$row->image.'" class="delete-img" title="Hapus gambar">Hapus</a>';
                        $result .= '</p>';
                        $result .= '</div>';
                    }
                    $result .= '</div>';
                }
            }

            echo $result;
        }
    }

    function store_image()
    {
        $this->Auth_Model->is_login();
        $user_id = username();
        $unik = $this->input->get('unik');
        $path = './assets/uploads/merchandise/temp/';

        $config = array(
            'upload_path' => $path,
            'allowed_types' => '*',
            'file_name' => time()
        );

        $this->load->library('upload', $config);
        $this->upload->do_upload('images');
        $data = $this->upload->data();
        $error = $this->upload->display_errors();
        $filename = isset($data['file_name']) ? $data['file_name'] : '';

        $simpan = 0;
        if ($error == '' && file_exists($path.$filename) == true) {
            $data = array(
                'image' => $filename,
                'user_insert' => $user_id,
                'unik' => $unik
            );

            $simpan = $this->customdb->process_data('temp_image_merchandise', $data);
        }

        echo json_encode(['status' => $simpan, 'filename' => $filename]);
    }

    function delete_image()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $unik = $this->input->post('unik');
            $name = $this->input->post('name');
            $path = './assets/uploads/merchandise/temp/';
            $user_id = username();

            $delete = 0;
            if (unlink($path.$name) == true) {
                $delete = $this->customdb->delete_data('temp_image_merchandise', ['user_insert' => $user_id, 'unik' => $unik, 'image' => $name]);
                $delete = 1;
            }

            echo json_encode(['status' => $delete]);
        }
    }

    function delete_image_merchandise()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $image = $this->input->post('image');
            $path = './assets/uploads/merchandise/';

            if (unlink($path.$image) == true) {
                $delete = $this->customdb->delete_data('tb_image_merchandise', ['id_merchandise' => $id, 'image' => $image]);
                if ($delete > 0) {
                    $status = 1;
                    $message = 'Gambar berhasil dihapus';
                } else {
                    $status = 0;
                    $message = 'Gagal menghapus gambar';
                }
            } else {
                $status = 0;
                $message = 'Gagal menghapus image';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_merchandise()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $unik = $this->input->post('unik');
            $nama = $this->input->post('nama');
            $harga = $this->input->post('harga');
            $berat = $this->input->post('berat');
            $satuan = $this->input->post('satuan');
            $kategori = $this->input->post('category');
            $deskripsi = $this->input->post('deskripsi');
            $suggest = $this->input->post('suggest');

            $user_id = username();

            if ($nama == '') {
                $status = 0;
                $message = '';

                if ($nama == '') {
                    $message .= 'Masukkan Nama Merchandise <br>';
                }
            } else {
                # attribute
                $ms_attribute = $this->customdb->view_by_id('ms_attribute', ['status' => 1], 'result');
                $attribute = [];
                if ($ms_attribute) {
                    foreach ($ms_attribute as $row) {
                        $post_attribute = $this->input->post('attribute-'.$row->id);
                        if ($post_attribute != '') {
                            $attribute[] = array(
                                'id' => $row->id,
                                'value' => $post_attribute
                            ); 
                        }
                    }
                }

                $data = array(
                    'id_category' => $kategori,
                    'id_jenis' => 2,
                    'nama' => $nama,
                    'deskripsi' => $deskripsi,
                    'harga' => $harga,
                    'berat' => $berat,
                    'berat_satuan' => $satuan,
                    'user_insert' => $user_id
                );

                if ($id == '') {
                    $simpan = $this->Merchandise_Model->add_merchandise($data, $unik, $attribute, $suggest);
                } else {
                    $simpan = $this->Merchandise_Model->update_merchandise($data, $unik, $id, $attribute, $suggest);
                }

                if ($simpan == true) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function delete_merchandise()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $update = $this->customdb->process_data('ms_merchandise', ['status' => 0], ['id' => $id]);
            if ($update > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function order()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/merchandise/order.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/merchandise/order', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_order()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw   = $this->input->get('draw');
            $start  = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order  = $this->input->get('order');

            $json = $this->Merchandise_Model->json_order($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);
            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_penawaran()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $deskripsi = $this->input->post('deskripsi');
            $unik = $this->input->post('unik');

            if ($deskripsi == '') {
                $status = 0;
                $message = '';

                if ($deskripsi == '') {
                    $message .= 'Masukkan Deskripsi <br>';
                }
            } else {
                $simpan = $this->Merchandise_Model->add_penawaran($id, $deskripsi, $unik);

                if ($simpan == true) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function detail_penawaran()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->Merchandise_Model->detail_penawaran($id);

            echo json_encode($data);
        }
    }

    function public_figure()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $search = $this->input->post('search');
            $start = $this->input->post('start');
            $limit = $this->input->post('limit');

            $response = [];
            $data = $this->Merchandise_Model->select_figure($search, $start, $limit);
            $total = $this->Merchandise_Model->count_figure($search);

            $paginate = true;
            if ((($start + 1) * $limit) >= $total) {
                $paginate = false;
            }

            if ($data) {
                $response['items'] = $data;
            }

            $response['pagination']['more'] = $paginate;
            echo json_encode($response);
        }
    }
}

/* End of file Merchandise.php */
/* Location: ./application/controllers/Merchandise.php */
