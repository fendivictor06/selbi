<?php
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Main extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Dashboard_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $widget = '';
        $public_figure = $this->Dashboard_Model->jumlah_artis();
        $widget .= $this->create_element('Public Figure', 'Jumlah Public Figure', $public_figure, base_url('backend/Selebriti/index'));

        $permintaan = $this->Dashboard_Model->permintaan_merchandise();
        $widget .= $this->create_element('Permintaan Merchandise', 'Jumlah Permintaan Merchandise', $permintaan, base_url('backend/Merchandise/order'));

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'main-dashboard.js'
        );

        $data = array(
            'widget' => $widget
        );

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/dashboard/dashboard', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function create_element($title = '', $subtitle = '', $total = 0, $url = '')
    {
        return '<div class="col-md-12 col-lg-6 col-xl-6">
                    <!--begin::Total Profit-->
                    <div class="m-widget24">
                        <div class="m-widget24__item" data-url="'.$url.'" style="cursor:pointer;">
                            <h4 class="m-widget24__title">
                                '.$title.'
                            </h4>
                            <br> 
                            <span class="m-widget24__desc">
                                '.$subtitle.'
                            </span>
                          
                            <span class="m-widget24__stats m--font-brand">
                                '.$total.'
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-brand" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <!--end::Total Profit-->
                </div>';
    }

    function new_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->Dashboard_Model->new_user();

            echo json_encode($data);
        }
    }
}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */
