<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('backend/Transaksi_Model');
	}

	function index()
	{
		$this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/transaksi/transaksi.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/transaksi/transaksi', $data);
        $this->load->view('backend/template/footer', $footer);
	}

	function data_transaksi()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');
            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');

            $data = $this->Transaksi_Model->json_transaksi($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $datestart, $dateend);
            echo json_encode($data);
		} else {
			show_404();
		}
	}
}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */ ?>