<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backend/Master_Model');
    }

    function brand()
    {
        $this->Auth_Model->is_login(); 
        $header = array(
            'styles' => datatable('css') 
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/brand.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/brand', $data);
        $this->load->view('backend/template/footer', $footer);
    } 

    function data_brand()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_brand', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);
                    
                    $path = path_brand();
                    $image = '';
                    if ($val->image != '') {
                        $image = '<img src="'.$path.$val->image.'" width="48" class="image-modal" style="cursor:pointer;">';
                    }

                    $arr[$row] = [
                        $no++,
                        $val->brand,
                        $image,
                        btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function id_brand()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $arr = [];
            $brand = $this->customdb->view_by_id('ms_brand', ['id' => $id], 'row');
            if ($brand) {
                $image = isset($brand->image) ? $brand->image : '';
                $path = path_brand();
                if ($image != '') {
                    $image = $path.$image;
                }

                $arr = array(
                    'id' => isset($brand->id) ? $brand->id : '',
                    'brand' => isset($brand->brand) ? $brand->brand : '',
                    'image' => $image
                );
            }

            echo json_encode($arr);
        }
    }

    function delete_brand()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('ms_brand', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_brand()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $brand = $this->input->post('brand');
            $path = './assets/uploads/brand/';

            $config = array(
                'upload_path' => $path,
                'allowed_types' => 'jpg|png|gif',
                'file_name' => date('YmdHis')
            );

            $this->load->library('upload', $config);
            $this->upload->do_upload('image');
            $data = $this->upload->data();
            $error = $this->upload->display_errors();
            $filename = isset($data['file_name']) ? $data['file_name'] : '';

            if ($brand == '') {
                $status = 0;
                $message = '';

                if ($brand == '') {
                    $message .= 'Masukkan Brand <br>';
                }
            } else {
                $simpan = 0;
                if ($id == '') {
                    if ($error == '' && file_exists($path.$filename) == true) {
                        $simpan = 1;
                    } else {
                        $simpan = 0;
                    }
                } else {
                    $simpan = 1;
                }

                if ($simpan == 1) {
                    $username = username();
                    $time = ($id == '') ? 'insert_at' : 'update_at';
                    $user = ($id == '') ? 'user_insert' : 'user_update';
                    $now = now();

                    $data = array(
                        'brand' => $brand,
                        'deskripsi' => '',
                        $time => $now,
                        $user => $username
                    );

                    if ($filename != '' && file_exists($path.$filename) == true) {
                        $data['image'] = $filename;
                    }

                    $condition = ($id != '') ? ['id' => $id] : [];

                    $proses = $this->customdb->process_data('ms_brand', $data, $condition);
                    if ($proses > 0) {
                        $status = 1;
                        $message = 'Data berhasil disimpan';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data';
                    }
                } else {
                    $status = 0;
                    $message = 'Gagal mengupload file';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function kategori()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/kategori.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/kategori', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_kategori()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_category', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);
                    
                    $path = path_kategori();
                    $image = '';
                    if ($val->image != '') {
                        $image = '<img src="'.$path.$val->image.'" width="48" class="image-modal" style="cursor:pointer;">';
                    }

                    $arr[$row] = [
                        $no++,
                        $val->category,
                        btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function id_kategori()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $kategori = $this->customdb->view_by_id('ms_category', ['id' => $id], 'row');

            echo json_encode($kategori);
        }
    }

    function delete_kategori()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('ms_category', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_kategori()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $kategori = $this->input->post('kategori');
            $path = './assets/uploads/kategori/';

            $config = array(
                'upload_path' => $path,
                'allowed_types' => 'jpg|png|gif',
                'file_name' => date('YmdHis')
            );

            $this->load->library('upload', $config);
            $this->upload->do_upload('image');
            $data = $this->upload->data();
            $error = $this->upload->display_errors();
            $filename = isset($data['file_name']) ? $data['file_name'] : '';

            if ($kategori == '') {
                $status = 0;
                $message = '';

                if ($kategori == '') {
                    $message .= 'Masukkan kategori <br>';
                }
            } else {
                $simpan = 1;
                // if ($id == '') {
                //     if ($error == '' && file_exists($path.$filename) == true) {
                //         $simpan = 1;
                //     } else {
                //         $simpan = 0;
                //     }
                // } else {
                //     $simpan = 1;
                // }

                if ($simpan == 1) {
                    $username = username();
                    $time = ($id == '') ? 'insert_at' : 'update_at';
                    $user = ($id == '') ? 'user_insert' : 'user_update';
                    $now = now();

                    $data = array(
                        'category' => $kategori,
                        $time => $now,
                        $user => $username
                    );

                    if ($filename != '' && file_exists($path.$filename) == true) {
                        $data['image'] = $filename;
                    }

                    $condition = ($id != '') ? ['id' => $id] : [];

                    $proses = $this->customdb->process_data('ms_category', $data, $condition);
                    if ($proses > 0) {
                        $status = 1;
                        $message = 'Data berhasil disimpan';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data';
                    }
                } else {
                    $status = 0;
                    $message = 'Gagal mengupload file';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function jenis()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/jenis.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/jenis', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_jenis()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_jenis_barang', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);

                    $arr[$row] = [
                        $no++,
                        $val->jenis,
                        btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function id_jenis()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $kategori = $this->customdb->view_by_id('`ms_jenis_barang`', ['id' => $id], 'row');

            echo json_encode($kategori);
        }
    }

    function delete_jenis()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('`ms_jenis_barang`', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_jenis()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $jenis = $this->input->post('jenis');

            if ($jenis == '') {
                $status = 0;
                $message = '';

                if ($jenis == '') {
                    $message .= 'Masukkan jenis <br>';
                }
            } else {
                $username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

                $data = array(
                    'jenis' => $jenis,
                    $time => $now,
                    $user => $username
                );

                $condition = ($id != '') ? ['id' => $id] : [];

                $proses = $this->customdb->process_data('`ms_jenis_barang`', $data, $condition);
                if ($proses > 0) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function provinsi()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/provinsi.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/provinsi', $data);
        $this->load->view('backend/template/footer', $footer);
    }
    function berita()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/berita.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/berita', $data);
        $this->load->view('backend/template/footer', $footer);
    }
    function getProvinsi(){
        $status ='';
        $response='';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "", 
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: a65dfbf11d96837894e26ac9ae1ee8c4"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl); 

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          //echo $response;

          $data = json_decode($response, true);
          //echo json_encode($k['rajaongkir']['results']);
            $now = now();
            $username = username();
            $b =$this->db->truncate('ms_provinsi');
            if($b){
                for ($i=0; $i < count($data['rajaongkir']['results']); $i++){
                    $update =array(
                        'id' => isset($data['rajaongkir']['results'][$i]['province_id']) ? $data['rajaongkir']['results'][$i]['province_id'] : '',
                        'provinsi' => isset($data['rajaongkir']['results'][$i]['province']) ? $data['rajaongkir']['results'][$i]['province'] : '',
                        'update_at' => $now,
                        'user_update' => $username,
                    );
                        $this->db->insert('ms_provinsi', $update);
                        if($this->db->affected_rows()){
                            $status ='Update Berhasil Oye';
                            $response =201;
                        }else {
                            $status ='Gagal';
                            $response =401;
                        }
                }
            }
        }
        $array = array('status' => $status,'response'=> $response);
       //add the header here
        echo json_encode($array);
    }

    function refreshBerita(){
        $status ='';
        $response='';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://newsapi.org/v2/top-headlines?country=id&category=entertainment&apiKey=191e9b3deb504d39adcbdc45d769bb46",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "", 
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Accept: */*",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Host: newsapi.org",
            "Postman-Token: 45e0c2d9-6a4d-44fb-b926-c7214dfe7872,78c4d82b-a35a-47ce-af43-540553eb55cf&pageSize=100",
            "User-Agent: PostmanRuntime/7.11.0",
            "accept-encoding: gzip, deflate",
            "cache-control: no-cache"
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl); 

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          //echo $response;

          $data = json_decode($response, true);
          // print_r($data['articles'][0]['author']);
            $b =$this->db->truncate('ms_berita');
            if($b){
                for ($i=0; $i < count($data['articles']); $i++){
                    $a= str_replace("T", " ", $data['articles'][$i]['publishedAt']);
                    $hasil = str_replace("Z", " ", $a);
                    $update =array(
                        'author' => isset($data['articles'][$i]['author']) ? $data['articles'][$i]['author'] : '',
                        'title' => isset($data['articles'][$i]['title']) ? $data['articles'][$i]['title'] : '',
                        'description' => isset($data['articles'][$i]['description']) ? $data['articles'][$i]['description'] : '',
                        'url' => isset($data['articles'][$i]['url']) ? $data['articles'][$i]['url'] : '',
                        'urlToImage' => isset($data['articles'][$i]['urlToImage']) ? $data['articles'][$i]['urlToImage'] : '',
                        'publishedAt' => isset($hasil) ? $hasil : '',
                        'content' => isset($data['articles'][$i]['content']) ? $data['articles'][$i]['content'] : '',
                        'status' => 1,
                    );
                        $this->db->insert('ms_berita', $update);
                        if($this->db->affected_rows()){
                            $status ='Update Berhasil Oye';
                            $response =201;
                        }else {
                            $status ='Gagal';
                            $response =401;
                        }
                }
            }
        }
        $array = array('status' => $status,'response'=> $response);
       //add the header here
        echo json_encode($array);
    }

    function data_berita()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_berita', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    // $btn_edit = btn_edit($val->id);
                    // $btn_delete = btn_delete($val->id);

                    $arr[$row] = [
                        $no++,
                        $val->title,
                        $val->author,
                        $val->description,
                        $val->publishedAt,
                        // btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function data_provinsi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_provinsi', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    // $btn_edit = btn_edit($val->id);
                    // $btn_delete = btn_delete($val->id);

                    $arr[$row] = [
                        $no++,
                        $val->provinsi,
                        // btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function id_provinsi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $kategori = $this->customdb->view_by_id('`ms_provinsi`', ['id' => $id], 'row');

            echo json_encode($kategori);
        }
    }

    function delete_provinsi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('`ms_provinsi`', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_provinsi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $provinsi = $this->input->post('provinsi');

            if ($provinsi == '') {
                $status = 0;
                $message = '';

                if ($provinsi == '') {
                    $message .= 'Masukkan provinsi <br>';
                }
            } else {
                $username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

                $data = array(
                    'provinsi' => $provinsi,
                    $time => $now,
                    $user => $username
                );

                $condition = ($id != '') ? ['id' => $id] : [];

                $proses = $this->customdb->process_data('`ms_provinsi`', $data, $condition);
                if ($proses > 0) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function kota()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/kota.js'
        );

        $ms_provinsi = $this->customdb->view_by_id('ms_provinsi', ['status' => 1], 'result');
        $provinsi = [];
        if ($ms_provinsi) {
            foreach ($ms_provinsi as $row) {
                $provinsi[$row->id] = $row->provinsi;
            }
        }

        $data = array(
            'provinsi' => $provinsi
        );

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/kota', $data);
        $this->load->view('backend/template/footer', $footer);
    }

     function getCity(){
        // $this->Auth_Model->is_login();
        $status ='';
        $response='';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://pro.rajaongkir.com/api/city",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: a65dfbf11d96837894e26ac9ae1ee8c4"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl); 

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          //echo $response;

          $data = json_decode($response, true);
          //echo json_encode($k['rajaongkir']['results']);

        $now = now();
        $username = username();
        $b =$this->db->truncate('ms_kota');
        // print_r(count($data['rajaongkir']['results']));
        if($b){
          for ($i=0; $i < count($data['rajaongkir']['results']); $i++){

                $update =array(
                    'id' => isset($data['rajaongkir']['results'][$i]['city_id']) ? $data['rajaongkir']['results'][$i]['city_id'] : '',
                    'kota' => isset($data['rajaongkir']['results'][$i]['city_name']) ? $data['rajaongkir']['results'][$i]['city_name'] : '',
                    'id_provinsi' =>isset($data['rajaongkir']['results'][$i]['province_id']) ? $data['rajaongkir']['results'][$i]['province_id'] : '',
                    'provinsi' => isset($data['rajaongkir']['results'][$i]['province']) ? $data['rajaongkir']['results'][$i]['province'] : '',
                    'kode_pos' =>isset($data['rajaongkir']['results'][$i]['postal_code']) ? $data['rajaongkir']['results'][$i]['postal_code'] : '',
                    'type' =>isset($data['rajaongkir']['results'][$i]['type']) ? $data['rajaongkir']['results'][$i]['type'] : '',
                    'insert_at' => $now,
                    'user_insert' => $username,
                );
                $this->db->insert('ms_kota',$update);
                if($this->db->affected_rows()){
                    $status ='Update Berhasil Oye';
                    $response =201;

                }else {
                    $status ='Gagal';
                    $response =401;
                }
            }
          }
      }
            $array = array('status' => $status,'response'=> $response);
           //add the header here
            echo json_encode($array);

    }
    function getAllKecamatan(){
        $status ='';
        $response='';
        $kota = $this->db->get('ms_kota')->result();
        foreach($kota as $row){
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?city=".$row->id,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 100,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                "key: a65dfbf11d96837894e26ac9ae1ee8c4"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

        $username = username();
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $data = json_decode($response, true);

                  for ($i=0; $i < count($data['rajaongkir']['results']); $i++){
                        $update =array(
                            'id' => isset($data['rajaongkir']['results'][$i]['subdistrict_id']) ? $data['rajaongkir']['results'][$i]['subdistrict_id'] : '',
                            'id_provinsi' =>isset($data['rajaongkir']['results'][$i]['province_id']) ? $data['rajaongkir']['results'][$i]['province_id'] : '',
                            'provinsi' => isset($data['rajaongkir']['results'][$i]['province']) ? $data['rajaongkir']['results'][$i]['province'] : '',
                            'id_kota' =>isset($data['rajaongkir']['results'][$i]['city_id']) ? $data['rajaongkir']['results'][$i]['city_id'] : '',
                            'kota' =>isset($data['rajaongkir']['results'][$i]['city']) ? $data['rajaongkir']['results'][$i]['city'] : '',
                            'type' =>isset($data['rajaongkir']['results'][$i]['type']) ? $data['rajaongkir']['results'][$i]['type'] : '',
                            'kecamatan' =>isset($data['rajaongkir']['results'][$i]['subdistrict_name']) ? $data['rajaongkir']['results'][$i]['subdistrict_name'] : '',
                            'user_insert' => $username,
                        );
                        $this->db->insert('ms_kecamatan',$update);
                        if($this->db->affected_rows()){
                            $status ='Update Berhasil Oye';
                            $response =201;

                        }else {
                            $status ='Gagal';
                            $response =401;
                        }
                    }
            }
        }
       //  $array = array('status' => $status,'response'=> $response);
       // //add the header here
       //  echo json_encode($array);
    }
    
    function data_kota()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->Master_Model->view_kota();

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);

                    $arr[$row] = [
                        $no++,
                        $val->provinsi,
                        $val->kota,
                        btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function add_kota()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $provinsi = $this->input->post('provinsi');
            $kota = $this->input->post('kota');

            // check mandatory field
            if ($provinsi == '' || $kota == '') {
                $message = '';
                $status = 0;

                // jika field provinsi kosong
                $message .= ($provinsi == '') ? 'Field Provinsi wajib diisi. <br>' : '';
                $message .= ($kota == '') ? 'Field Kota wajib diisi. <br>' : '';
            } else {
                $username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

                // buat data utk disimpan
                $data = array(
                    'id_provinsi' => $provinsi,
                    'kota' => $kota,
                    $time => $now,
                    $user => $username
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_kota', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_kota()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_kota', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_kota()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $hapus = $this->customdb->process_data('ms_kota', $data, ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function pekerjaan()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        
        $footer = array(
            'script' => datatable('js'),
            'app' => 'backend/master/pekerjaan.js'
        );

        $data = array();

        $this->load->view('backend/template/header', $header);
        $this->load->view('backend/master/pekerjaan', $data);
        $this->load->view('backend/template/footer', $footer);
    }

    function data_pekerjaan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $data = $this->customdb->view_by_id('ms_pekerjaan', ['status' => 1], 'result');

            $arr = [];
            if ($data) {
                $no = 1;
                foreach ($data as $row => $val) {
                    $btn_edit = btn_edit($val->id);
                    $btn_delete = btn_delete($val->id);

                    $arr[$row] = [
                        $no++,
                        $val->pekerjaan,
                        btn_group([$btn_edit, $btn_delete])
                    ];
                }
            }

            $response = array(
                'data' => $arr
            );

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function id_pekerjaan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $kategori = $this->customdb->view_by_id('`ms_pekerjaan`', ['id' => $id], 'row');

            echo json_encode($kategori);
        }
    }

    function delete_pekerjaan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $now = now();
            $username = username();

            $data = array(
                'status' => 0,
                'update_at' => $now,
                'user_update' => $username
            );

            $delete = $this->customdb->process_data('`ms_pekerjaan`', $data, ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_pekerjaan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $pekerjaan = $this->input->post('pekerjaan');

            if ($pekerjaan == '') {
                $status = 0;
                $message = '';

                if ($pekerjaan == '') {
                    $message .= 'Masukkan pekerjaan <br>';
                }
            } else {
                $username = username();
                $time = ($id == '') ? 'insert_at' : 'update_at';
                $user = ($id == '') ? 'user_insert' : 'user_update';
                $now = now();

                $data = array(
                    'pekerjaan' => $pekerjaan,
                    $time => $now,
                    $user => $username
                );

                $condition = ($id != '') ? ['id' => $id] : [];

                $proses = $this->customdb->process_data('`ms_pekerjaan`', $data, $condition);
                if ($proses > 0) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function ajax_kota()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $search = $this->input->post('search');
            $provinsi = $this->input->post('provinsi');

            $arr = [];
            $query = $this->Master_Model->ajax_kota($provinsi, $search);
            if ($query) {
                foreach ($query as $row => $val) {
                    $arr[$row] = array(
                        'id' => $val->id,
                        'kota' => $val->kota
                    );
                }
            }

            echo json_encode($arr);
        } else {
            show_404();
        }
    }

}

/* End of file Master.php */
/* Location: ./application/controllers/Master.php */
