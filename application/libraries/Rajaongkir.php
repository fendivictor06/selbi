<?php  

class Rajaongkir {

	// starter url
	// private $url = 'https://api.rajaongkir.com/starter/';
	// basic url
	// private $url = 'https://api.rajaongkir.com/basic/';
	// pro url
	private $url = 'https://pro.rajaongkir.com/api/';

	private $key = 'a65dfbf11d96837894e26ac9ae1ee8c4';

	function webservice($url = '', $data = '', $method = 'GET', $header = [])
	{
		ini_set('memory_limit', '-1');
	    ini_set('MAX_EXECUTION_TIME', '-1');
	    set_time_limit(0);
	    
		$curl = curl_init();
		$key = $this->key;
 
		$header[] = 'key:'.$key;

		curl_setopt_array($curl, array(   
			CURLOPT_URL => $url,   
			CURLOPT_RETURNTRANSFER => true,   
			CURLOPT_CUSTOMREQUEST => $method,   
			CURLOPT_POSTFIELDS => $data,   
			CURLOPT_HTTPHEADER => $header,
			CURLOPT_SSL_VERIFYPEER => false
		)); 
	 
		$response = curl_exec($curl); 
		$error = curl_error($curl);

		curl_close($curl);

		if ($error) {
			return [
				'status' => 0, 
				'response' => $error
			];
		} else {
			return [
				'status' => 1,
				'response' => $response
			];
		}
	}

	function api_url($url = '')
	{
		$base_url = $this->url;
		switch ($url) {
			case 'province':
				return [
					'url' => $base_url.'province',
					'method' => 'GET'
				];
				break;
			case 'city':
				return [
					'url' => $base_url.'city',
					'method' => 'GET'
				];
				break;
			case 'cost':
				return [
					'url' => $base_url.'cost',
					'method' => 'POST'
				];
				break;
			case 'subdistrict':
				return [
					'url' => $base_url.'subdistrict',
					'method' => 'GET'
				];
				break;
			case 'waybill':
				return [
					'url' => $base_url.'waybill',
					'method' => 'POST'
				];
				break;
			default:
				return [];
				break;
		}
	}

	function request($type = '', $params=[])
	{
		$ci =& get_instance();
		$request = $this->api_url($type);
		$url = isset($request['url']) ? $request['url'] : '';
		$method = isset($request['method']) ? $request['method'] : '';
		$key = $this->key;

		// id for province & city
		$id = isset($params['id']) ? $params['id'] : '';
		// province for city
		$province = isset($params['province']) ? $params['province'] : '';
		// city for subdistrict
		$city = isset($params['city']) ? $params['city'] : '';
		// all parameters below for cost api
		$origin = isset($params['origin']) ? $params['origin'] : '';
		$destination = isset($params['destination']) ? $params['destination'] : '';
		$weight = isset($params['weight']) ? $params['weight'] : '';
		$courier = isset($params['courier']) ? $params['courier'] : '';
		$origin_type = isset($params['originType']) ? $params['originType'] : '';
        $destination_type = isset($params['destinationType']) ? $params['destinationType'] : '';
		// waybill
		$waybill = isset($params['waybill']) ? $params['waybill'] : '';

		if ($type == 'cost' && $origin_type == '') {
			$origin_type = 'city';
		}

		if ($type == 'cost' && $destination_type == '') {
			$destination_type = 'city';
		}

		$arr = [];
		if ($id != '') {
			$arr['id'] = $id;
		}

		if ($province != '') {
			$arr['province'] = $province;
		}

		if ($origin != '') {
			$arr['origin'] = $origin;
		}

		if ($destination != '') {
			$arr['destination'] = $destination;
		}

		if ($weight != '') {
			$arr['weight'] = $weight;
		}

		if ($courier != '') {
			$arr['courier'] = $courier;
		}

		if ($city != '') {
			$arr['city'] = $city;
		}

		if ($waybill != '') {
			$arr['waybill'] = $waybill;
		}

		if ($origin_type != '') {
			$arr['originType'] = $origin_type;
		}

		if ($destination_type != '') {
			$arr['destinationType'] = $destination_type;
		}
 
		# build parameter
		$data = http_build_query($arr);
		$response = [];
		if ($url != '' && $method != '') {
			if ($type == 'province' && $method == 'GET') {
				$result = $this->webservice($url.'?'.$data, [], $method);
			} else if ($type == 'city' && $method == 'GET') {
				$result = $this->webservice($url.'?'.$data, [], $method);
			} else if ($type == 'subdistrict' && $method == 'GET') {
				$result = $this->webservice($url.'?'.$data, [], $method);
			} else if ($type == 'waybill' && $method == 'POST') {
				$header[] = 'Content-Type:application/x-www-form-urlencoded';
				$result = $this->webservice($url, $data, $method, $header);
			} else if ($type == 'cost' && $method == 'POST') {
				$hasil = [];
				$header[] = 'Content-Type:application/x-www-form-urlencoded';
				if ($courier == '') {
					$ms_kurir = $ci->db->where('status', 1)
									->get('ms_kurir')
									->result();

					if ($ms_kurir) {
						foreach ($ms_kurir as $row) {
							$data .= '&courier='.$row->kode;
							$cost_result = $this->webservice($url, $data, $method, $header);
							if ($cost_result) {
								$create_results = $this->create_results($cost_result);
								if ($create_results) {
									$hasil[] = $create_results;
								}
							}
						}
					}
				} else {
					$cost_result = $this->webservice($url, $data, $method, $header);
					if ($cost_result) {
						$create_results = $this->create_results($cost_result);
						if ($create_results) {
							$hasil[] = $create_results;
						}
					}
				}

				$result = array(
					'status' => 1,
					'response' => $hasil
				);		
			} else {
				$result = [];
			}

			if ($result) {
				$ws_result = isset($result['status']) ? $result['status'] : 0;
				if ($ws_result == 1) {
					$status = 1;
					$message = 'Berhasil';

					$response = isset($result['response']) ? $result['response'] : '';
				} else {
					$status = 0;
					$message = 'Terjadi kesalahan saat terhubung ke third-party';

					$response = isset($result['response']) ? $result['response'] : '';
				}
			} else {
				$status = 0;
				$message = 'Parameter tidak sesuai';
			}
		} else {
			$status = 0;
			$message = 'Unknown Method';
		}

		return array(
			'status' => $status,
			'message' => $message,
			'response' => $response
		);
	}

	function create_results($result = [])
	{
		$hasil = [];
		if ($result) {
			$expedisi = isset($result['response']) ? $result['response'] : [];
			$expedisi = json_decode($expedisi, true);
			$expedisi = isset($expedisi['rajaongkir']['results']) ? $expedisi['rajaongkir']['results'] : [];

			for ($i = 0; $i < count($expedisi); $i++) {
				$costs = isset($expedisi[$i]['costs']) ? $expedisi[$i]['costs'] : [];

				if ($costs) {
					for ($c = 0; $c < count($costs); $c++) {
						$cost = isset($costs[$c]['cost']) ? $costs[$c]['cost'] : [];

						if ($cost) {
							for ($s = 0; $s < count($cost); $s++) {
								$hasil[] = array(
									'code' => isset($expedisi[$i]['code']) ? $expedisi[$i]['code'] : '',
									'name' => isset($expedisi[$i]['name']) ? $expedisi[$i]['name'] : '',
									'service' => isset($costs[$c]['service']) ? $costs[$c]['service'] : '',
									'description' => isset($costs[$c]['description']) ? $costs[$c]['description'] : '',
									'value' => isset($cost[$s]['value']) ? $cost[$s]['value'] : '',
									'etd' => isset($cost[$s]['etd']) ? $cost[$s]['etd'] : '',
									'note' => isset($cost[$s]['note']) ? $cost[$s]['note'] : ''
	 							);
							}
						}
					}
				}							
			}	
		}

		return $hasil;
	}
}

?>