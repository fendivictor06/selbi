<?php  
class Midtrans
{
    private $dev = true;

    function url() 
    {
        $dev = $this->dev;
        $url = ($dev) ? 'https://app.sandbox.midtrans.com/snap/v1/transactions' : 'https://app.midtrans.com/snap/v1/transactions';

        return $url;
    }

    function server_key() 
    {
        $dev = $this->dev;
        $key = ($dev) ? 'SB-Mid-server-fE7PG4BJnthhy4d6BM7INlKz' : 'Mid-server-Q774NCZrcYyRd6Mid3DK0YJp';

        return $key;
    }

    function last_counter($tipe = '')
    {
        $ci =& get_instance();
        $bulan = date('m');
        $tahun = date('Y');

        return $ci->db->query("
            SELECT *
            FROM counter_invoice a
            WHERE a.`tipe` = '$tipe'
            AND a.`bulan` = '$bulan'
            AND a.tahun = '$tahun' ")->row();
    }

    function invoice($tipe = 'user', $flag = 'INV')
    {
        $ci =& get_instance();
        $counter = $this->last_counter($tipe);
        $bulan = date('m');
        $tahun = date('Y');

        if (! empty($counter)) {
            $max = isset($counter->counter) ? $counter->counter : 0;
            $max = $max + 1;
            $data = array(
                'counter' => $max,
            );

            $condition = array(
                'bulan' => $bulan,
                'tahun' => $tahun,
                'tipe' => $tipe
            );

            $ci->db->where($condition)
                ->update('counter_invoice', $data);
            
            switch (strlen($max)) {
                case '1':
                    $max = '000'.$max;
                    break;
                case '2':
                    $max = '00'.$max;
                    break;
                case '3':
                    $max = '0'.$max;
                    break;
                default:
                    $max = $max;
                    break;
            }


            $nonota = $flag.'/'.date('ym').'/'.$max;
        } else {
            $data = array(
                'counter' => 1,
                'bulan' => $bulan,
                'tahun' => $tahun,
                'tipe' => $tipe
            );

            $ci->db->insert('counter_invoice', $data);

            $nonota = $flag.'/'.date('ym').'/0001';
        } 

        // check if exists on transaksi
        $exists = $ci->db->where('order_id', $nonota)
                    ->get('temp_transaksi')
                    ->row();

        if (! empty($exists)) {
            $this->generate_nobukti($kdcus);
        } else {
            return $nonota;
        }
    }

    function payment($get = [])
    {
        $url = $this->url();
        $server_key = $this->server_key();
        $ci =& get_instance();

        $nobukti = $this->invoice('user-midtrans', 'SEL');
        $get['transaction_details']['order_id'] = $nobukti;

        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            // Add header to the request, including Authorization generated from server key
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json', 
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode($server_key . ':')
            ),
            CURLOPT_POSTFIELDS => json_encode($get)
        );
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);

        // get data from request paramete store to temp_transaksi
        $decode = $get;
        $customer_details = isset($decode['customer_details']) ? $decode['customer_details'] : '';
        // billing address
        $billing_address = isset($customer_details['billing_address']) ? $customer_details['billing_address'] : '';
        // shipping address
        $shipping_address = isset($customer_details['shipping_address']) ? $customer_details['shipping_address'] : '';
        // credit card
        $credit_card = isset($decode['credit_card']) ? $decode['credit_card'] : '';
        // trx_details
        $transaction_details = isset($decode['transaction_details']) ? $decode['transaction_details'] : '';
 
        $email = isset($customer_details['email']) ? $customer_details['email'] : '';
        $first_name = isset($customer_details['first_name']) ? $customer_details['first_name'] : '';
        $phone = isset($customer_details['phone']) ? $customer_details['phone'] : '';

        $address_billing = isset($billing_address['address']) ? $billing_address['address'] : '';
        $billing_city = isset($billing_address['city']) ? $billing_address['city'] : '';
        $billing_country_code = isset($billing_address['country_code']) ? $billing_address['country_code'] : '';
        $billing_firstname = isset($billing_address['first_name']) ? $billing_address['first_name'] : '';
        $billing_phone = isset($billing_address['phone']) ? $billing_address['phone'] : '';
        $billing_postalcode = isset($billing_address['postal_code']) ? $billing_address['postal_code'] : '';

        $address_shipping = isset($shipping_address['address']) ? $shipping_address['address'] : '';
        $shipping_city = isset($shipping_address['city']) ? $shipping_address['city'] : '';
        $shipping_country_code = isset($shipping_address['country_code']) ? $shipping_address['country_code'] : '';
        $shipping_firstname = isset($shipping_address['first_name']) ? $shipping_address['first_name'] : '';
        $shipping_phone = isset($shipping_address['phone']) ? $shipping_address['phone'] : '';
        $shipping_postalcode = isset($shipping_address['postal_code']) ? $shipping_address['postal_code'] : '';

        $credit_auth = isset($credit_card['authentication']) ? $credit_card['authentication'] : '';
        $bank = isset($credit_card['bank']) ? $credit_card['bank'] : '';
        $save_card = isset($credit_card['save_card']) ? $credit_card['save_card'] : '';
        $secure = isset($credit_card['secure']) ? $credit_card['secure'] : '';
        
        $gross_amount = isset($transaction_details['gross_amount']) ? $transaction_details['gross_amount'] : '';
        $order_id = isset($transaction_details['order_id']) ? $transaction_details['order_id'] : '';
        $currency = isset($transaction_details['currency']) ? $transaction_details['currency'] : '';
        $user_id = isset($decode['user_id']) ? $decode['user_id'] : '';
        $uid = userid();

        // items
        $item = isset($decode['item_details']) ? $decode['item_details'] : '';
        $temporary_detail = [];
        if ($item) {
            foreach ($item as $row) {
                $id_item = isset($row['id']) ? $row['id'] : '';
                $name_item = isset($row['name']) ? $row['name'] : '';
                $price_item = isset($row['price']) ? $row['price'] : '';
                $quantity_item = isset($row['quantity']) ? $row['quantity'] : '';

                $exp_id = explode('-', $id_item);
                $items_id = '';
                $kurir = '';
                $id_penjual = '';
                if (count($exp_id) > 1) {
                    $kurir = isset($exp_id[0]) ? $exp_id[0] : '';
                    $id_penjual = isset($exp_id[1]) ? $exp_id[1] : '';

                    $items_id = 0;
                } else {
                    $items_id = $id_item;

                    $keranjang = $ci->db->where(['id' => $items_id])->get('tb_keranjang')->row();
                    $id_penjual = isset($keranjang->id_penjual) ? $keranjang->id_penjual : '';
                }
                
                $temporary_detail[] = array(
                    'order_id' => $order_id,
                    'id_cart' => $items_id,
                    'name' => $name_item,
                    'price' => $price_item,
                    'quantity' => $quantity_item,
                    'id_penjual' => $id_penjual,
                    'kurir' => $kurir,
                    'user_insert' => $uid
                );

                // $ci->Main_Model->process_data('tb_keranjang', ['flag' => 2], ['id' => $id_item]);
            }
        }

        $temporary = array(
            'order_id' => $order_id,
            'billing_address' => $address_billing,
            'billing_city' => $billing_city,
            'billing_country_code' => $billing_country_code,
            'billing_firstname' => $billing_firstname,
            'billing_phone' => $billing_phone,
            'billing_postalcode' => $billing_postalcode,
            'email' => $email,
            'first_name' => $first_name,
            'phone' => $phone,
            'shipping_address' => $address_shipping,
            'shipping_city' => $shipping_city,
            'shipping_country_code' => $shipping_country_code,
            'shipping_firstname' => $shipping_firstname,
            'shipping_phone' => $shipping_phone,
            'shipping_postalcode' => $shipping_postalcode,
            'currency' => $currency,
            'gross_amount' => $gross_amount,
            'credit_auth' => $credit_auth,
            'bank' => $bank,
            'save_card' => $save_card,
            'secure' => $secure,
            'user_id' => $user_id,
            'user_insert' => $uid
        );

        $ci->load->model('api/Transaksi_Model');
        $ci->Transaksi_Model->log_transaksi($temporary, $temporary_detail);

        log_api($get, 200, 'Berhasil', [$temporary, $temporary_detail]);
        return $result;
    }

    function lelang($get = [])
    {
        $url = $this->url();
        $server_key = $this->server_key();
        $ci =& get_instance();

        $nobukti = $this->invoice('user-midtrans', 'BID');
        $get['transaction_details']['order_id'] = $nobukti;

        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            // Add header to the request, including Authorization generated from server key
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json', 
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode($server_key . ':')
            ),
            CURLOPT_POSTFIELDS => json_encode($get)
        );
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);

        // get data from request paramete store to temp_transaksi
        $decode = $get;
        $customer_details = isset($decode['customer_details']) ? $decode['customer_details'] : '';
        // billing address
        $billing_address = isset($customer_details['billing_address']) ? $customer_details['billing_address'] : '';
        // shipping address
        $shipping_address = isset($customer_details['shipping_address']) ? $customer_details['shipping_address'] : '';
        // credit card
        $credit_card = isset($decode['credit_card']) ? $decode['credit_card'] : '';
        // trx_details
        $transaction_details = isset($decode['transaction_details']) ? $decode['transaction_details'] : '';
 
        $email = isset($customer_details['email']) ? $customer_details['email'] : '';
        $first_name = isset($customer_details['first_name']) ? $customer_details['first_name'] : '';
        $phone = isset($customer_details['phone']) ? $customer_details['phone'] : '';

        $address_billing = isset($billing_address['address']) ? $billing_address['address'] : '';
        $billing_city = isset($billing_address['city']) ? $billing_address['city'] : '';
        $billing_country_code = isset($billing_address['country_code']) ? $billing_address['country_code'] : '';
        $billing_firstname = isset($billing_address['first_name']) ? $billing_address['first_name'] : '';
        $billing_phone = isset($billing_address['phone']) ? $billing_address['phone'] : '';
        $billing_postalcode = isset($billing_address['postal_code']) ? $billing_address['postal_code'] : '';

        $address_shipping = isset($shipping_address['address']) ? $shipping_address['address'] : '';
        $shipping_city = isset($shipping_address['city']) ? $shipping_address['city'] : '';
        $shipping_country_code = isset($shipping_address['country_code']) ? $shipping_address['country_code'] : '';
        $shipping_firstname = isset($shipping_address['first_name']) ? $shipping_address['first_name'] : '';
        $shipping_phone = isset($shipping_address['phone']) ? $shipping_address['phone'] : '';
        $shipping_postalcode = isset($shipping_address['postal_code']) ? $shipping_address['postal_code'] : '';

        $credit_auth = isset($credit_card['authentication']) ? $credit_card['authentication'] : '';
        $bank = isset($credit_card['bank']) ? $credit_card['bank'] : '';
        $save_card = isset($credit_card['save_card']) ? $credit_card['save_card'] : '';
        $secure = isset($credit_card['secure']) ? $credit_card['secure'] : '';
        
        $gross_amount = isset($transaction_details['gross_amount']) ? $transaction_details['gross_amount'] : '';
        $order_id = isset($transaction_details['order_id']) ? $transaction_details['order_id'] : '';
        $currency = isset($transaction_details['currency']) ? $transaction_details['currency'] : '';
        $user_id = isset($decode['user_id']) ? $decode['user_id'] : '';
        $uid = userid();

        // items
        $item = isset($decode['item_details']) ? $decode['item_details'] : '';
        $temporary_detail = [];
        if ($item) {
            foreach ($item as $row) {
                $id_item = isset($row['id']) ? $row['id'] : '';
                $name_item = isset($row['name']) ? $row['name'] : '';
                $price_item = isset($row['price']) ? $row['price'] : '';
                $quantity_item = isset($row['quantity']) ? $row['quantity'] : '';

                $exp_id = explode('-', $id_item);
                $items_id = '';
                $kurir = '';
                $id_penjual = '';
                if (count($exp_id) > 1) {
                    $kurir = isset($exp_id[0]) ? $exp_id[0] : '';
                    $id_penjual = isset($exp_id[1]) ? $exp_id[1] : '';

                    $items_id = 0;
                } else {
                    $items_id = $id_item;

                    // $keranjang = $ci->db->where(['id' => $items_id])->get('tb_keranjang')->row();
                    $keranjang = $ci->db->query("SELECT * from tb_lelang l
                                                join view_barang b on b.id=l.id_barang  where l.id='$items_id'")->row();
                    // print_r($keranjang);
                    // $keranjang = $ci->db->query("SELECT * from  view_barang id='items_id' ")->row();
                    // $id_penjual = isset($keranjang->id_penjual) ? $keranjang->id_penjual : '';
                    $id_penjual = $keranjang->id_penjual;
                }
                
                $temporary_detail[] = array(
                    'order_id' => $order_id,
                    'id_cart' => $items_id,
                    'name' => $name_item,
                    'price' => $price_item,
                    'quantity' => $quantity_item,
                    'id_penjual' => $id_penjual,
                    'kurir' => $kurir,
                    'user_insert' => $uid
                );

                // $ci->Main_Model->process_data('tb_keranjang', ['flag' => 2], ['id' => $id_item]);
            }
        }

        $temporary = array(
            'order_id' => $order_id,
            'billing_address' => $address_billing,
            'billing_city' => $billing_city,
            'billing_country_code' => $billing_country_code,
            'billing_firstname' => $billing_firstname,
            'billing_phone' => $billing_phone,
            'billing_postalcode' => $billing_postalcode,
            'email' => $email,
            'first_name' => $first_name,
            'phone' => $phone,
            'shipping_address' => $address_shipping,
            'shipping_city' => $shipping_city,
            'shipping_country_code' => $shipping_country_code,
            'shipping_firstname' => $shipping_firstname,
            'shipping_phone' => $shipping_phone,
            'shipping_postalcode' => $shipping_postalcode,
            'currency' => $currency,
            'gross_amount' => $gross_amount,
            'credit_auth' => $credit_auth,
            'bank' => $bank,
            'save_card' => $save_card,
            'secure' => $secure,
            'user_id' => $user_id,
            'type' =>'bid',
            'user_insert' => $uid
        );

        $ci->load->model('api/Transaksi_Model');
        $ci->Transaksi_Model->log_transaksi($temporary, $temporary_detail);

        log_api($get, 200, 'Berhasil', [$temporary, $temporary_detail]);
        return $result;
    }
}
?>