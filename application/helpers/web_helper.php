<?php

function datatable($param = 'css')
{
    switch ($param) {
        case 'js':
            return '<script type="text/javascript" src="' . base_url() . 'assets/plugins/datatables/datatables.min.js"></script>
                    <script type="text/javascript" src="' . base_url() . 'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>';
            break;
        
        default:
            return '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/plugins/datatables/datatables.min.css">
                    <link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css">';
            break;
    }
}

function ShowDataById($table, $where, $show)
{
    $ci =& get_instance();
    $query = $ci->db->get_where($table, $where);
    $row   = $query->row();
    if ($query->num_rows() > 0) {
        return $row->$show;
    } else {
        return '-';
    }
    
}

function getById($table = '', $field = '', $id = '', $show = '')
{
    $ci =& get_instance();
    
    $condition = '';
    if ($id != '') {
        $condition = " WHERE $field = '$id' ";
    }
    
    $query = $ci->db->query("
        SELECT *
        FROM $table 
        $condition ")->row();
    
    $result = isset($query->$show) ? $query->$show : '';
    
    return $result;
}

function build_query($data, $where = 'WHERE')
{
    $result = '';
    if ($data) {
        $jumlah = count($data);
        $result .= ' (';
        for ($i = 0; $i < $jumlah; $i++) {
            $result .= $where . " = '" . $data[$i] . "'";
            
            if (end($data) == $data[$i]) {
                $result .= '';
            } else {
                $result .= ' OR ';
            }
        }
        
        $result .= ')';
    }
    
    return $result;
}

function rupiah($angka)
{
    
    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;
    
}

function build_query_table($data, $where = 'WHERE', $table = '')
{
    $result = '';
    if ($data) {
        $jumlah = count($data);
        $result .= ' (';
        for ($i = 0; $i < $jumlah; $i++) {
            $result .= $table . "." . $where . " = '" . $data[$i] . "'";
            
            if (end($data) == $data[$i]) {
                $result .= '';
            } else {
                $result .= ' OR ';
            }
        }
        
        $result .= ')';
    }
    
    return $result;
}

function tbl_template($id = 'dataTables-example', $class = '')
{
    $template = array(
        'table_open' => '<table class="table table-striped table-bordered table-hover table-checkable order-column ' . $class . '" id="' . $id . '">',
        
        'thead_open' => '<thead>',
        'thead_close' => '</thead>',
        
        'heading_row_start' => '<tr>',
        'heading_row_end' => '</tr>',
        'heading_cell_start' => '<th>',
        'heading_cell_end' => '</th>',
        
        'tbody_open' => '<tbody>',
        'tbody_close' => '</tbody>',
        
        'row_start' => '<tr>',
        'row_end' => '</tr>',
        'cell_start' => '<td>',
        'cell_end' => '</td>',
        
        'row_alt_start' => '<tr>',
        'row_alt_end' => '</tr>',
        'cell_alt_start' => '<td>',
        'cell_alt_end' => '</td>',
        
        'table_close' => '</table>'
    );
    
    return $template;
}

function web_title()
{
    $ci =& get_instance();
    $class  = $ci->router->fetch_class();
    $method = $ci->router->fetch_method();
    
    $menu = $ci->Menu_Model->parent($class, $method);
    
    return isset($menu->label) ? $menu->label : '';
}

function rname($bil)
{
    $bil = str_replace("'", "-", $bil);
    $bil = str_replace('"', '-', $bil);
    $bil = str_replace(' ', '-', $bil);
    $bil = str_replace('*', '-', $bil);
    $bil = str_replace('`', '-', $bil);
    $bil = str_replace('!', '-', $bil);
    $bil = str_replace('&', '-', $bil);
    $bil = str_replace('=', '-', $bil);
    $bil = str_replace(';', '-', $bil);
    $bil = str_replace(':', '-', $bil);
    $bil = str_replace('?', '-', $bil);
    $bil = str_replace('%', '-', $bil);
    $bil = str_replace('@', '-', $bil);
    $bil = str_replace('~', '-', $bil);
    $bil = str_replace('#', '-', $bil);
    $bil = str_replace('$', '-', $bil);
    $bil = str_replace('^', '-', $bil);
    $bil = str_replace('(', '-', $bil);
    $bil = str_replace(')', '-', $bil);
    $bil = str_replace('/', '-', $bil);
    $bil = str_replace('\\', '-', $bil);
    $bil = str_replace(',', '-', $bil);
    $bil = str_replace('--', '-', $bil);
    $bil = str_replace('--', '-', $bil);
    $bil = strtolower($bil);
    return $bil;
}

function response_datatable($draw = '', $total_filtered = 0, $data)
{
    $arr = array(
        'draw' => $draw,
        'recordsFiltered' => $total_filtered,
        'recordsTotal' => $total_filtered,
        'data' => $data
    );
    
    return $arr;
}

function btn_edit($id = '')
{
    return '<a href="javascript:;" data-refid="' . $id . '" class="btn btn-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill update" title="Update data">
                <i class="fa fa-edit"></i>
            </a>';
}


function btn_custom_edit($id = '')
{
    $customId = str_replace("/", "_", $id);
    return '<a href="javascript:;" data-refid="' . $customId . '" class="btn btn-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill update" title="Update data">
                <i class="fa fa-edit"></i>
            </a>';
}

function btn_delete($id = '')
{
    return '<a href="javascript:;" data-refid="' . $id . '" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill delete" title="Hapus data">
                <i class="fa fa-trash"></i>
            </a>';
}

function btn_custom($id = '', $action = '', $url = 'javascript:;', $class = 'btn-info', $icon = 'fa fa-check', $title = '')
{
    $target = '';
    if ($url != '') {
        $target = 'target="_blank"';
    }
    
    return '<a href="' . $url . '" data-refid="' . $id . '" class="btn ' . $class . ' m-btn m-btn--icon m-btn--icon-only m-btn--pill ' . $action . '" title="' . $title . '" ' . $target . '>
                <i class="' . $icon . '"></i>
            </a>';
}

function btn_group($button)
{
    $result = '';
    if (!empty($button)) {
        $result .= '<div class="m-demo__preview m-demo__preview--btn">';
        for ($i = 0; $i < count($button); $i++) {
            if (!empty($button[$i])) {
                $result .= $button[$i] . '&nbsp;';
            }
        }
        $result .= '</div>';
    }
    
    return $result;
}

function user_id()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('User-Id');
}

function id_karyawan()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('Id-Karyawan');
}

function id_company()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('Id-Company');
}

function id_user()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('Id-User');
}

function token_auth()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('Token');
}

function search_datatable($kolom, $search = '')
{
    $condition = '';
    if ($search != '') {
        $condition .= ' AND (';
        for ($i = 0; $i < count($kolom); $i++) {
            $condition .= $kolom[$i] . " LIKE '%$search%' ";
            if ($kolom[$i] != end($kolom)) {
                $condition .= ' OR ';
            }
        }
        $condition .= ')';
    }
    
    return $condition;
}

function order_datatable($kolom_order, $column = '', $dir = '')
{
    $order = '';
    if ($column != '' && $dir != '') {
        $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';
        
        if ($col != '') {
            $order .= " ORDER BY $col $dir ";
        }
    }
    
    return $order;
}

function decode_image($file = '', $path = '')
{
    $decoder = base64_decode($file);
    header('Content-Type: bitmap; charset=utf-8');
    $open = fopen($path, 'wb');
    fwrite($open, $decoder);
    fclose($open);
}

function filter_params($params = '', $index = '')
{
    $ci =& get_instance();
    return isset($params[$index]) ? $ci->db->escape_str($params[$index]) : '';
}

function filter_isset($params = '')
{
    return isset($params) ? $params : '';
}

function file_ext($filename = '')
{
    $filename = explode('.', $filename);
    $filename = end($filename);
    
    return $filename;
}

function file_type($filetype = '')
{
    $exp_foto_type = explode('/', $filetype);
    
    return isset($exp_foto_type[0]) ? $exp_foto_type[0] : '';
}

function tglConvert($tgl)
{
    $str   = $tgl;
    $g     = explode("-", $str);
    $h     = $g[1];
    $bulan = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    );
    
    $hasil = $bulan[$h];
    print_r($hasil);
    return $g[2] . '-' . $hasil . '-' . $g[0];
}

function TglID($date)
{
    $tgl = strtotime($date);
    return date('d/m/Y', $tgl);
    
}

/* Create by - Akbar */
function alert($msg = '', $param = 'success')
{
    switch ($param) {
        case 'info':
            return '<div class="alert alert-info alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> ' . $msg . ' </div>';
            break;
        
        case 'danger':
            return '<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> ' . $msg . ' </div>';
            break;
        
        case 'warning':
            return '<div class="alert alert-warning alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> ' . $msg . ' </div>';
            break;
        
        default:
            return '<div class="alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong>Berhasil !</strong> ' . $msg . ' </div>';
            break;
    }
}
/* End Create by - Akbar */