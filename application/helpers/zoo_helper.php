<?php
    
function custom()
{
    return '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/dashboard/css/custom.css">';
}

function date_picker($param = 'css')
{
    switch ($param) {
        case 'js':
            return '<script src="'.base_url().'assets/plugins/date-picker/js/bootstrap-datepicker.min.js"></script>
                    <script src="'.base_url().'assets/frontend/js/custome_date.js"></script>';
            break;
        
        default:
            return '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/date-picker/css/bootstrap-datepicker.min.css">
                    <link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/date-picker/css/bootstrap-datepicker.standalone.css">';
            break;
    }
}

function img_light_box($param = 'css')
{
    switch ($param) {
        case 'js':
            return '<script src="'.base_url().'assets/plugins/imagelightbox/imagelightbox.js"></script>';
            break;
        
        default:
            return '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/imagelightbox/imagelightbox.css">';
            break;
    }
}

function zoom()
{
    return '<script src="'.base_url().'assets/plugins/jzoom/jquery.zoom.min.js"></script>';
}

function sweet_alert()
{
    return '<script src="'.base_url().'assets/plugins/sweet-alert/sweet-alert.js"></script>';
}

function slimscroll()
{
    return '<script src="'.base_url().'assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>';
}

function dropzone($param = 'css')
{
    switch ($param) {
        case 'js':
            return '<script type="text/javascript" src="'.base_url().'assets/plugins/dropzone/dropzone.min.js"></script>';
            break;
        
        default:
            return '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/dropzone/dropzone.min.css"><link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/dropzone/basic.min.css">';
            break;
    }
}

function chartjs()
{
    return '<script type="text/javascript" src="'.base_url().'assets/plugins/chartjs/Chart.bundle.js"></script>
            <script type="text/javascript" src="'.base_url().'assets/plugins/chartjs/utils.js"></script>';
}