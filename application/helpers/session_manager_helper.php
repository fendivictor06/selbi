<?php

function username()
{
    $ci =& get_instance();

    return $ci->session->userdata('username');
}

function profile_name()
{
    $ci =& get_instance();

    return $ci->session->userdata('profile_name');
}

function uid()
{
    $ci =& get_instance();

    return $ci->session->userdata('uid');
}

function level_user()
{
    $ci =& get_instance();

    $level = $ci->session->userdata('level');
    return $level;
}
