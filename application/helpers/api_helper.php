<?php defined('BASEPATH') or exit('No direct script access allowed');

function print_json($status = 200, $message = '', $data = [])
{
    $ci =& get_instance();
    $response = response($status, $message, $data);

    return $ci->auth->print_json($response);
}

function get_params()
{
    return json_decode(file_get_contents('php://input'), true);
}

function response($status = 200, $message = '', $data = [])
{
    return array(
        'response' => $data,
        'metadata' => array(
            'status' => $status,
            'message' => $message
        )
    );
}

    # do enkrip id
function enkrip_id($id = '')
{
    $date_init = new DateTime(date('Y-m-d'));
    $time = $date_init->getTimestamp();
    $date = getdate();
    $baru = $id * $date['year'] * $date['mon'] * $date['mday'];

    return ($baru + $time)*$date['hours'];
}

    # do dekrip id
function dekrip_id($id = '')
{
    $date_init = new DateTime(date('Y-m-d'));
    $time = $date_init->getTimestamp();
    $date = getdate();
    $div = $id / $date['hours'];
    $new_id = $div - $time;
        
    return (($new_id/$date['year'])/$date['mon'])/$date['mday'];
}

function path_brand()
{
    return base_url('assets/uploads/brand/');
}

function path_kategori()
{
    return base_url('assets/uploads/kategori/');
}

function path_pelapak()
{
    return base_url('assets/uploads/pelapak/');
}

function path_produk()
{
    return base_url('assets/uploads/product/');
}

function path_slider()
{
    return base_url('assets/uploads/slider/');
}

function path_banner()
{
    return base_url('assets/uploads/banner/');
}

function path_gallery()
{
    return base_url('assets/uploads/feed/gallery/');
}

function path_kegiatan()
{
    return base_url('assets/uploads/feed/kegiatan/');
}

function path_merchandise()
{
    return base_url('assets/uploads/merchandise/');
}

function path_penawaran()
{
    return base_url('assets/uploads/penawaran/');
}

function path_attribute()
{
    return base_url('assets/uploads/item/');
}

function path_hotnews()
{
    return base_url('assets/uploads/news/');
}

function log_api($request = '', $status = '', $message = '', $data = [])
{
    $ci =& get_instance();

    $ci->load->model('api/Log_Model');

    $response = array(
        'status' => $status,
        'message' => $message,
        'data' => $data
    );

    $response = json_encode($response);
    $request = json_encode($request);

    $user = userid();

    $ci->Log_Model->log($request, $response, $user);
}

function userid()
{
    $ci =& get_instance();

    $userid = $ci->input->get_request_header('User-Id');

    return isset($userid) ? $userid : '';
}

function iduser()
{
    $ci =& get_instance();

    $userid = userid();

    $ms_user = $ci->db->where('uid', $userid)
                    ->get('tb_user')
                    ->row();

    $id_user = isset($ms_user->id) ? $ms_user->id : '';

    return $id_user;
}

function array_to_query($arr = [])
{
    $string = '';
    if ($arr) {
        $jumlah = count($arr);
        for ($i = 0; $i < $jumlah; $i++) {
            $string .= '(';
            for ($j = 0; $j < count($arr[$i]); $j++) {
                $string .= "'".$arr[$i][$j]."'";

                if ($j != (count($arr[$i]) - 1)) {
                    $string .= ', ';
                }
            }
            $string .= ')';

            if ($i != ($jumlah - 1)) {
                $string .= ', ';
            }
        }
    }

    return $string;
}
