<?php  

function create_breadcrumb()
{
	$ci =& get_instance();
	$class = $ci->router->fetch_class();
    $method = $ci->router->fetch_method();

    $menu = $ci->Menu_Model->parent($class, $method);
    $parent = isset($menu->parent) ? $menu->parent : 0;

    $this_parent = $ci->customdb->view_by_id('tb_menu', ['id' => $parent], 'row');
    $parent_label = isset($this_parent->label) ? $this_parent->label : '';

    $separator = '';
    if ($parent > 0) {
    	$separator = 'm-subheader__title--separator';
    }

    
    if ($parent > 0) {
    	$breadcrumb = '<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
    						<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="#" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">
									-
								</li>
								<li class="m-nav__item">
									<a href="" class="m-nav__link">
										<span class="m-nav__link-text">
											'.$parent_label.'
										</span>
									</a>
								</li>
								<li class="m-nav__separator">
									-
								</li>
								<li class="m-nav__item">
									<a href="" class="m-nav__link">
										<span class="m-nav__link-text">
											'.web_title().'
										</span>
									</a>
								</li>
						</ul> ';
    } else {
    	$breadcrumb = '<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title '.$separator.'">
									'.web_title().'
								</h3>';
    }

	$breadcrumb .= '		</div>
						</div>
					</div>';

	return $breadcrumb;
}

?>