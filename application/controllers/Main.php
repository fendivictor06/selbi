<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('frontend/Content_Model');
		$this->load->helper('seo');
	}

	function index()
	{
		$feature_product = $this->feature_product();
		$new_product = $this->product('new');
		$hot_product = $this->product('hot');
		$sale_product = $this->product('sale');
		$banner = $this->banner();

		$data['feature_product'] = $feature_product;
		$data['new_product'] = $new_product;
		$data['hot_product'] = $hot_product;
		$data['sale_product'] = $sale_product;
		$data['banner'] = $banner;

		$this->load->view('frontend/template/header');
		$this->load->view('frontend/main/home', $data);
		$this->load->view('frontend/template/footer');
	}

	function feature_product()
	{
		$result = '';

		$data = $this->Content_Model->view_barang();

		if ($data) {
			foreach ($data as $row) {
				$path = base_url('assets/uploads/product/');

				$image = ($row->image != '') ? $path.$row->image : '';
				$presentase = ($row->rating / 5) * 100;

				$result .= '<div class="product">
		                        <figure class="product-image-container">
		                            <a href="product.html" class="product-image">
		                                <img data-src="'.$image.'" alt="product">
		                            </a>
		                            <a href="'.base_url('quickview/Product/details/'.slug($row->nama).'?id='.$row->id).'" class="btn-quickview">Quick View</a>
		                        </figure>
		                        <div class="product-details">
		                            <div class="ratings-container">
		                                <div class="product-ratings">
		                                    <span class="ratings" style="width:'.$presentase.'%"></span><!-- End .ratings -->
		                                </div><!-- End .product-ratings -->
		                            </div><!-- End .product-container -->
		                            <h2 class="product-title">
		                                <a href="product.html">'.$row->nama.'</a>
		                            </h2>
		                            <div class="price-box">
		                                <span class="product-price">'.format_ribuan($row->harga).'</span>
		                            </div><!-- End .price-box -->

		                            <div class="product-action">
		                                <a href="#" class="paction add-wishlist" title="Add to Wishlist">
		                                    <span>Add to Wishlist</span>
		                                </a>

		                                <a href="product.html" class="paction add-cart" title="Add to Cart">
		                                    <span>Add to Cart</span>
		                                </a>

		                                <a href="#" class="paction add-compare" title="Add to Compare">
		                                    <span>Add to Compare</span>
		                                </a>
		                            </div><!-- End .product-action -->
		                        </div><!-- End .product-details -->
		                    </div><!-- End .product -->';
			}
		}

		return $result;
	}

	function product($params = '')
	{
		$result = '';

		switch ($params) {
			case 'new':
				
				$data = $this->Content_Model->barang_new();

				if ($data) {
					foreach ($data as $row) {
						$path = base_url('assets/uploads/product/');

						$image = ($row->image != '') ? $path.$row->image : '';
						$presentase = ($row->rating / 5) * 100;

						$result .= '<div class="product product-sm">
		                                <figure class="product-image-container">
		                                    <a href="product.html" class="product-image">
		                                        <img data-src="'.$image.'" alt="product">
		                                    </a>
		                                </figure>
		                                <div class="product-details">
		                                    <h2 class="product-title">
		                                        <a href="product.html">'.$row->nama.'</a>
		                                    </h2>
		                                    <div class="ratings-container">
		                                        <div class="product-ratings">
		                                            <span class="ratings" style="width:'.$presentase.'%"></span><!-- End .ratings -->
		                                        </div><!-- End .product-ratings -->
		                                    </div><!-- End .product-container -->
		                                    <div class="price-box">
		                                        <span class="product-price">'.format_ribuan($row->harga).'</span>
		                                    </div><!-- End .price-box -->
		                                </div><!-- End .product-details -->
		                            </div><!-- End .product -->';
					}
				}

				break;

			case 'hot':

				$data = $this->Content_Model->barang_hot();

				if ($data) {
					foreach ($data as $row) {
						$path = base_url('assets/uploads/product/');

						$image = ($row->image != '') ? $path.$row->image : '';
						$presentase = ($row->rating / 5) * 100;

						$result .= '<div class="product product-sm">
		                                <figure class="product-image-container">
		                                    <a href="product.html" class="product-image">
		                                        <img data-src="'.$image.'" alt="product">
		                                    </a>
		                                </figure>
		                                <div class="product-details">
		                                    <h2 class="product-title">
		                                        <a href="product.html">'.$row->nama.'</a>
		                                    </h2>
		                                    <div class="ratings-container">
		                                        <div class="product-ratings">
		                                            <span class="ratings" style="width:'.$presentase.'%"></span><!-- End .ratings -->
		                                        </div><!-- End .product-ratings -->
		                                    </div><!-- End .product-container -->
		                                    <div class="price-box">
		                                        <span class="product-price">'.format_ribuan($row->harga).'</span>
		                                    </div><!-- End .price-box -->
		                                </div><!-- End .product-details -->
		                            </div><!-- End .product -->';
					}
				}

				break;

			case 'sale':
				
				$data = $this->Content_Model->barang_sale();

				if ($data) {
					foreach ($data as $row) {
						$path = base_url('assets/uploads/product/');

						$image = ($row->image != '') ? $path.$row->image : '';
						$presentase = ($row->rating / 5) * 100;

						$result .= '<div class="product product-sm">
		                                <figure class="product-image-container">
		                                    <a href="product.html" class="product-image">
		                                        <img data-src="'.$image.'" alt="product">
		                                    </a>
		                                </figure>
		                                <div class="product-details">
		                                    <h2 class="product-title">
		                                        <a href="product.html">'.$row->nama.'</a>
		                                    </h2>
		                                    <div class="ratings-container">
		                                        <div class="product-ratings">
		                                            <span class="ratings" style="width:'.$presentase.'%"></span><!-- End .ratings -->
		                                        </div><!-- End .product-ratings -->
		                                    </div><!-- End .product-container -->
		                                    <div class="price-box">
		                                        <span class="product-price">'.format_ribuan($row->harga).'</span>
		                                    </div><!-- End .price-box -->
		                                </div><!-- End .product-details -->
		                            </div><!-- End .product -->';
					}
				}

				break;
			
			default:
				$result = '';
				break;
		}

		

		return $result;
	}

	function banner()
	{
		$data = $this->Content_Model->view_banner();

		$result = '';

		if ($data) {
			foreach ($data as $row) {
				$path = base_url('assets/uploads/banner/');

				$result .= '<div class="home-slide">
		                        <div class="owl-lazy slide-bg" data-src="'.$path.$row->gambar.'"></div>
		                        <div class="home-slide-content text-white">
		                        </div><!-- End .home-slide-content -->
		                    </div><!-- End .home-slide -->';
			}
		}

		return $result;
	}
}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */ ?>