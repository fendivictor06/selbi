var _element = '#firebaseui-auth-container';

blockUI();
var uiConfig = {
	signInSuccessUrl: _url,
	signInOptions: [
  		// Leave the lines as is for the providers you want to offer your users.
  		firebase.auth.EmailAuthProvider.PROVIDER_ID,
  		firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  		firebase.auth.FacebookAuthProvider.PROVIDER_ID
	],
	queryParameterForWidgetMode: 'mode',
	callbacks: {
	    signInSuccessWithAuthResult: function(authResult, redirectUrl) {

	    	// console.log(authResult.additionalUserInfo.profile);
	    	// console.log(authResult);

	    	var profile = authResult.additionalUserInfo.profile;
	    	var uid = authResult.user.uid;
	    	var displayName = profile.name;
	    	var email = profile.email;
	    	var phoneNumber = authResult.user.phoneNumber;
	    	var provider = authResult.user.providerData[0].providerId;
	    	var photoURL = profile.picture;

	    	var data = {
                id: uid,
                name: displayName,
                gbr: photoURL,
                email: email,
                phone: phoneNumber,
                via: provider
            };

            console.log(data);

            $.ajax({
                url: url_login,
                type:'POST',
                data: data,
                beforeSend: function() {
                    blockUI();
                },
                success:function(result){
                    $(".login_butt").hide();
                    $("#login_pic").show();
                    $.unblockUI();

                    window.location.assign(base_url);
                },
                error: function(){
                    $.unblockUI();
                }
            });

	    	$.unblockUI();
	     	return true;	
	    },
	    uiShown: function() {
	      	// The widget is rendered.
	      	// Hide the loader.
	      	$.unblockUI();
	    },
	    signInFailure: function(error) {
	    	$.unblockUI();
	  		// $("#btn-login-sosmed").html('Login');
	    }
  	}
};

// Initialize the FirebaseUI Widget using Firebase.
var ui = new firebaseui.auth.AuthUI(firebase.auth());
// The start method will wait until the DOM is loaded.
ui.start(_element, uiConfig);
// ui.start(_elemen_responsive, uiConfig);