initApp = function() {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            // User is signed in.
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var uid = user.uid;
            var phoneNumber = user.phoneNumber;
            var providerData = user.providerData;
            user.getIdToken().then(function(accessToken) {
                // document.getElementById('sign-in-status').textContent = 'Signed in';
                // document.getElementById('sign-in').textContent = 'Sign out';
                // document.getElementById('account-details').textContent = JSON.stringify({
                //     displayName: displayName,
                //     email: email,
                //     emailVerified: emailVerified,
                //     phoneNumber: phoneNumber,
                //     photoURL: photoURL,
                //     uid: uid,
                //     accessToken: accessToken,
                //     providerData: providerData
                // }, null, '  ');

                var opt_data = {
                    id : uid,
                    name : displayName,
                    gbr : photoURL,
                    email : email,
                    phone : phoneNumber,
                    via : providerData[0]['providerId']
                };
                $(".login_butt").hide();
                $("#login_pic").show();
                $.ajax({
                    url:url_login,
                    type:'POST',
                    data: opt_data,
                    beforeSend: function() {
                        blockUI();
                    },
                    success:function(result){
                        $(".login_butt").hide();
                        $("#login_pic").show();
                        $.unblockUI();
                    },
                    error: function(){
                        $.unblockUI();
                    }
                });
            });
			 
			
        } else {
            // User is signed out.
            // document.getElementById('sign-in-status').textContent = 'Signed out';
            // document.getElementById('sign-in').textContent = 'Sign in';
            // document.getElementById('account-details').textContent = 'null';
            // $("#btn-login-sosmed").html('Login');
        }
    }, function(error) {
        console.log(error);
    });
};


window.addEventListener('load', function() {
    initApp();
});