var id = $("#id").val();
var table = $("#tb-data").DataTable({
    responsive:true,
    processing:true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
    	url:base_url+"dashboard/Read/ajax_data_detail_lelang?id="+id,
    	type:"GET"
    }
});

var productImageSlider = $('#product-image-slider');
    productImageSlider.owlCarousel({
        loop: false,
        nav: false,
        dots: false,
        items: 1,
        thumbs: true,
        thumbImage: true,
        thumbsPrerender: true
    });

$('.product-image-single').zoom({
	magnifiy: 10
});