Dropzone.autoDiscover = false;
var startdate = new Date();
$(".date-picker").datepicker({autoclose: true,startDate: startdate});
var jobs_id = $("#jobs_id").val();
    
var drop = new Dropzone("#my-file", {
    url: base_url+"dashboard/Create/ajax_upload_img_temp?jobs_id="+jobs_id,
    maxFilesize: 5,
    method:"POST",
    acceptedFiles:"image/*",
    paramName:"userfile",
    init : function() {
        this.on("sending",function(file, xhr, formData) {
            file.token = Math.random();
            formData.append("token", file.token);
        });

        this.on("success", function (file, respon) {
            console.log(respon);
        });

        // this.on("removedfile", function(file) {
        // });
    },
    // renameFile: function(file) {
    //     let newName = new Date().getTime() + '_' + file.name;
    //     return newName;
    // },
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    removedfile: function(file) {
        var name = file.name;        
            $.ajax({
                type: 'POST',
                url: base_url+'dashboard/Delete/ajax_delete_temp_img',
                data: "id="+name+"&jobs_id="+jobs_id,
                dataType: 'JSON',
                success:function(r) {
                    console.log(r.message);
                },
                error:function(err) {
                    console.log(err);
                }
            });
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
    }
});

$(function(ready){
    $("#id_jenis").change(function() {
        val = this.value;
        if(val != '-') {
            $(".form_title").show('normal');
            if(val == '0') {
                $("#title").text('Tambah Baru');
            } else if(val == '1') {
                $("#title").text('Barang Preloved');
            } else if(val == '2') {
                $("#title").text('Barang Merchandise');
            }
        } else {
            $(".form_title").hide('normal');
        }
            if(val == '0') {
                $(".form_new_product").show('normal');
            } else {
                $(".form_new_product").hide('normal');
            }

            if(val == '1' || val == '2') {
                $(".form_exist").show('normal');
                get_barang(val);
            } else {
                $(".form_exist").hide('normal');
            }
    });    
});

$("#btn-save").click(function() {
    $.ajax({
        url : base_url+"dashboard/Create/ajax_save_lelang",
        type:"POST",
        data:$("#form-data").serialize(),
        dataType:"JSON",
        beforeSend:function() {
            $("#btn-save").attr('disabled',true).html('<i class="fa fa-refresh fa-spin"></i> Menyimpan...');
        },
        complete:function() {
            $("#btn-save").attr('disabled',false).html('<i class="fa fa-refresh"></i> Simpan Data');
        },
        success:function(r) {
            if(r.status == true) {
                $("#alert").html(r.alert);
                swal({ icon: 'success', type: 'success', title: 'Berhasil', text: r.message });
                setInterval(function(){ 
                    window.location.href = base_url+'dashboard/Main/lelang';
                }, 1500);
            } else {
                $("#alert").html(r.alert);
                $("html, body").animate({ scrollTop: $('#alert').offset().top }, 1000);
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
});

function get_barang(val) {
    $.ajax({
        url:base_url+"dashboard/Read/ajax_data_img_lelang?id="+val,
        type:"GET",
        dataType:"JSON",
        beforeSend:function() {
            $("#image-id").html('<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span>');
        },
        success:function(r) {
            $("#image-id").html(r.html);
        },
        error:function(err) {
            console.log(err);
        }
    });
}

function change_img(id) {
    var id_jenis = $("#id_jenis").val();
    $.ajax({
        url:base_url+"dashboard/Read/ajax_img_selected_lelang?id="+id+"&id_jenis="+id_jenis,
        type:"GET",
        dataType:"JSON",
        beforeSend:function() {
            $("#image-id").html('<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span>');
        },
        success:function(r) {
            if(r.status == true) {
                $("#image-id").html(r.html);
            } else {
                $("#alert").html(r.alert);
                $("html, body").animate({ scrollTop: $('#alert').offset().top }, 1000);
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
}