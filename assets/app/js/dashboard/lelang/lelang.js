var startdate = new Date();
$(".date-picker").datepicker({autoclose: true,startDate: startdate});
var table = $("#tb-data").DataTable({
	responsive:true,
	processing:true,
	serverSide:true,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
	ajax : {
		url : base_url+"dashboard/Read/ajax_data_lelang",
		type:"GET"
	}
});

$("#add_new").click(function(){
    window.open(base_url+'dashboard/Main/form_lelang', '_blank');
});

$(document).on('click', '.batal', function(){
    refid = $(this).data('refid');
    swal({
        text: "Apakah anda yakin akan membatalkan data ini ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willUpdate) => {
        if (willUpdate) {
            $.ajax({
                url : base_url+'dashboard/Update/ajax_update_pembatalan_lelang',
                type : 'post',
                data : {
                    'id' : refid
                },
                dataType : 'json',
                success : function(data) {
                    if(data.status == true) {
                        table.ajax.reload(null, false);
                        $("#alerts").html(data.alert);
                    } else {
                        $("#alerts").html(data.alert);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat menghapus data');
                }
            });
        }
    });
});

$(document).on('click', '.approved', function(){
    refid = $(this).data('refid');
    $("#modal-edit").modal('show');
    $("#id").val(refid);
    $.ajax({
        url:base_url+"dashboard/Read/ajax_data_lelang_id",
        type : 'post',
        data : {
            'id' : refid
        },
        dataType:"JSON",
        success:function(r) {
            $("#start").val(r.tgl_mulai);
            $("#end").val(r.tgl_sampai);
            $("#bid_awal").val(r.bid_awal);
        },
        error:function(err) {
            console.log(err);
        }
    });
});

$(document).on('click', '.rejected', function(){
    refid = $(this).data('refid');
    $("#modal-tolak").modal('show');
    $("#id").val(refid);
    $.ajax({
        url:base_url+"dashboard/Read/ajax_data_lelang_id",
        type : 'post',
        data : {
            'id' : refid
        },
        dataType:"JSON",
        success:function(r) {
            $("#keterangan").val(r.keterangan);
        },
        error:function(err) {
            console.log(err);
        }
    });
});

$("#btn-update").click(function() {
    $.ajax({
        url:base_url+"dashboard/Update/ajax_update_data_lelang",
        type:"POST",
        data:$("#form-update").serialize(),
        dataType:"JSON",
        beforeSend:function(){
            $("#btn-update").attr('disabled',true).html('Menyimpan ...');
        },
        complete:function() {
            $("#btn-update").attr('disabled',false).html('Update');
        },
        success:function(r) {
            if(r.status == true) {
                table.ajax.reload(null, false);
                $("#notif").html(r.alert);
                $("#modal-edit").modal('hide');
            } else {
                $("#notif").html(r.alert);
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
});