var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var color = Chart.helpers.color;
var barChartData = {
	labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
	datasets: [{
		label: 'Preloved',
		backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
		borderColor: window.chartColors.red,
		borderWidth: 1,
		data: [
			35,
			30,
			35,
			40,
			45,
			50,
			55
		]
	}, {
		label: 'Merchandise',
		backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
		borderColor: window.chartColors.blue,
		borderWidth: 1,
		data: [
			25,
			30,
			35,
			40,
			45,
			50,
			55
		]
	}]

};

var randomScalingFactor = function() {
	return Math.round(Math.random() * 100);
};

var config = {
	type: 'pie',
	data: {
		datasets: [{
			data: [
				randomScalingFactor(),
				randomScalingFactor(),
				randomScalingFactor(),
				// randomScalingFactor(),
				// randomScalingFactor(),
			],
			backgroundColor: [
				window.chartColors.red,
				window.chartColors.orange,
				window.chartColors.yellow,
				// window.chartColors.green,
				// window.chartColors.blue,
			],
			label: 'Dataset 1'
		}],
		labels: [
			'Whishlist Product',
			'Follower',
			'Product Sold'
		]
	},
	options: {
		responsive: true
	}
};

// window.onload = function() {
// 	var ctx = document.getElementById('chart-area').getContext('2d');
// 	window.myPie = new Chart(ctx, config);
// };

window.onload = function() {
	var ctxx = document.getElementById('chart-area').getContext('2d');
	window.myPie = new Chart(ctxx, config);

	var ctx = document.getElementById('canvas').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: barChartData,
		options: {
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: true,
				text: 'Transaksi Penjualan Produk'
			}
		}
	});
};