var nobukti = $('#nobukti').val();
var table = $("#tb-data").DataTable({
	responsive:true,
	processing:true,
	serverSide:true,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
	ajax : {
		url : base_url+"dashboard/Read/ajax_detail_data_transaksi",
		data : {'nobukti': nobukti},
		type:"POST"
	}
}); 
$("#btn-save").click(function() {
    $.ajax({
        url : base_url+"dashboard/Create/ajax_save_transaksi",
        type:"POST",
        data:$("#form-data").serialize(),
        dataType:"JSON",
        beforeSend:function() {
            $("#btn-save").attr('disabled',true).html('<i class="fa fa-refresh fa-spin"></i> Menyimpan...');
        },
        complete:function() {
            $("#btn-save").attr('disabled',false).html('<i class="fa fa-refresh"></i> Simpan Data');
        },
        success:function(r) {
            if(r.status == true) {
                $("#alert").html(r.alert);
                swal({ icon: 'success', type: 'success', title: 'Berhasil', text: r.message });
                setInterval(function(){ 
                    window.location.href = base_url+'dashboard/Main/transaksi';
                }, 1500);
            } else {
                $("#alert").html(r.alert);
                $("html, body").animate({ scrollTop: $('#alert').offset().top }, 1000);
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
});