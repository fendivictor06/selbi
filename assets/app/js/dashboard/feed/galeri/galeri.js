function loadData() {
    var search = $("#search").val();
    $.ajax({
        url : base_url+"dashboard/Read/ajax_data_gallery?search="+search,
        type:"GET",
        dataType:"JSON",
        beforeSend:function() {
            $("#data-gallery").html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span></center>');
        },
        success:function(r) {
            if(r.status == true) { 
                $("#data-gallery").html(r.html);
            } else {
                toastr.warning('Terjadi kesalahan saat memuat data');
                $("#data-gallery").html('<center>-no data-</center>');
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
} 

$("#search").keydown(function(e) {
    if(e.which == 13) {
        loadData();
    }
});
loadData();

$("#add_new").click(function(){
    window.open(base_url+'dashboard/Main/form_galeri', '_blank');
});

// $(document).on('click', '.update', function(){
//     refid = $(this).data('refid');
//     window.open(base_url+'Main/form_galeri/'+refid, '_blank');
// });

$(document).on('click', '.delete', function(){
    refid = $(this).data('refid');
    swal({
        text: "Apakah anda yakin akan menghapus data ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : base_url+'dashboard/Delete/ajax_delete_gallery',
                type : 'post',
                data : {
                    'id' : refid
                },
                dataType : 'json',
                beforeSend:function() {
                    $("#data-gallery").html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span></center>');
                },
                success : function(data) {
                    if(data.status == 1) {
                        $("#alerts").html(data.alert);
                        loadData()
                    } else {
                        $("#alerts").html(data.alert);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat menghapus data');
                }
            });
        }
    });
});

function show_img(id) {
    $("#modal-gmb").modal('show');
    $.ajax({
        url: base_url+"dashboard/Read/ajax_detail_img_gallery?id="+id,
        type:"GET",
        dataType:"JSON",
        success:function(r) {
            if(r.status == true) {
                $("#detail-img").html(r.html);
            } else {
                $("#detail-img").html("<center> - No data available - </center>");
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
}