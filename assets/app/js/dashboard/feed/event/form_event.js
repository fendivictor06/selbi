$("#btn-save").click(function() {
    $.ajax({
        url : base_url+"dashboard/Create/ajax_save_event",
        type:"POST",
        data:$("#form-data").serialize(),
        dataType:"JSON",
        beforeSend:function() {
            $("#btn-save").attr('disabled',true).html('<i class="fa fa-refresh fa-spin"></i> Menyimpan...');
        },
        complete:function() {
            $("#btn-save").attr('disabled',false).html('<i class="fa fa-refresh"></i> Simpan Data');
        },
        success:function(r) {
            if(r.status == true) {
                $("#alert").html(r.alert);
                swal({ icon: 'success', type: 'success', title: 'Berhasil', text: r.message });
                setInterval(function(){ 
                    window.location.href = base_url+'dashboard/Main/event';
                }, 1500);
            } else {
                $("#alert").html(r.alert);
                $("html, body").animate({ scrollTop: $('#alert').offset().top }, 1000);
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
});