Dropzone.autoDiscover = false;
var jobs_id = $("#jobs_id").val();
    
var drop = new Dropzone("#my-file", {
    url: base_url+"dashboard/Create/ajax_upload_img_temp?jobs_id="+jobs_id,
    maxFilesize: 5,
    method:"POST",
    acceptedFiles:"image/*",
    paramName:"userfile",
    init : function() {
        this.on("sending",function(file, xhr, formData) {
            file.token = Math.random();
            formData.append("token", file.token);
        });

        this.on("success", function (file, respon) {
            console.log(respon);
        });
    },
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    removedfile: function(file) {
        var name = file.name;        
            $.ajax({
                type: 'POST',
                url: base_url+'dashboard/Delete/ajax_delete_temp_img',
                data: "id="+name+"&jobs_id="+jobs_id,
                dataType: 'JSON',
                success:function(r) {
                    console.log(r.message);
                },
                error:function(err) {
                    console.log(err);
                }
            });
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
    }
});

$("#btn-save").click(function(){
	var id = $("#id").val();

	$.ajax({
		url: base_url+'dashboard/Create/upload_merchandise/',
		data: {
			'id': id,
			'token': jobs_id
		},
		dataType: 'json',
		type: 'post',
		beforeSend:function() {
            $("#btn-save").attr('disabled',true).html('<i class="fa fa-refresh fa-spin"></i> Menyimpan...');
        },
        complete:function() {
            $("#btn-save").attr('disabled',false).html('<i class="fa fa-refresh"></i> Simpan Data');
        },
        success: function(data) {
        	if (data.status) {
        		swal({ icon: 'success', type: 'success', title: 'Berhasil', text: data.message, buttons: {
        			Ok: 'OK'
        		} })
        		.then((value) => {
					switch (value) {
					 
					    case "Ok":
					      window.location.href=base_url+'dashboard/Main/data_order_merchandise';
					      break;
					 
					    default:
					      swal("Oops!");
					}
				});
        	} else {
        		swal({ icon: 'error', type: 'error', title: 'Gagal', text: data.message });
        	}
        },
        error: function(error) {
        	console.log(error);
        }
	});
});