var table = $("#tb-data").DataTable({
	responsive:true,
	processing:true,
	serverSide:true,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    order: [[0, 'desc']],
	ajax : {
		url : base_url+"dashboard/Read/ajax_data_order_merchandise",
		type:"GET"
	}
});

$("#add_new").click(function(){
    window.open(base_url+'dashboard/Main/order_merchandise', '_blank');
});

$(document).on('click', '.update', function(){
    refid = $(this).data('refid');
    $("#modal-edit").modal('show');
    $("#id").val(refid);
    $.ajax({
    	url:base_url+"dashboard/Read/ajax_data_order_merchandise_id?id="+refid,
    	type:"GET",
    	dataType:"JSON",
    	success:function(r) {
    		if(r.status == false) {
    			$("#alert").html(r.alert);
    		} else {
                $("#jml").val(r.jumlah);
    			$("#hargajual").val(r.hargajual);
                $("#ket").val(r.keterangan);
    			$("#attr").html(r.attr_html);
    		}
    	},
    	error:function(err) {
    		console.log(err);
    	}

    });
});

$(document).on('click', '#btn-update', function() {
	// id  = $("#id").val();
	// ket = $("#ket").val();
 //    jml = $("#jml").val();
 //    ukuran = $('input[name=ukuran]:checked').val();
 //    warna  = $('input[name=warna]:checked').val();
    $.ajax({
        // url:base_url+"dashboard/Update/ajax_update_data_order_merchandise?id="+id+"&ket="+ket+"&jml="+jml+"&ukuran="+ukuran+"&warna="+warna,
    	url:base_url+"dashboard/Update/ajax_update_data_order_merchandise",
    	type:"POST",
        data:$("#form-update").serialize(),
    	dataType:"JSON",
    	beforeSend:function() {
    		$("#btn-update").attr('disabled',true).html('Menyimpan ...');
    	},
    	complete:function() {
    		$("#btn-update").attr('disabled',false).html('Update');
    	},
    	success:function(r) {
    		if(r.status == true) {
    			swal({ icon: 'success', type: 'success', title: 'Berhasil', text: r.message });
    			$("#modal-edit").modal('hide');
    			table.ajax.reload();
    		} else {
    			$("#notif").html(r.alert);
    		}
    	},
    	error:function(err) {
    		console.log(err);
    	}

    });
});

$(document).on('click', '.cancel', function(){
    refid = $(this).data('refid');
    swal({
        text: "Apakah anda yakin akan membatalkan data orderan ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((action) => {
        if (action) {
            $.ajax({
                url : base_url+'dashboard/Update/ajax_pembatalan_order_merchandise?id='+refid,
                type : 'get',
                dataType : 'json',
                success : function(data) {
                    if(data.status == true) {
                       	swal({ icon: 'success', type: 'success', title: 'Berhasil', text: data.message });
                        table.ajax.reload();
                    } else {
                        $("#alerts").html(data.alert);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat mengupdate data');
                }
            });
        }
    });
});