function loadData() {
    $.ajax({
        url : base_url+"dashboard/Read/ajax_data_list_order_merchandise",
        type:"GET",
        dataType:"JSON",
        beforeSend:function() {
            $("#order-list").html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span></center>');
        },
        success:function(r) {
            if(r.status == true) {
                $("#order-list").html(r.html);
            } else {
                toastr.warning('Terjadi kesalahan saat memuat data');
                $("#order-list").html('<center>-No data available-</center>');
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
} loadData();