var table = $("#tb-data").DataTable({
	responsive:true,
	processing:true,
	// serverSide:true,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },     
	bLengthChange: false,
	bFilter: false,
	paging: false
    // ,
	// ajax : {
	// 	url : base_url+"dashboard/Read/ajax_data_order_merchandise",
	// 	type:"GET"
	// }
});

$('a[data-imagelightbox="top"]').imageLightbox({
    activity: true,
    arrows: true,
    navigation: true,
    button: true,
    overlay: true
});

$(document).on('click', '.accept', function(){
    refid = $(this).data('refid');
    swal({
        title: "Apakah anda yakin akan menyetujui penawaran ini ?",
        text: "Jika anda menyetujui penawaran ini, data merchandise anda akan langsung terbit menjadi data barang anda ketika klik Ok.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((action) => {
        if (action) {
            $.ajax({
                url : base_url+'dashboard/Update/ajax_terima_penawaran_merchandise',
                type : 'get',
                data : {
                    'id':refid,
                    'id_order':$("#id").val()
                },
                dataType : 'json',
                beforeSend:function() {
                    $(".accept").attr('disabled',true).html('Memproses ...');
                },
                complete:function() {
                    $(".accept").attr('disabled',false).html('<i class="fa fa-check-circle-o"></i> Terima');
                },
                success : function(data) {
                    if(data.status == true) {
                       	swal({ icon: 'success', type: 'success', title: 'Berhasil', text: data.message });
                        setInterval(function(){ 
                            window.location.href = base_url+"dashboard/Main/data_order_merchandise";
                        }, 1500);
                    } else {
                        $("#alerts").html(data.alert);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat mengupdate data');
                }
            });
        }
    });
});

$(document).on('click', '.reject', function(){
    refid = $(this).data('refid');
    swal({
        title: "Apakah anda yakin akan menolak penawaran ini ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((action) => {
        if (action) {
            $.ajax({
                url : base_url+'dashboard/Update/ajax_tolak_penawaran_merchandise?id='+refid,
                type : 'get',
                dataType : 'json',
                success : function(data) {
                    if(data.status == true) {
                       	swal({ icon: 'success', type: 'success', title: 'Berhasil', text: data.message });
                        setInterval(function(){ 
                            window.location.reload(true);
                        }, 1500);
                    } else {
                        $("#alerts").html(data.alert);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat mengupdate data');
                }
            });
        }
    });
});

$(document).on('click', '.get-detail', function() {
    refid = $(this).data('refid');
    $.ajax({
        url : base_url+'dashboard/Read/ajax_detail_image_penawaran?id='+refid,
        type : 'get',
        dataType : 'json',
        beforeSend:function() {
            $("#img-detail-"+refid).html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span></center>');
        },
        success : function(data) {
            if(data.status == true) {
                var arr = data.html;
                var html= '<div class="row on-top-10" style="background: #fcfcfc;padding: 8px;">';
                for (var i =0; i < arr.length; i++) {
                    html += '<div class="col-md-3" style="padding-left:0px;"><a style="margin-bottom: -10px;" href="'+arr[i].url+'" data-imagelightbox="bottom-'+refid+'">';
                    html += '<img class="img-thumbnail" src="'+arr[i].url+'"/>';
                    html += '</a></div>';
                }
                html += '</div>';
                $("#img-detail-"+refid).html(html);
                $('a[data-imagelightbox="bottom-'+refid+'"]').imageLightbox({ activity: true, arrows: true, navigation: true, button: true,
                    overlay: true });
            } else {
                $("#img-detail-"+refid).html('- No image available -');
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat mengupdate data');
        }
    });
});

// $(document).on('click', '#cancel', function(){
//     id = $("#id").val();
//     swal({
//         title: "Apakah anda yakin akan membatalkan data orderan ?",
//         icon: "warning",
//         buttons: true,
//         dangerMode: true,
//     })
//     .then((action) => {
//         if (action) {
//             $.ajax({
//                 url : base_url+'dashboard/Update/ajax_pembatalan_order_merchandise?id='+id,
//                 type : 'get',
//                 dataType : 'json',
//                 success : function(data) {
//                     if(data.status == true) {
//                        	swal({ icon: 'success', type: 'success', title: 'Berhasil', text: data.message });
//                         window.location.href = base_url+"dashboard/Main/data_order_merchandise";
//                     } else {
//                         $("#alerts").html(data.alert);
//                     }
//                 },
//                 error : function() {
//                     toastr.warning('Terjadi kesalahan saat mengupdate data');
//                 }
//             });
//         }
//     });
// });