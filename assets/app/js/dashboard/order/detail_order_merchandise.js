var productImageSlider = $('#product-image-slider');
    productImageSlider.owlCarousel({
        loop: false,
        nav: false,
        dots: false,
        items: 1,
        thumbs: true,
        thumbImage: true,
        thumbsPrerender: true
    });

$('.product-image-single').zoom({
	magnifiy: 10
});

$('a[data-imagelightbox="a"]').imageLightbox({
    activity: true,
    arrows: true,
    navigation: true,
    button: true,
    overlay: true
});

$("#btn-order").click(function() {
    // var id = $("#id").val();
    // ukuran = $('input[name=ukuran]:checked').val();
    // warna  = $('input[name=warna]:checked').val();
    $.ajax({
        // url:base_url+"dashboard/Create/ajax_save_order?id="+id+"&jml="+$('#qty').val()+"&ket="+$('#catatan').val()+"&ukuran="+ukuran+"&warna="+warna,
        url:base_url+"dashboard/Create/ajax_save_order",
        type:"POST",
        data:$("#form-data").serialize(),
        dataType:"JSON",
        beforeSend:function() {
            $(this).attr('disabled',true).html('Menyimpan ...');
        },
        complete:function() {
            $(this).attr('disabled',false).html('<i class="fa fa-shopping-cart"></i> Order');
        },
        success:function(r) {
            if(r.status == true) {
                swal({ icon: 'success', type: 'success', title: 'Berhasil', text: r.message });
                setInterval(function(){ 
                    window.location.href = base_url+'dashboard/Main/data_order_merchandise';
                }, 1500);
            } else {
                $("#alert").html(r.alert);
                $("html, body").animate({ scrollTop: $('#alert').offset().top }, 1000);
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
});

$("#btn-catatan").click(function() {
    $(".form-note").toggle('slow');
});