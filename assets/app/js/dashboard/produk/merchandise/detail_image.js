Dropzone.autoDiscover = false;
var jobs_id = $("#jobs_id").val();

var drop = new Dropzone("#my-file", {
    url: base_url+"dashboard/Create/ajax_upload_img_temp?jobs_id="+jobs_id,
    maxFilesize: 5,
    method:"POST",
    acceptedFiles:"image/*",
    paramName:"userfile",
    init : function() {
        this.on("sending",function(file, xhr, formData) {
            file.token = Math.random();
            formData.append("token", file.token);
        });

        this.on("success", function (file, respon) {
            console.log(respon);
        });

        // this.on("removedfile", function(file) {
        // });
    },
    // renameFile: function(file) {
    //     let newName = new Date().getTime() + '_' + file.name;
    //     return newName;
    // },
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    removedfile: function(file) {
        var name = file.name;        
            $.ajax({
                type: 'POST',
                url: base_url+'dashboard/Delete/ajax_delete_temp_img',
                data: "id="+name+"&jobs_id="+jobs_id,
                dataType: 'JSON',
                success:function(r) {
                    console.log(r.message);
                },
                error:function(err) {
                    console.log(err);
                }
            });
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
    }
});

function reload_img() {
    var id = $("#id").val();
    $.ajax({
        url:base_url+"dashboard/Read/ajax_data_image_merchandise/"+id,
        dataType:"JSON",
        beforeSend:function(){
            $("#form-images").html('<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span>');
        },
        success:function(r){
            $("#form-images").html(r.html);
            $('a[data-imagelightbox="a"]').imageLightbox({
                activity: true,
                arrows: true,
                navigation: true,
                button: true,
                overlay: true
            });
        },
        error:function(err){
            console.log(err);
        }
    });
}

$(document).ready(function() {
    $('#form-images').slimscroll({
        height: '400px'
    });
    reload_img();
});

function delete_img(id) {
    swal({
        text: "Apakah anda yakin akan menghapus gambar ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url:base_url+"dashboard/Delete/ajax_delete_img_merchandise/?id="+id,
                type:"GET",
                dataType:"JSON",
                success:function(r) {
                    if(r.status == true) {
                        reload_img();
                        swal({ icon: 'success', type: 'success', title: 'Berhasil', text: r.message });
                    } else {
                        swal({ icon: 'error', type: 'error', title: 'Gagal', text: r.message });
                    }
                },
                error:function(err) {
                    console.log(err);
                }
            });            
        }
    });
}

$("#btn-save").click(function() {
    var id = $("#id").val();
    $.ajax({
        url : base_url+"dashboard/Create/ajax_save_img_merchandise?id="+id+"&jobs_id="+jobs_id,
        type:"GET",
        dataType:"JSON",
        beforeSend:function() {
            $("#btn-save").attr('disabled',true).html('<i class="fa fa-refresh fa-spin"></i> Menyimpan...');
        },
        complete:function() {
            $("#btn-save").attr('disabled',false).html('<i class="fa fa-refresh"></i> Simpan Data');
        },
        success:function(r) {
            if(r.status == true) {
                swal({ icon: 'success', type: 'success', title: 'Berhasil', text: r.message });
                Dropzone.forElement('#my-file').removeAllFiles(true)
                reload_img();
            } else {
                $("#alert").html(r.alert);
                $("html, body").animate({ scrollTop: $('#alert').offset().top }, 1000);
            }
        },
        error:function(err) {
            console.log(err);
        }
    });
});