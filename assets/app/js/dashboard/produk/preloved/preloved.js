var table = $("#tb-data").DataTable({
	responsive:true,
	processing:true,
	serverSide:true,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
	ajax : {
		url : base_url+"dashboard/Read/ajax_data_preloved",
		type:"GET"
	}
});
 
$("#add_new").click(function(){
    window.open(base_url+'dashboard/Main/form_preloved', '_blank');
});

$(document).on('click', '.update', function(){
    refid = $(this).data('refid');
    window.open(base_url+'dashboard/Main/form_preloved/'+refid, '_blank');
});

$(document).on('click', '.delete', function(){
    refid = $(this).data('refid');
    swal({
        text: "Apakah anda yakin akan menghapus data ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : base_url+'dashboard/Delete/ajax_delete_preloved',
                type : 'post',
                data : {
                    'id' : refid
                },
                dataType : 'json',
                success : function(data) {
                    if(data.status == 1) {
                        table.ajax.reload(null, false);
                        $("#alerts").html(data.alert);
                    } else {
                        $("#alerts").html(data.alert);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat menghapus data');
                }
            });
        }
    });
});