$(document).ready(function(){
	loadArtis();
	$.ajax({
	    url: 'https://newsapi.org/v2/top-headlines?country=id&category=entertainment&apiKey=191e9b3deb504d39adcbdc45d769bb46&pagesize=6',
	    type: 'get',
	    dataType: 'json',
	    async :false,
	    success: function(data){
				jQuery.each( data['articles'], function(i, val ) {
					var str = val.description;
					if(val.description != null){
						var str_substring=str.substring(0, 80);
					}else {
						var str_substring='NULL';
					}
	                i += 1;
	                if(i <=3 ){
		                $('#b_pertama').append(
		                	'<div class="d-block d-md-flex listing">\
								<a href="'+val.url+'" target="_blank" class="img d-block" style="background-image: url('+val.urlToImage+')"></a>\
								<div class="lh-content-siap">\
                					<h3><a href="'+val.url+'" target="_blank">'+val.title+'</a></h3>\
                					<p>'+str_substring+'...</p>\
                  					<p class="mb-0">Author : '+val.author+'<span></span></p>\
                 				</div>\
		 					</div>'
		                );

	                }else{
		                $('#b_kedua').append(
		                	'<div class="d-block d-md-flex listing">\
								<a href="'+val.url+'" target="_blank" class="img d-block" style="background-image: url('+val.urlToImage+')"></a>\
								<div class="lh-content-siap">\
                					<h3><a href="'+val.url+'" target="_blank">'+val.title+'</a></h3>\
                					<p>'+str_substring+'...</p>\
                  					<p class="mb-0">Author : '+val.author+'<span></span></p>\
                 				</div>\
		 					</div>'
		                );
	                }
	            });
	      },
	});
	$(".public-figure-search").click(function(){
		let id = $(this).data('refid');

		loadArtis(id);
		// loadArtisArrival();
	});

	
	function loadArtis(id) {
		$.ajax({
			url: base_url+'Main/public_figure',
			method: 'get',
			dataType: 'json',
			data: {
				'id': id
			},
			success: function(data) {
				html = '';
				html += '<div class="row-fluid">';
				if (data.length > 0) {
					for(i = 0; i < data.length; i++) {
						html += '<div class="col-xs-6 col-sm-6 col-md-3" style="margin-bottom:10px;padding-left: 5px;padding-right: 5px;">\
									<div class="hovereffect">\
										<a href="'+base_url+'Main/profile/'+data[i].id+'">\
											<img class="img-responsive" style="height: 250px; width: 100%; object-fit: cover; object-position: 50% 10%;" src="'+data[i].image+'" alt="">\
											<div class="overlay">\
												<h2>'+data[i].nama+'</h2>\
											</div>\
										</a>\
									</div>\
				 				</div>';
					}
				} 
				html += '</div>';

				$("#public-figure-result").html(html);
			},
			error: function() {

			}
		});
	}
	
});