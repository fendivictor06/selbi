var ajax_load_item = function(url, method, data){
	var result;
	$.ajax({
		url : url,
		type : method, 
		data : data,
		async : false,
		dataType : 'json',
		success : function(data) {
			result = data;
		}, 
		error : function() {
			toastr.warning('', 'Terjadi kesalahan saat memuat data');
		}
	});

	return result;
};

var ajax_delete_item = function(title, url, method, data, datatype, load_items){
	swal({
	  	text: title,
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : url,
	  			type : method,
	  			data : data,
	  			async : false,
	  			dataType : datatype,
	  			success : function(data) {
	  				if(data.status == 1) {
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }

		            load_items.ajax.reload(null, false);
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat mengirim data');
	  			}
	  		});
	  	}
	});
}

var ajax_save = function(url, method, data) {
	var result;
	$.ajax({
        url : url,
        type : method,
        data : data,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_modal .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_modal .modal-content');        
        },
        success : function(data) {
        	result = data;
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
    });

    return result;
}

var ajax_select2 = function(url, element, placeholder) {
	$(element).select2({
		placeholder: placeholder,
		allowClear: true,
		ajax: {
			url: url,
			dataType: 'json',
			delay: 250,
			data: function(params){
				return {
					q: params.term
				}
			},
			processResults: function(data) {
				var results = [];
				$.each(data, function(i, val){
					results.push({
						id: val.id,
						text: val.text
					});
				});

				return {
					results : results
				}
			}
		}
	});
}

var ajax_save_page = function(url, method, data) {
	var result;
	$.ajax({
        url : url,
        type : method,
        data : data,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('.m-portlet', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('.m-portlet');        
        },
        success : function(data) {
        	result = data;
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
    });

    return result;
}

var ajax_custom_delete = function(title, url, method, data, datatype){
	swal({
	  	text: title,
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : url,
	  			type : method,
	  			data : data,
	  			async : false,
	  			dataType : datatype,
	  			success : function(data) {
	  				if(data.status == 1) {
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat mengirim data');
	  			}
	  		});
	  	}
	});
}


var ajax_load_html = function(url, method, data){
	var result;
	$.ajax({
		url : url,
		type : method, 
		data : data,
		async : false,
		success : function(data) {
			result = data;
		}, 
		error : function() {
			toastr.warning('', 'Terjadi kesalahan saat memuat data');
		}
	});

	return result;
};