function loadResult(start, count, keyword) {
    $.ajax({
        url : base_url+'backend/Gallery/data_gallery?start='+start+'&count='+count+"&keyword="+keyword, 
        beforeSend : function() {
            mApp.block('#gallery-view', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function() {
            mApp.unblock('#gallery-view');
        },
        success : function(data) {
            $("#gallery-view").append(data);
        },
        error : function() {
            toastr.warning('', 'Terjadi kesalahan saat memuat data');
        }
    });
}

const loadImageGallery = (refid) => {
    $.ajax({
        url : base_url+'backend/Gallery/gallery_id/'+refid,
        beforeSend : function(){
            mApp.block('#details-feed', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#details-feed');
        },
        success : function(data) {
            $("#details-feed").html('');
            $("#details-feed").html(data);
            
        },
        error : function() {
            toastr.warning('', 'Terjadi kesalahan saat memuat data');
        }
    });
}

$(document).on('click', '.details', function(){
    refid = $(this).data('refid');
    $("#m_modal").modal();

    loadImageGallery(refid);
});

$("#search-btn").click(function(){
    var start = 0;
    var count = 6;
    var keyword = $("#search").val();
    $("#gallery-view").html('');
    loadResult(start, count, keyword);
});

$(document).ready(function(){
    var start = 0;
    var count = 6;
    var keyword = $("#search").val();

    loadResult(start, count, keyword);

    $(window).scroll(function(){
        if(($(window).scrollTop() == $(document).height() - $(window).height())){

            start = parseFloat(start + 6);

            loadResult(start, count, keyword);
        }
    });
});

$(document).on('click', '.delete', function(){
    let refid = $(this).data('refid');
    let url = base_url+'backend/Gallery/delete_gallery/';
    let data = {
        'id' : refid
    }

    swal({
        text: 'Apakah anda yakin akan menghapus data?',
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : url,
                type : 'post',
                data : data,
                async : false,
                dataType : 'json',
                success : function(data) {
                    if(data.status == 1) {
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                    }

                    $("#gallery-view").html('');
                    let keyword = $("#search").val();
                    loadResult(0, 6, keyword);

                    start = 6;
                    count = 6;
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat menghapus data');
                }
            });
        }
    });
});

$(document).on('click', '.details-delete', function(){
    let refid = $(this).data('refid');
    let url = base_url+'backend/Gallery/delete_image/';
    let data = {
        'id' : refid
    }

    swal({
        text: 'Apakah anda yakin akan menghapus data?',
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : url,
                type : 'post',
                data : data,
                async : false,
                dataType : 'json',
                success : function(data) {
                    if(data.status == 1) {
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                    }

                    loadImageGallery(data.id_feed);
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat menghapus data');
                }
            });
        }
    });
});