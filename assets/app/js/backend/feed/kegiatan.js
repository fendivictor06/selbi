var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Kegiatan/data_kegiatan',
        type: "GET"
    },
    columnDefs : [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 6 }
    ],
    aaSorting: [[3, 'desc']]
});

$(document).on('click', '.delete', function(){
    var refid = $(this).data('refid');
    var url = base_url+'backend/Kegiatan/delete_kegiatan/';
    var data = {
        'id' : refid
    }

    var delete_items = ajax_delete_item('Yakin ingin menghapus data ini ?', url, 'post', data, 'json', table);
});