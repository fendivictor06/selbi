var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Slider/data_slider',
        type: "GET"
    }
});

$(document).on('click', '.delete', function(){
    var refid = $(this).data('refid');
    var url = base_url+'backend/Slider/delete_slider/';
    var data = {
        'id' : refid
    }

    var delete_items = ajax_delete_item('Yakin ingin menghapus data ini ?', url, 'post', data, 'json', table);
});

$("#form-data").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    var url = base_url+'backend/Slider/add_slider/';
    var id = $("#id").val();

    var simpan = ajax_save_page(url, 'post', formData);   

    if(simpan.status == 1) {
        toastr.success(simpan.message);
        clear_form();
        $("#m_modal").modal('toggle');
        table.ajax.reload(null, false);
    } else {
        toastr.warning(simpan.message);
    } 

    return false;
});

$("#add_new").click(function(){
	clear_form();
	$("#m_modal").modal();
});

var clear_form = function(){
	$("#id").val("");
	document.getElementById('form-data').reset();
}