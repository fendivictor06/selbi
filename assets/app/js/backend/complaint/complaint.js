var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Complaint/data_complaint',
        type: "GET"
    },
    columnDefs : [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 5 },
        { "orderable": false, "targets": 6 }
    ],
    aaSorting: [[4, 'desc']]
});