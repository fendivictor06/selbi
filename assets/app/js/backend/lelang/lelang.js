var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Lelang/data_lelang',
        type: "GET"
    },
    columnDefs : [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 6 }
    ],
    aaSorting: [[5, 'desc']]
});

$(document).on('click', '.details', function(){
    var id = $(this).data('refid');
    var items = ajax_load_item(base_url+'backend/Lelang/detail_lelang', 'post', {'id' : id});
    $("#m_details").modal();
    $("#nama-barang").html(items.nama);
    $("#penjual-barang").html(items.penjual);
    $("#category-barang").html(items.category);
    $("#brand-barang").html(items.brand);
    $("#jenis-barang").html(items.jenis);
    $("#kondisi-barang").html(items.kondisi);
    $("#berat-barang").html(items.berat+' '+items.berat_satuan);
    $("#deskripsi-barang").html(items.deskripsi);
    $("#status-barang").html(items.status);
    $("#waktu-barang").html(items.waktu);
    $("#id").val(id);
    $("#keterangan").val("");
    $('[name="approve"]').prop('checked', false);

    images = items.images;
    tmp_img = '';
    if (images.length > 0) {
        tmp_img += '<div class="row">';
        for (i = 0; i < images.length; i++) {
            tmp_img += '<div class="col-md-4">';
            tmp_img += '<div class="card">';
            tmp_img += '<img class="card-img-top hover-image" src="'+images[i]+'" alt="'+items.nama+'">';
            tmp_img += '</div>';
            tmp_img += '</div>';
        }
        tmp_img += '</div>';
    }

    $("#images-barang").html(tmp_img);
});

$("#form-data").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    swal({
        text: "Yakin akan memverifikasi Lelang ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            save = ajax_save(base_url+'backend/Lelang/approve', 'post', formData);
            if (save.status == 1) {
                toastr.success(save.message);

                table.ajax.reload(null, false);
                $("#m_details").modal('toggle');
            } else {
                toastr.warning(save.message);
            }
        }
    });

    return false;
});

$(document).on('click', '.approve', function(){
    
});