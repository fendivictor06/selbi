var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/User_Management/data_user',
        type: "GET"
    },
    columnDefs : [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 5 }
    ],
    aaSorting: [[1, 'asc']]
});

function clear_form() {
	$("#id").val("");
	document.getElementById('form-data').reset();
	$("#username").attr('readonly', false);
}

function init_level() {
	var level = $("#level").val();

	if (level == 1) {
		$("#form-perusahaan").css('display', 'none');
		$("#form-karyawan").css('display', 'none');
		$("#form-profile").css('display', 'block');
	} else if (level == 2) {
		$("#form-perusahaan").css('display', 'block');
		$("#form-karyawan").css('display', 'none');
		$("#form-profile").css('display', 'block');
	} else {
		$("#form-perusahaan").css('display', 'block');
		$("#form-karyawan").css('display', 'block');
		$("#form-profile").css('display', 'none');
	}
}

$("#add_new").click(function(){
	clear_form();
	$("#m_modal").modal('show');
	init_level();
});

$("#m_modal").on('show.bs.modal', function() {
	$('#level').select2({
	    placeholder: "Pilih Level"
	});
});

$("#level").change(function(){
	init_level();
});

$("#form-data").submit(function(event) {
	event.preventDefault();
	formData = new FormData($(this)[0]);
	$.ajax({
		url : base_url+"backend/User_Management/add_user",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_modal .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_modal .modal-content');        
        },
        success : function(data) {
            if(data.status == 1) {
                table.ajax.reload(null, false);
                clear_form();
                toastr.success(data.message);
                $("#m_modal").modal('toggle');
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
	});

	return false;
});

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	$.ajax({
		url : base_url+"backend/User_Management/user_id",
		type : "post",
		data : {
			'id' : refid
		},
		dataType : "json",
		success : function(data) {
			$("#m_modal").modal('show');
			$("#id").val(data.id);
			$("#level").val(data.level).trigger('change');
			init_level();
			$("#username").val(data.username);
			$("#username").attr('readonly', true);
			$("#profile_name").val(data.profile_name);
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat memuat data');
		}
	});
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'backend/User_Management/hapus_user',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});
