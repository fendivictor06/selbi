var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/User/ajax_data_user',
        type: "GET"
    },
    columnDefs : [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 7 }
    ],
    aaSorting: [[6, 'asc']]
});

$(document).on('click', '.banned', function(){
    var refid = $(this).data('refid');
    var url = base_url+'backend/User/ban_user/';
    var data = {
        'refid' : refid
    }

    var ban_items = ajax_delete_item('Yakin ingin banned user ini ?', url, 'post', data, 'json', table);
});

$(document).on('click', '.update', function(){
    $("#m_modal").modal();
    refid = $(this).data('refid');
    $("#id").val(refid);
    $("#provinsi").val('').trigger('change');

    var load_items = ajax_load_item(base_url+'backend/User/id_user', 'post', {'id' : refid});
    $("#nama").val(load_items.profile_name);
});

$(document).ready(function(){
    $("#provinsi").select2({
        placeholder : 'Pilih Provinsi'
    });
    $("#kota").select2({
        placeholder : 'Pilih Kota'
    });
    $("#pekerjaan").select2();
});

$("#provinsi").change(function(){
    var provinsi = $(this).val();
    $("#kota").val("").trigger("change");
    $("#kota").select2({
        ajax : {
            url : base_url+"backend/master/ajax_kota",
            dataType : "json",
            type : "post",
            delay : 250,
            data : function(params) {
                return {
                    search : params.term,
                    provinsi : provinsi
                }
            },
            processResults : function(data) {
                var results = [];
                $.each(data, function(index, item) {
                    results.push({
                        id : item.id, 
                        text : item.kota
                    });
                });

                return {
                    results : results
                };
            }
        },
        placeholder : "Pilih Kota"
    });
});

$("#form-data").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    formData.append('update', 0);
    var url = base_url+'backend/User/add_selebriti/';

    var simpan = ajax_save(url, 'post', formData);   

    if(simpan.status == 1) {
        table.ajax.reload(null, false);
        toastr.success(simpan.message);
        $("#m_modal").modal('toggle');
        document.getElementById('form-data').reset();
        $("#id").val("");
    } else {
        toastr.warning(simpan.message);
    }

    return false;
});