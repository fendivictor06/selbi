var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Master/data_kota',
        type: "GET"
    }
});

function clear_form() {
	$("#id").val("");
	$("#provinsi").val("").trigger("change");
	document.getElementById('form-data').reset();
} 

$("#add_new").click(function(){
	clear_form();
	$("#m_modal").modal('show');
});

$("#m_modal").on('show.bs.modal', function() {
	$('#provinsi').select2({
	    placeholder: "Pilih Provinsi"
	});
});

$(document).ready(function() {
$('#submit').click(function(){
    $("#submit").addClass("loading");
	$.ajax({
		url : base_url+"backend/Master/getCity",
		type : "get",
		async : true,
		cache : false,
		success : function(data){
	      $(".submit").removeClass("loading");
	      $(".submit").removeClass("hide-loading");
	      $(".done").removeClass("finish");
	      $(".failed").removeClass("finish");
	       $('#tb-data').DataTable().ajax.reload();
		},
	  error: function(){
	      $(".submit").addClass("hide-loading");
	      // For failed icon just replace ".done" with ".failed"
	      $(".done").addClass("finish");
	  }
	})
})
});
$("#form-data").submit(function(event) {
	event.preventDefault();
	formData = new FormData($(this)[0]);
	$.ajax({
		url : base_url+"backend/Master/add_kota",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_modal .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_modal .modal-content');        
        },
        success : function(data) {
            if(data.status == 1) {
                table.ajax.reload(null, false);
                clear_form();
                toastr.success(data.message);
                $("#m_modal").modal('toggle');
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
	});

	return false;
});

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	$.ajax({
		url : base_url+"backend/Master/id_kota",
		type : "post",
		data : {
			'id' : refid
		},
		dataType : "json",
		success : function(data) {
			$("#m_modal").modal('show');
			$("#id").val(data.id);
			$("#provinsi").val(data.id_provinsi).trigger('change');
			$("#kota").val(data.kota)
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat memuat data');
		}
	});
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'backend/Master/hapus_kota',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});