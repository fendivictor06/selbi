var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Master/data_berita',
        type: "GET"
    } 
}); 
setInterval(function(){refreshBerita();}, 90000);
// console.log(setInterval(location.reload(), 900));
$(document).ready(function() {
$('#submit').click(function(){
    $("#submit").addClass("loading");
    $.ajax({
        url : base_url+"backend/Master/refreshBerita",
        type : "get",
        async : true,
        cache : false,
        success : function(data){
          $(".submit").removeClass("loading");
          $(".submit").removeClass("hide-loading");
          $(".done").removeClass("finish");
          $(".failed").removeClass("finish");
           $('#tb-data').DataTable().ajax.reload();
        },
      error: function(){
          $(".submit").addClass("hide-loading");
          // For failed icon just replace ".done" with ".failed"
          $(".done").addClass("finish");
      }
    })
});
});

function refreshBerita(){
    $.ajax({
        url : base_url+"backend/Master/refreshBerita",
        type : "get",
        async : true,
        cache : false,
        success : function(data){
           $('#tb-data').DataTable().ajax.reload();
        },
      error: function(){
      }
    })
}