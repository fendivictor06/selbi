var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Master/data_brand',
        type: "GET"
    }
});

$(document).on('click', '.delete', function(){
    var refid = $(this).data('refid');
    var url = base_url+'backend/Master/delete_brand/';
    var data = {
        'id' : refid
    }

    var delete_items = ajax_delete_item('Yakin ingin menghapus data ini ?', url, 'post', data, 'json', table);
});

$(document).on('click', '.update', function(){
    var refid = $(this).data('refid');
    var data = ajax_load_item(base_url+'backend/Master/id_brand', 'post', {'id' : refid});
    $("#m_modal").modal();
    $("#id").val(data.id);
    $("#brand").val(data.brand);
});

$("#add_new").click(function(){
	clear_form();
	$("#m_modal").modal();
});

var clear_form = function(){
	$("#id").val("");
	document.getElementById('form-data').reset();
}

$("#form-data").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    var url = base_url+'backend/Master/add_brand/';
    var id = $("#id").val();

    var simpan = ajax_save_page(url, 'post', formData);   

    if(simpan.status == 1) {
        toastr.success(simpan.message);
        clear_form();
        $("#m_modal").modal('toggle');
        table.ajax.reload(null, false);
    } else {
        toastr.warning(simpan.message);
    }

    return false;
});