var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Hot_News/data_news',
        type: "GET"
    },
    columnDefs : [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 4, "width" : "80px" },
        { "orderable": false, "targets": 2 }
    ],
    aaSorting: [[3, 'desc']]
});

$("#add_new").click(function(){
	window.location.href = base_url+"backend/Hot_News/form_news";
});	

$(document).on('click', '.update', function(){
	var refid = $(this).data('refid');
    window.open(base_url+'backend/Hot_News/form_news/'+refid);
});

$(document).on('click', '.delete', function(){
    var refid = $(this).data('refid');
    var url = base_url+'backend/Hot_News/delete_news/';
    var data = {
        'id' : refid
    }

    var delete_items = ajax_delete_item('Yakin ingin menghapus data ini ?', url, 'post', data, 'json', table);
});

$(document).on('click', '.view-news', function(){
	var refid = $(this).data('refid');
	window.open(base_url+'backend/Hot_News/view_news/'+refid);
});