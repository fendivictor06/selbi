$(document).ready(function(){
    $('#deskripsi').summernote({
        height: 400, 
    });

    if (id != '') {
	    var data = ajax_load_item(base_url+'backend/Hot_News/id_news', 'post', {'id' : id});

	    $("#id").val(data.id);
	    $("#judul").val(data.judul);
	    $("#deskripsi").summernote('code', data.teks);
	}
});

$("#form-data").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    formData.append('teks', $('#deskripsi').summernote('code'));

    var url = base_url+'backend/Hot_News/add_news/';
    var id = $("#id").val();

    var simpan = ajax_save_page(url, 'post', formData);   

    if(simpan.status == 1) {
        toastr.success(simpan.message);
    } else {
        toastr.warning(simpan.message);
    }

    return false;
});

$("#clearform").click(function() {
	document.getElementById('form-data').reset();
	$("#deskripsi").summernote('reset');
});