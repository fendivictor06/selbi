var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Merchandise/data_merchandise',
        type: "GET"
    },
    columnDefs : [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 6 }
    ],
    aaSorting: [[1, 'desc']]
});

$(document).on('click', '.delete', function(){
    var refid = $(this).data('refid');
    var url = base_url+'backend/Merchandise/delete_merchandise/';
    var data = {
        'id' : refid
    }

    var delete_items = ajax_delete_item('Yakin ingin menghapus data ini ?', url, 'post', data, 'json', table);
});

$(document).on('click', '.update', function(){
    var refid = $(this).data('refid');
    window.open(base_url+'backend/Merchandise/form_merchandise/'+refid, '_blank');
});