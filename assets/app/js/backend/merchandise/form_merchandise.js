$(document).ready(function(){
	$("#satuan").select2({
		placeholder : "Pilih Satuan"
	});

	$("#category").select2({
		placeholder : "Pilih Kategori"
	});

	load_gallery();

	let id_merchandise = $("#id").val();
	let items = ajax_load_item(base_url+'backend/Merchandise/load_attribute/'+id_merchandise, 'get', {});
	var attributeHtml = '';
	var isChecked = [];
	if (items.length > 0) {
		for (i = 0; i < items.length; i++) {
			attributeHtml += '<div class="col-md-6"><div class="m-form__group form-group"><label>'+items[i].attribute+'</label>';
			attributeHtml += '<div class="m-checkbox-inline">';

			var itemAttribute = items[i].item;
			if (itemAttribute.length > 0) {
				for (x = 0; x < itemAttribute.length; x++) {
					selected = itemAttribute[x].selected;
					textSelected = (selected == 1) ? 'selected="selected"' : '';

					attributeHtml += '<label class="m-checkbox">';
					attributeHtml += '<input type="checkbox" name="attribute-'+items[i].id+'[]" value="'+itemAttribute[x].id+'" > '+itemAttribute[x].value+' <span></span>';
					attributeHtml += '</label>';

					if (selected == 1) {
						isChecked.push(itemAttribute[x].id);
					}
				}
			}

			attributeHtml += '</div>';
			attributeHtml += '</div></div>';
		}
	}

	$("#attribute-result").html(attributeHtml);
	if (isChecked.length > 0) {
		for (c = 0; c < isChecked.length; c++) {
			$('input:checkbox[value="'+isChecked[c]+'"]').attr('checked', true);
		}
	}

	var selected = [];
	if (public_figure.length > 0) {
		for (var row in public_figure) {
			selected.push(public_figure[row].id);
		}
	}

	$("#suggest").select2({
		data : public_figure,
        ajax : {
            url : base_url+"backend/Merchandise/public_figure",
            dataType : "json",
            type : "post",
            delay : 250,
            data : function(params) {
                return {
                    search: params.term,
                    start: params.page || 0,
                    limit : 10
                }
            },
            processResults : function(data, params) {
                return {
                    results : data.items,
                    pagination: data.pagination
                };

                $("#suggest").val(public_figure).trigger("change");
            }
        },
        placeholder : "Pilih public figure"
    });

    $("#suggest").val(selected).trigger('change');
});

var unik = Math.random();
var clearDropzone;
Dropzone.autoDiscover = false;
$("div#mydropzone").dropzone({ 
	url: base_url+"backend/Merchandise/store_image?unik="+unik,
	acceptedFiles: "image/*",
	paramName: "images",
	maxFilesize: 100,
    method: "post",
    success: function(file, response) {
    	obj = JSON.parse(response);
    	if (obj.status > 0) {
    		file.previewElement.id = obj.filename;
    	}
    },
    init: function () {
        var dropzone = this;
        clearDropzone = function(){
            dropzone.removeAllFiles(true);
        };
    },
    addRemoveLinks: true,
    removedfile: function(file) {
        var name = file.previewElement.id;        
            $.ajax({
                type: 'post',
                url: base_url+'backend/Merchandise/delete_image',
                data: {
                	'name' : name,
                	'unik' : unik
                },
                dataType: 'json',
                success: function(result) {
                    console.log(result.status);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
    }
});

$("#form-data").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    formData.append('unik', unik);
    var url = base_url+'backend/Merchandise/add_merchandise/';
    var id = $("#id").val();

    var simpan = ajax_save_page(url, 'post', formData);   

    if(simpan.status == 1) {
        toastr.success(simpan.message);
        if (id == '') {
        	document.getElementById('form-data').reset();
        	$("#id").val("");

        	setTimeout(window.location.href = base_url+"backend/Merchandise", 1000);
        }
        clearDropzone();
        load_gallery();
    } else {
        toastr.warning(simpan.message);
    }

    return false;
});

var load_gallery = function() {
	var id = $("#id").val();
	var result = ajax_load_html(base_url+'backend/Merchandise/load_gallery/'+id, 'get', {});

	$("#gallery-result").html(result);
}

$(document).on('click', '.delete-img', function(){
	var img = $(this).data('img');
	var id = $("#id").val();
	if (img != '') {
		var url = base_url+'backend/Merchandise/delete_image_merchandise/';
	    var data = {
	        'id' : id,
	        'image' : img
	    }

		swal({
		  	text: 'Yakin ingin menghapus data ini ?',
		  	icon: "warning",
		  	buttons: true,
		  	dangerMode: true,
		})
		.then((willDelete) => {
		  	if (willDelete) {
		  		$.ajax({
		  			url : url,
		  			type : 'post',
		  			data : data,
		  			async : false,
		  			dataType : 'json',
		  			success : function(data) {
		  				if(data.status == 1) {
			                toastr.success(data.message);
			            } else {
			                toastr.warning(data.message);
			            }

			            load_gallery();
		  			},
		  			error : function() {
		  				toastr.warning('Terjadi kesalahan saat mengirim data');
		  			}
		  		});
		  	}
		});
	} else {
		swall('Gambar tidak ditemukan');
	}
});