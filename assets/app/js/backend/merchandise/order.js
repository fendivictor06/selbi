var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Merchandise/data_order',
        type: "GET"
    },
    columnDefs : [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 9 }
    ],
    aaSorting: [[3, 'desc']]
});

var load_penawaran = function(id){
    var load_items = ajax_load_item(base_url+'backend/Merchandise/detail_penawaran', 'post', {'id' : id});
    var result = '';
    if (load_items) {
        result += '<div class="m-portlet m-portlet--full-height" style="max-height:300px; overflow-y:auto">';
        result += '<div class="m-portlet__body">';
        result += '<div class="m-widget3">';

        for (i = 0; i < load_items.length; i++) {
            var user = (load_items[i].admin != '') ? load_items[i].admin : load_items[i].userid;
            var status = load_items[i].status;
            var label;
            if (status == 1 || status == 2) {
                label = 'info';
            } else if (status == 3) {
                label = 'success';
            } else {
                label = 'danger';
            }

            var images = '';
            if (load_items[i].images) {
                images += '<div class="row" style="margin-bottom:10px;">';
                for (a = 0; a < load_items[i].images.length; a++) {
                    images += ' <div class="col-md-4">\
                                    <div class="card">\
                                        <img class="card-img-top hover-image image-modal" src="'+load_items[i].images[a]+'"  style="width:100%;" >\
                                    </div>\
                                </div>';
                }
                images += '</div>';
            }

            result += 
            '<div class="m-widget3__item">\
                <div class="m-widget3__header">\
                    <div class="m-widget3__user-img">\
                        <img class="m-widget3__img" src="'+base_url+'assets/app/media/img/users/user4.png" alt="">\
                    </div>\
                    <div class="m-widget3__info">\
                        <span class="m-widget3__username">\
                            '+user+'\
                        </span>\
                        <br>\
                        <span class="m-widget3__time">\
                            '+load_items[i].waktu+'\
                        </span>\
                    </div>\
                    <span class="m-widget3__status m--font-'+label+'">\
                        '+load_items[i].progress+'\
                    </span>\
                </div>\
                <div class="m-widget3__body">\
                    <p class="m-widget3__text">\
                        '+load_items[i].keterangan+'\
                    </p>\
                    '+images+'\
                </div>\
            </div>';
        }

        result += '</div>';
        result += '</div>';
        result += '</div>';
    }

    $("#results").html(result);
}

$(document).on('click', '.orders', function(){
    var id = $(this).data('refid');
    $("#id").val(id);
    $("#m_modal").modal();
    load_penawaran(id);
});

var unik = Math.random();
var clearDropzone;
Dropzone.autoDiscover = false;
$("div#mydropzone").dropzone({ 
    url: base_url+"backend/Merchandise/store_image?unik="+unik,
    acceptedFiles: "image/*",
    paramName: "images",
    maxFilesize: 100,
    method: "post",
    success: function(file, response) {
        obj = JSON.parse(response);
        if (obj.status > 0) {
            file.previewElement.id = obj.filename;
        }
    },
    init: function () {
        var dropzone = this;
        clearDropzone = function(){
            dropzone.removeAllFiles(true);
        };
    },
    addRemoveLinks: true,
    removedfile: function(file) {
        var name = file.previewElement.id;        
            $.ajax({
                type: 'post',
                url: base_url+'backend/Merchandise/delete_image',
                data: {
                    'name' : name,
                    'unik' : unik
                },
                dataType: 'json',
                success: function(result) {
                    console.log(result.status);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
    }
});

$("#form-data").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    formData.append('unik', unik);
    var url = base_url+'backend/Merchandise/add_penawaran/';
    var id = $("#id").val();

    var simpan = ajax_save_page(url, 'post', formData);   

    if(simpan.status == 1) {
        toastr.success(simpan.message);
        document.getElementById('form-data').reset();
        $("#id").val("");
        // $("#m_modal").modal('hide');
        clearDropzone();
        load_penawaran(id);
    } else {
        toastr.warning(simpan.message);
    }

    return false;
});

$(document).on('click', '.views', function(){
    var id = $(this).data('refid');
});

