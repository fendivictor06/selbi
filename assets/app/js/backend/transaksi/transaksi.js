var start = moment().format('DD/MM/YYYY');
var end = moment().format('DD/MM/YYYY');
var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Transaksi/data_transaksi?datestart='+start+'&dateend='+end,
        type: "GET"
    }    
});

// predefined ranges
$('#search').daterangepicker({
    buttonClasses: 'm-btn btn',
    applyClass: 'btn-primary',
    cancelClass: 'btn-secondary',
    locale: {
        format: 'DD/MM/YYYY'
    },
    startDate: start,
    endDate: end,
    ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, function(datestart, dateend, label) {
    $('#search .form-control').val(datestart.format('DD/MM/YYYY') + ' / ' + dateend.format('DD/MM/YYYY'));

    start = datestart.format('DD/MM/YYYY');
    end = dateend.format('DD/MM/YYYY');

});

$("#search-btn").click(function(){
    table.ajax.url(base_url+'backend/Transaksi/data_transaksi?datestart='+start+'&dateend='+end).load();
});
