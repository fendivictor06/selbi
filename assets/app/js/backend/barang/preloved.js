/* Create by - Akbar */

var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Barang/ajax_data_preloved',
        type: "GET"
    }    
});

$("#add_new").click(function(){
    window.open(base_url+'backend/Barang/form_preloved', '_blank');
});

$(document).on('click', '.update', function(){
    refid = $(this).data('refid');
    window.open(base_url+'backend/Barang//'+refid, '_blank');
});

$(document).on('click', '.delete', function(){
    refid = $(this).data('refid');
    swal({
        text: "Apakah anda yakin akan menghapus data ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : base_url+'backend/Barang/delete_barang',
                type : 'post',
                data : {
                    'id' : refid
                },
                dataType : 'json',
                success : function(data) {
                    if(data.status == 1) {
                        table.ajax.reload(null, false);
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat menghapus data');
                }
            });
        }
    });
});