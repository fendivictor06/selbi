var table = $("#tb-data").DataTable({
    processing : true,
    responsive :true,
    serverSide : true,
    language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    ajax : {
        url : base_url+'backend/Greeting/ajax_data_greeting',
        type: "GET"
    }    
});

$(document).on('click', '.delete', function(){
    refid = $(this).data('refid');
    swal({
        text: "Apakah anda yakin akan menghapus data ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : base_url+'backend/Greeting/delete_greeting',
                type : 'post',
                data : {
                    'id' : refid
                },
                dataType : 'json',
                success : function(data) {
                    if(data.status == 1) {
                        table.ajax.reload(null, false);
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat menghapus data');
                }
            });
        }
    });
});