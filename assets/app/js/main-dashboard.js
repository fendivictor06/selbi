var load_user = function(){
	let data = ajax_load_item(base_url+'backend/Main/new_user', 'get', {});

	if (data) {
		let result = '';
		for (i = 0; i < data.length; i++) {
			result += '	<div class="m-widget4__item">\
							<div class="m-widget4__img m-widget4__img--pic">\
                                <img src="'+data[i].foto+'" alt="" width="48">\
                            </div>\
                            <div class="m-widget4__info">\
                                <span class="m-widget4__title">\
                                    '+data[i].profile_name+'\
                                </span>\
                                <br>\
                                <span class="m-widget4__sub">\
                                    '+data[i].email+'\
                                </span>\
                            </div>\
                        </div>';
		}

		$("#new_user").html(result);
	}
}

$(document).ready(function(){
	load_user();
});